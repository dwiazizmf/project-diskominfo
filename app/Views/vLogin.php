<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="">

    <title><?= $title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="/Bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/Bootstrap/css/floating-labels.css" rel="stylesheet">
</head>

<body class="text-center" style="background-image: url('/Themes/img/wall1.jpg'); /* Full height */
  height: 100%;

  /* Center and scale the image nicely */
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;">
    <form class="form-signin" method="POST" action="/login/checkLogin">
        <?= csrf_field(); ?>
        <img class="mb-4" src="/Themes/img/Logodiskominfo.png" alt="" width="348" height="165">
        <h1 class="h1 mb-1 font-weight-normal">SIMPEL-KEUN</h1>
        <h1 class="h6 mb-4 font-weight-normal">(SISTEM PELAPORAN REALISASI KEUANGAN)</h1>
        <?= $this->include('layout/Alert.php'); ?>
        <label for="inputEmail" class="sr-only">NIP</label>
        <input type="text" name="nip_pegawai" id="nip_pegawai" class="form-control" placeholder="NIP" required autofocus>
        </br>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name='password_pegawai' id="password_pegawai" class="form-control" placeholder="Password" required>
        <div class="checkbox mb-3">
            <!-- <label>
                <input type="checkbox" value="remember-me"> Remember me
            </label>-->
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        <p class="mt-5 mb-3 text-muted">&copy; 2020</p>
    </form>
</body>

</html>