<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<!-- Custom styles for this page -->
<link href="/Themes/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="/Themes/vendor/datatables/buttons.dataTables.min.css" rel="stylesheet">

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">List Data</h6>
    </div>
    <div class="card-body">
        <?= $this->include('layout/Alert.php'); ?>
        <form action="/kompensasi/update/<?= $data['id']; ?>" method="POST">
            <?= csrf_field(); ?>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="kodin">Kodin</label>
                    <input type="text" class="form-control <?= ($validation->hasError('kodin')) ? 'is-invalid' : ''; ?>" name="kodin" id="kodin" value="<?= (old('kodin')) ? old('kodin') : $data['kodin']; ?>" <?= ($validation->hasError('kodin')) ? 'autofocus' : ''; ?>>
                    <div id="validation_kodin" class="invalid-feedback">
                        <?= $validation->getError('kodin'); ?>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control <?= ($validation->hasError('nama')) ? 'is-invalid' : ''; ?>" name="nama" id="nama" value="<?= (old('nama')) ? old('nama') : $data['nama']; ?>" <?= ($validation->hasError('nama')) ? 'autofocus' : ''; ?>>
                    <div id="validation_nama" class="invalid-feedback">
                        <?= $validation->getError('nama'); ?>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="rek_internal">Rekening Internal</label>
                    <input type="text" class="form-control <?= ($validation->hasError('rek_internal')) ? 'is-invalid' : ''; ?>" name="rek_internal" id="rek_internal" value="<?= (old('rek_internal')) ? old('rek_internal') : $data['rek_internal']; ?>" <?= ($validation->hasError('rek_internal')) ? 'autofocus' : ''; ?>>
                    <div id="validation_rek_internal" class="invalid-feedback">
                        <?= $validation->getError('rek_internal'); ?>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label for="rek_eksternal">Rekening Eksternal</label>
                    <input type="text" class="form-control <?= ($validation->hasError('rek_eksternal')) ? 'is-invalid' : ''; ?>" name="rek_eksternal" id="rek_eksternal" value="<?= (old('rek_eksternal')) ? old('rek_eksternal') : $data['rek_eksternal']; ?>" <?= ($validation->hasError('rek_eksternal')) ? 'autofocus' : ''; ?>>
                    <div id="validation_rek_eksternal" class="invalid-feedback">
                        <?= $validation->getError('rek_eksternal'); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="tpp">TPP</label>
                <input type="text" class="form-control <?= ($validation->hasError('tpp')) ? 'is-invalid' : ''; ?>" name="tpp" id="tpp" value="<?= (old('tpp')) ? old('tpp') : $data['tpp']; ?>" <?= ($validation->hasError('tpp')) ? 'autofocus' : ''; ?>>
                <div id="validation_tpp" class="invalid-feedback">
                    <?= $validation->getError('tpp'); ?>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="pph_21">PPH 21</label>
                    <input type="text" class="form-control <?= ($validation->hasError('pph_21')) ? 'is-invalid' : ''; ?>" name="pph_21" id="pph_21" value="<?= (old('pph_21')) ? old('pph_21') : $data['pph_21']; ?>" <?= ($validation->hasError('pph_21')) ? 'autofocus' : ''; ?>>
                    <div id="validation_pph_21" class="invalid-feedback">
                        <?= $validation->getError('pph_21'); ?>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label for="simpanan_kas">Simpanan Kas</label>
                    <input type="text" class="form-control <?= ($validation->hasError('simpanan_kas')) ? 'is-invalid' : ''; ?>" name="simpanan_kas" id="simpanan_kas" value="<?= (old('simpanan_kas')) ? old('simpanan_kas') : $data['simpanan_kas']; ?>" <?= ($validation->hasError('simpanan_kas')) ? 'autofocus' : ''; ?>>
                    <div id="validation_simpanan_kas" class="invalid-feedback">
                        <?= $validation->getError('simpanan_kas'); ?>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="potongan_kkps">Potongan KKPS</label>
                    <input type="text" class="form-control <?= ($validation->hasError('potongan_kkps')) ? 'is-invalid' : ''; ?>" name="potongan_kkps" id="potongan_kkps" value="<?= (old('potongan_kkps')) ? old('potongan_kkps') : $data['potongan_kkps']; ?>" <?= ($validation->hasError('potongan_kkps')) ? 'autofocus' : ''; ?>>
                    <div id="validation_potongan_kkps" class="invalid-feedback">
                        <?= $validation->getError('potongan_kkps'); ?>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label for="pot_dinas">Pot Dinas</label>
                    <input type="text" class="form-control <?= ($validation->hasError('pot_dinas')) ? 'is-invalid' : ''; ?>" name="pot_dinas" id="pot_dinas" value="<?= (old('pot_dinas')) ? old('pot_dinas') : $data['pot_dinas']; ?>" <?= ($validation->hasError('pot_dinas')) ? 'autofocus' : ''; ?>>
                    <div id="validation_pot_dinas" class="invalid-feedback">
                        <?= $validation->getError('pot_dinas'); ?>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>

<!-- Page level plugins -->
<script src="/Themes/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.buttons.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/jszip.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.html5.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.print.min.js"></script>


<!-- Page level custom scripts -->
<script src="/js/Salary.js"></script>

<?= $this->endSection(); ?>