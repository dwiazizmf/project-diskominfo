<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<!-- Custom styles for this page -->
<link href="/Themes/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="/Themes/vendor/datatables/buttons.dataTables.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" />

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">List Data</h6>
    </div>
    <div class="card-body">
        <?= $this->include('layout/Alert.php'); ?>
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Bidang</th>
                        <th>BPP</th>
                        <th>Tahun</th>
                        <th>Jumlah DPA</th>
                        <th>Anggaran Kas</th>
                        <th>Rekap SP2D</th>
                        <th>Rekap SPJ</th>
                        <!--<th>Pelimpahan</th>
                        <th>Confirm 1</th>
                        <th>Confirm 2</th>-->
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1;
                    foreach ($data as $value) : ?>
                        <tr>
                            <td><?= $no; ?></td>
                            <td><?= $bidang[$value['id_bidang']]; ?></td>
                            <td><?= "- " . $pegawai[$value['id_pegawai']]; ?><?= ($value['id_pegawai_2'] != '') ? '</br> - ' . $pegawai[$value['id_pegawai_2']] : '' ?></td>
                            <td><?= $value['tahun_anggaran']; ?></td>
                            <td class="text-right"><?= number_format($value['jumlah_dpa'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['anggaran_kas'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['sp2d'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['spj'], 0, ",", "."); ?></td>
                            <!--<td class="text-right"><?= number_format($value['pelimpahan'], 0, ",", "."); ?></td>
                            <td><?= ($value['confirm_1']) ? '<label style="color:green;"><strong>YA</strong></label>' : '<label style="color:red;"><strong>NO</strong</label>'; ?></td>
                            <td><?= ($value['confirm_2']) ? '<label style="color:green;"><strong>YA</strong></label>' : '<label style="color:red;"><strong>NO</strong</label>'; ?></td>
                            --->
                            <td>

                                <?php if ($_SESSION['data']['id_jabatan'] == 2 || $_SESSION['data']['id_jabatan'] == 3) { ?>
                                    <button type="button" data-id="<?= $value['id']; ?>" data-jumlah="<?= number_format($value['jumlah_dpa'], 0, ",", "."); ?>" data-title="<?= $bidang[$value['id_bidang']] . " tahun " . $value['tahun_anggaran']; ?>" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap">Edit</button>

                                    <a href="/realisasi/edit/<?= $value['unique_id']; ?>" class="btn btn-warning btn-sm">Isi</a>
                                    <a href="/realisasi/edit_pelimpahan/<?= $value['unique_id']; ?>" class="btn btn-warning btn-sm">Pelimpahan</a>
                                <?php } ?>
                                <form id="deleteForm" action="/anggaran/<?= $value['id']; ?>" method="POST" class="d-inline">
                                    <?= csrf_field(); ?>
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="btn btn-danger btn-sm" onclick="deleteConfirm()">DELETE</button>
                                </form>

                                <?php if ($_SESSION['data']['id_jabatan'] == 3 || $_SESSION['data']['id_jabatan'] == 6) { ?>
                                    <!--
                                    <form action="/anggaran/<?= $value['id']; ?>" method="POST" class="d-inline">
                                        <?= csrf_field(); ?>
                                        <input type="hidden" name="_method" value="PUT">
                                        <?php if ($_SESSION['data']['id_jabatan'] == 3) { ?>
                                            <input type="hidden" name="user_confirm_1" value="<?= $_SESSION['data']['id_pegawai']; ?>">
                                            <input type="hidden" name="confirm_1" value="1">
                                        <?php } else { ?>
                                            <input type="hidden" name="user_confirm_2" value="<?= $_SESSION['data']['id_pegawai']; ?>">
                                            <input type="hidden" name="confirm_2" value="1">
                                        <?php } ?>
                                        <button class="btn btn-success btn-sm">CONFIRM</button>
                                    </form>
                                    -->
                                <?php } ?>
                            </td>
                        </tr>
                    <?php $no++;
                    endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <input type="hidden" readonly class="form-control" id="id_jumlah_anggaran">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Jumlah lama :</label>
                        <input type="text" readonly class="form-control" id="jumlah_lama">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Jumlah Baru:</label>
                        <input type="text" class="form-control" nama="jumlah_baru" id="jumlah_baru_input">
                        <input type="hidden" class="form-control" name="jumlah_baru" id="jumlah_baru">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" onclick="edit_jumlah_anggarn()" class="btn btn-primary">Edit</button>
            </div>
        </div>
    </div>
</div>

<!-- Page level plugins -->
<script src="/Themes/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.buttons.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/jszip.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.html5.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.print.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous"></script>

<!-- Page level custom scripts -->
<script src="/js/Salary.js"></script>

<script>
    $('#exampleModal').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var jumlah = button.data('jumlah') // Extract info from data-* attributes
        var title = button.data('title')
        var id = button.data('id')
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text('Edit Anggaran ' + title)
        $('#jumlah_lama').val(jumlah)
        $('#id_jumlah_anggaran').val(id)
    })

    function edit_jumlah_anggarn() {
        var id = $('#id_jumlah_anggaran').val();
        var jumlah_baru = $('#jumlah_baru').val();
        if (jumlah_baru == '') {
            alert('Kolom jumlah baru harus diisi');
            return false;
        }
        $.ajax({
            type: 'POST',
            url: "anggaran/edit_jumlah_anggaran",
            data: {
                id: id,
                jumlah_dpa: jumlah_baru
            },
            dataType: 'json',
            success: function(data) {
                if (data) {
                    location.reload()
                }
            }
        });
    }

    function deleteConfirm() {
        var result = confirm("Yakin akan menghapus data ini?");
        if (result) {
            document.getElementById("deleteForm").submit();
        }
    }

    function formatAmountNoDecimals(number) {
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(number)) {
            number = number.replace(rgx, '$1' + '.' + '$2');
        }
        return number;
    }

    function formatAmount(number, name) {
        // remove all the characters except the numeric values
        number = number.replace(/[^0-9]/g, '');

        // set the default value
        // if (number.length == 0) number = "0.00";
        // else if (number.length == 1) number = "0.0" + number;
        // else if (number.length == 2) number = "0." + number;
        // else number = number.substring(0, number.length - 2) + '.' + number.substring(number.length - 2, number.length);

        // set the precision
        number = new Number(number);
        $("[name=" + name + "]").val(number);
        number = number.toFixed(2); // only works with the "."

        // change the splitter to ","
        number = number.replace(/\./g, ',');

        // format the amount
        x = number.split(',');
        x1 = x[0];
        x2 = x.length > 1 ? ',' + x[1] : '';

        return formatAmountNoDecimals(x1);
    }

    $(function() {

        $('#jumlah_baru_input').keyup(function() {
            console.log("masuk kan");
            var name = $(this).attr('nama');
            $(this).val(formatAmount($(this).val(), name));
        });

    });
</script>

<?= $this->endSection(); ?>