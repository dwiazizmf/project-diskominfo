<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">List Data</h6>
    </div>
    <div class="card-body">
        <?= $this->include('layout/Alert.php'); ?>
        <form action="/anggaran/store" method="POST">
            <?= csrf_field(); ?>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="tahun_anggaran">Periode</label>
                    <input type="text" readonly placeholder="Input Periode" value="<?= old('tahun_anggaran'); ?>" class="form-control <?= ($validation->hasError('tahun_anggaran')) ? 'is-invalid' : ''; ?>" name="tahun_anggaran" id="tahun_anggaran" <?= ($validation->hasError('tahun_anggaran')) ? 'autofocus' : ''; ?>>
                    <div id="validation_tahun_anggaran" class="invalid-feedback">
                        <?= $validation->getError('tahun_anggaran'); ?>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label for="id_bidang">Bidang</label>
                    <select class="form-control <?= ($validation->hasError('id_bidang')) ? 'is-invalid' : ''; ?>" name="id_bidang" id="id_bidang" <?= ($validation->hasError('id_bidang')) ? 'autofocus' : ''; ?>>
                        <?php foreach ($bidang as $value) { ?>
                            <option value="<?= $value['id']; ?>"><?= $value['nama_bidang']; ?></option>
                        <?php } ?>
                    </select>
                    <div id="validation_id_bidang" class="invalid-feedback">
                        <?= $validation->getError('id_bidang'); ?>
                    </div>
                </div>
            </div>
            <?php if ($_SESSION['data']['id_jabatan'] == 3 || $_SESSION['data']['id_jabatan'] == 2) { ?>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="id_pegawai">BPP 1</label>
                        <select class="form-control <?= ($validation->hasError('id_pegawai')) ? 'is-invalid' : ''; ?>" name="id_pegawai" id="id_pegawai" <?= ($validation->hasError('id_pegawai')) ? 'autofocus' : ''; ?>>
                            <?php foreach ($pegawai as $value) { ?>
                                <option value="<?= $value['id']; ?>"><?= $value['nama_pegawai']; ?></option>
                            <?php } ?>

                        </select>
                        <div id="validation_id_pegawai" class="invalid-feedback">
                            <?= $validation->getError('id_pegawai'); ?>
                        </div>
                    </div>
                    <!--<div class="form-group col-md-6">
                        <label for="id_pegawai_2">BPP 2</label>
                        <select class="form-control <?= ($validation->hasError('id_pegawai_2')) ? 'is-invalid' : ''; ?>" name="id_pegawai_2" id="id_pegawai_2" <?= ($validation->hasError('id_pegawai_2')) ? 'autofocus' : ''; ?>>
                            <option value="">-- Pilih --</option>
                            <?php foreach ($pegawai as $value) { ?>
                                <option value="<?= $value['id']; ?>"><?= $value['nama_pegawai']; ?></option>
                            <?php } ?>
                        </select>
                        <div id="validation_id_pegawai_2" class="invalid-feedback">
                            <?= $validation->getError('id_pegawai_2'); ?>
                        </div>
                    </div>-->
                </div>
            <?php } else { ?>
                <input type="hidden" name="id_pegawai" value="<?= $_SESSION['data']['id_pegawai']; ?>">
            <?php } ?>

            <div class="form-group">
                <label for="jumlah_dpa">Jumlah DPA</label>
                <input type="text" class="form-control <?= ($validation->hasError('jumlah_dpa')) ? 'is-invalid' : ''; ?>" name="jumlah_dpa" id="jumlah_dpa" value="<?= old('jumlah_dpa')  ?>" <?= ($validation->hasError('jumlah_dpa')) ? 'autofocus' : ''; ?>>
                <div id="validation_jumlah_dpa" class="invalid-feedback">
                    <?= $validation->getError('jumlah_dpa'); ?>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Insert</button>
        </form>


    </div>

</div>

<div class="alert alert-success" role="alert">
    <p>* Jika dalam bidang itu hanya ada satu BPP, silahkan pilih satu saja</p>
</div>

<!-- Page level custom scripts -->
<script src="/js/Anggaran.js"></script>

<?= $this->endSection(); ?>