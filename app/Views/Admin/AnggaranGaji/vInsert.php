<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">List Data</h6>
    </div>
    <div class="card-body">
        <?= $this->include('layout/Alert.php'); ?>
        <form action="/anggaran_gaji/store" method="POST">
            <?= csrf_field(); ?>


            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="tahun_anggaran">Periode</label>
                    <input type="text" readonly placeholder="Input Periode" value="<?= old('tahun_anggaran'); ?>" class="form-control <?= ($validation->hasError('tahun_anggaran')) ? 'is-invalid' : ''; ?>" name="tahun_anggaran" id="tahun_anggaran" <?= ($validation->hasError('tahun_anggaran')) ? 'autofocus' : ''; ?>>
                    <div id="validation_tahun_anggaran" class="invalid-feedback">
                        <?= $validation->getError('tahun_anggaran'); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="jumlah_dpa_gaji">Jumlah DPA</label>
                <input type="text" class="form-control <?= ($validation->hasError('jumlah_dpa_gaji')) ? 'is-invalid' : ''; ?>" name="jumlah_dpa_gaji" id="jumlah_dpa_gaji" value="<?= old('jumlah_dpa_gaji')  ?>" <?= ($validation->hasError('jumlah_dpa_gaji')) ? 'autofocus' : ''; ?>>
                <div id="validation_jumlah_dpa_gaji" class="invalid-feedback">
                    <?= $validation->getError('jumlah_dpa_gaji'); ?>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Insert</button>
        </form>
    </div>
</div>

<!-- Page level custom scripts -->
<script src="/js/Anggaran.js"></script>

<?= $this->endSection(); ?>