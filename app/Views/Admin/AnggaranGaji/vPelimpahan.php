<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<!-- Custom styles for this page -->
<link href="/Themes/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="/Themes/vendor/datatables/buttons.dataTables.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" />

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">List Data</h6>
    </div>
    <div class="card-body">
        <?= $this->include('layout/Alert.php'); ?>
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th rowspan="2">No</th>
                        <th rowspan="2">Nama BPP</th>
                        <th colspan="3">Pelimpahan</th>
                        <th colspan="3">SPJ</th>
                        <th colspan="3">Saldo Kas BPP</th>
                    </tr>
                    <tr>
                        <th>S/D Bulan Lalu</th>
                        <th>Bulan Ini</th>
                        <th>S/D Bulan Ini</th>
                        <th>S/D Bulan Lalu</th>
                        <th>Bulan Ini</th>
                        <th>S/D Bulan Ini</th>
                        <th>S/D Bulan Lalu</th>
                        <th>Bulan Ini</th>
                        <th>S/D Bulan Ini</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1;
                    foreach ($data as $value) : ?>
                        <tr>
                            <td><?= $no; ?></td>
                            <td><?= $pegawai[$value['id_pegawai']] . " (" . $bidang[$value['id_bidang']] . ")"; ?></td>
                            <td class="text-right"><?= number_format($value['sd_bulan_lalu'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['bulan_ini'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['sd_bulan_ini'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['spj_sd_bulan_lalu'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['spj_bulan_ini'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['spj_sd_bulan_ini'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['bpp_sd_bulan_lalu'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['bpp_bulan_ini'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['bpp_sd_bulan_ini'], 0, ",", "."); ?></td>
                        </tr>
                    <?php $no++;
                    endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Page level plugins -->
<script src="/Themes/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.buttons.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/jszip.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.html5.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.print.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous"></script>

<!-- Page level custom scripts -->
<script src="/js/Salary.js"></script>

<?= $this->endSection(); ?>