<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<!-- Custom styles for this page -->
<link href="/Themes/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="/Themes/vendor/datatables/buttons.dataTables.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" />

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">List Data</h6>
    </div>
    <div class="card-body">
        <?= $this->include('layout/Alert.php'); ?>
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>User Insert</th>
                        <th>Tahun</th>
                        <th>Jumlah DPA</th>
                        <th>Anggaran Kas</th>
                        <th>SP2D</th>
                        <th>SPJ</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1;
                    foreach ($data as $value) : ?>
                        <tr>
                            <td><?= $no; ?></td>
                            <td><?= $pegawai[$value['user_inserted']]; ?></td>
                            <td><?= $value['tahun_anggaran']; ?></td>
                            <td class="text-right"><?= number_format($value['jumlah_dpa_gaji'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['anggaran_kas_gaji'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['sp2d_gaji'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['spj_gaji'], 0, ",", "."); ?></td>
                            <td>

                                <?php if ($_SESSION['data']['id_jabatan'] == 2) { ?>
                                    <a href="/anggaran_gaji/edit/<?= $value['unique_id']; ?>" class="btn btn-warning btn-sm">Isi</a>
                                <?php } ?>
                                <form id="deleteForm" action="/anggaran_gaji/<?= $value['id']; ?>" method="POST" class="d-inline">
                                    <?= csrf_field(); ?>
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="btn btn-danger btn-sm" onclick="deleteConfirm()">DELETE</button>
                                </form>
                                <?php if ($_SESSION['data']['id_jabatan'] == 3 || $_SESSION['data']['id_jabatan'] == 6) { ?>
                                    <!-- <form action="/anggaran/<?= $value['id']; ?>" method="POST" class="d-inline">
                                        <?= csrf_field(); ?>
                                        <input type="hidden" name="_method" value="PUT">
                                        <?php if ($_SESSION['data']['id_jabatan'] == 3) { ?>
                                            <input type="hidden" name="user_confirm_1" value="<?= $_SESSION['data']['id_pegawai']; ?>">
                                            <input type="hidden" name="confirm_1" value="1">
                                        <?php } else { ?>
                                            <input type="hidden" name="user_confirm_2" value="<?= $_SESSION['data']['id_pegawai']; ?>">
                                            <input type="hidden" name="confirm_2" value="1">
                                        <?php } ?>
                                        <button class="btn btn-success btn-sm">CONFIRM</button>
                                    </form>-->
                                <?php } ?>
                            </td>
                        </tr>
                    <?php $no++;
                    endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Page level plugins -->
<script src="/Themes/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.buttons.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/jszip.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.html5.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.print.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous"></script>

<!-- Page level custom scripts -->
<script src="/js/Salary.js"></script>

<script>
    function deleteConfirm() {
        var result = confirm("Yakin akan menghapus data ini?");
        if (result) {
            document.getElementById("deleteForm").submit();
        }
    }
</script>

<?= $this->endSection(); ?>