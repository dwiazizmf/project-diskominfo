<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<!-- Custom styles for this page -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" />

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">List Data</h6>
    </div>
    <div class="card-body">
        <?= $this->include('layout/Alert.php'); ?>
        <form action="/anggaran/store" method="POST">
            <?= csrf_field(); ?>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="id_bidang">Bidang</label>
                    <select onchange="check()" class="form-control <?= ($validation->hasError('id_bidang')) ? 'is-invalid' : ''; ?>" name="id_bidang" id="id_bidang" <?= ($validation->hasError('id_bidang')) ? 'autofocus' : ''; ?>>
                        <?php foreach ($bidang as $value) { ?>
                            <option value="<?= $value['id']; ?>"><?= $value['nama_bidang']; ?></option>
                        <?php } ?>
                    </select>
                    <div id="validation_id_bidang" class="invalid-feedback">
                        <?= $validation->getError('id_bidang'); ?>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label for="tahun_anggaran">Periode</label>
                    <input onchange="check()" type="text" readonly placeholder="Input Periode" value="<?= old('tahun_anggaran'); ?>" class="form-control <?= ($validation->hasError('tahun_anggaran')) ? 'is-invalid' : ''; ?>" name="tahun_anggaran" id="tahun_anggaran" <?= ($validation->hasError('tahun_anggaran')) ? 'autofocus' : ''; ?>>
                    <div id="validation_tahun_anggaran" class="invalid-feedback">
                        <?= $validation->getError('tahun_anggaran'); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="jumlah_dpa">Jumlah DPA</label>
                <input type="text" class="form-control <?= ($validation->hasError('jumlah_dpa')) ? 'is-invalid' : ''; ?>" name="jumlah_dpa" id="jumlah_dpa" value="<?= old('jumlah_dpa')  ?>" <?= ($validation->hasError('jumlah_dpa')) ? 'autofocus' : ''; ?>>
                <div id="validation_jumlah_dpa" class="invalid-feedback">
                    <?= $validation->getError('jumlah_dpa'); ?>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Insert</button>
        </form>
    </div>
</div>

<!-- Page level plugins -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous"></script>


<!-- Page level custom scripts -->
<script src="/js/Anggaran.js"></script>

<?= $this->endSection(); ?>