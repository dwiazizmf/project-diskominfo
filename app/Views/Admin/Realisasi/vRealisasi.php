<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<!-- Custom styles for this page -->
<link href="/Themes/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="/Themes/vendor/datatables/buttons.dataTables.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" />


<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
    </div>
    <div class="card-body">
        <?= $this->include('layout/Alert.php'); ?>
        <form action="/realisasi/store" method="POST">
            <?= csrf_field(); ?>
            <input type="hidden" name="unique_id" value="<?= $anggaran[0]['unique_id']; ?>" />
            <input type="hidden" name="id_anggaran" value="<?= $anggaran[0]['id']; ?>" />
            <input type="hidden" name="id_anggaran_kas" value="<?= $anggaran_kas[0]['id']; ?>" />
            <input type="hidden" name="id_realisasisp2d" value="<?= $realisasi_sp2d[0]['id']; ?>" />
            <input type="hidden" name="id_realisasispj" value="<?= $realisasi_spj[0]['id']; ?>" />
            <input type="hidden" name="id_pelimpahan" value="<?= $pelimpahan[0]['id']; ?>" />
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Bidang</th>
                            <th>BPP</th>
                            <th>DPA</th>
                            <th>Jenis</th>
                            <th>Januari</th>
                            <th>Februari</th>
                            <th>Maret</th>
                            <th>April</th>
                            <th>Mei</th>
                            <th>Juni</th>
                            <th>Juli</th>
                            <th>Agustus</th>
                            <th>Sepetember</th>
                            <th>Oktober</th>
                            <th>November</th>
                            <th>Desember</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?= $bidang['nama_bidang']; ?></td>
                            <td><?= $pegawai['nama_pegawai']; ?></td>
                            <td><?= $anggaran[0]['jumlah_dpa']; ?></td>
                            <td>Angsuran Kas</td>
                            <?php for ($i = 1; $i < 13; $i++) { ?>
                                <td>
                                    <input type="text" class='text-right amount' value="<?= (old('anggaran_kas_bulan_' . $i)) ? number_format(old('anggaran_kas_bulan_' . $i), 0, ",", ".") : number_format($anggaran_kas[0]['bulan_' . $i], 0, ",", "."); ?>" nama="anggaran_kas_bulan_<?= $i; ?>" />
                                    <input type="hidden" class='text-right' value="<?= (old('anggaran_kas_bulan_' . $i)) ? old('anggaran_kas_bulan_' . $i) : $anggaran_kas[0]['bulan_' . $i]; ?>" name="anggaran_kas_bulan_<?= $i; ?>" />
                                    <div id="validation_anggaran_kas_bulan_<?= $i; ?>" class="invalid-feedback" style="display: block;">
                                        <?= $validation->getError("anggaran_kas_bulan_" . $i); ?>
                                    </div>
                                </td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><?= $bidang['nama_bidang']; ?></td>
                            <td><?= $pegawai['nama_pegawai']; ?></td>
                            <td><?= $anggaran[0]['jumlah_dpa']; ?></td>
                            <td>Rekap SP2D</td>
                            <?php for ($i = 1; $i < 13; $i++) { ?>
                                <td><input type="text" class='text-right amount' value="<?= (old('realisasi_sp2d_bulan_' . $i)) ? number_format(old('realisasi_sp2d_bulan_' . $i), 0, ",", ".") : number_format($realisasi_sp2d[0]['bulan_' . $i], 0, ",", "."); ?>" nama="realisasi_sp2d_bulan_<?= $i; ?>" />
                                    <input type="hidden" class='text-right' value="<?= (old('realisasi_sp2d_bulan_' . $i)) ? old('realisasi_sp2d_bulan_' . $i) : $realisasi_sp2d[0]['bulan_' . $i]; ?>" name="realisasi_sp2d_bulan_<?= $i; ?>" />
                                    <div id="validation_realisasi_sp2d_bulan_1" class="invalid-feedback" style="display: block;">
                                        <?= $validation->getError('realisasi_sp2d_bulan_' . $i); ?>
                                    </div>
                                </td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><?= $bidang['nama_bidang']; ?></td>
                            <td><?= $pegawai['nama_pegawai']; ?></td>
                            <td><?= $anggaran[0]['jumlah_dpa']; ?></td>
                            <td>Rekap SPJ</td>
                            <?php for ($i = 1; $i < 13; $i++) { ?>
                                <td><input type="text" class='text-right amount' value="<?= (old('realisasi_spj_bulan_' . $i)) ? number_format(old('realisasi_spj_bulan_' . $i), 0, ",", ".") : number_format($realisasi_spj[0]['bulan_' . $i], 0, ",", "."); ?>" nama="realisasi_spj_bulan_<?= $i; ?>" />
                                    <input type="hidden" class='text-right' value="<?= (old('realisasi_spj_bulan_' . $i)) ? old('realisasi_spj_bulan_' . $i) : $realisasi_spj[0]['bulan_' . $i]; ?>" name="realisasi_spj_bulan_<?= $i; ?>" />
                                    <div id="validation_realisasi_spj_bulan_1" class="invalid-feedback" style="display: block;">
                                        <?= $validation->getError('realisasi_spj_bulan_1'); ?>
                                    </div>
                                </td>
                            <?php } ?>
                        </tr>
                        <!--<tr>
                            <td><?= $bidang['nama_bidang']; ?></td>
                            <td><?= $pegawai['nama_pegawai']; ?></td>
                            <td><?= $anggaran[0]['jumlah_dpa']; ?></td>
                            <td>Pelimpahan</td>
                            <td><input type="text" value="<?= (old('pelimpahan_bulan_1')) ? old('pelimpahan_bulan_1') : $pelimpahan[0]['bulan_1']; ?>" name="pelimpahan_bulan_1" />
                                <div id="validation_pelimpahan_bulan_1" class="invalid-feedback" style="display: block;">
                                    <?= $validation->getError('pelimpahan_bulan_1'); ?>
                                </div>
                            </td>
                            <td><input type="text" value="<?= (old('pelimpahan_bulan_2')) ? old('pelimpahan_bulan_2') : $pelimpahan[0]['bulan_2']; ?>" name="pelimpahan_bulan_2" />
                                <div id="validation_pelimpahan_bulan_2" class="invalid-feedback" style="display: block;">
                                    <?= $validation->getError('pelimpahan_bulan_2'); ?>
                                </div>
                            </td>
                            <td><input type="text" value="<?= (old('pelimpahan_bulan_3')) ? old('pelimpahan_bulan_3') : $pelimpahan[0]['bulan_3']; ?>" name="pelimpahan_bulan_3" />
                                <div id="validation_pelimpahan_bulan_3" class="invalid-feedback" style="display: block;">
                                    <?= $validation->getError('pelimpahan_bulan_3'); ?>
                                </div>
                            </td>
                            <td><input type="text" value="<?= (old('pelimpahan_bulan_4')) ? old('pelimpahan_bulan_4') : $pelimpahan[0]['bulan_4']; ?>" name="pelimpahan_bulan_4" />
                                <div id="validation_pelimpahan_bulan_4" class="invalid-feedback" style="display: block;">
                                    <?= $validation->getError('pelimpahan_bulan_4'); ?>
                                </div>
                            </td>
                            <td><input type="text" value="<?= (old('pelimpahan_bulan_5')) ? old('pelimpahan_bulan_5') : $pelimpahan[0]['bulan_5']; ?>" name="pelimpahan_bulan_5" />
                                <div id="validation_pelimpahan_bulan_5" class="invalid-feedback" style="display: block;">
                                    <?= $validation->getError('pelimpahan_bulan_5'); ?>
                                </div>
                            </td>
                            <td><input type="text" value="<?= (old('pelimpahan_bulan_6')) ? old('pelimpahan_bulan_6') : $pelimpahan[0]['bulan_6']; ?>" name="pelimpahan_bulan_6" />
                                <div id="validation_pelimpahan_bulan_6" class="invalid-feedback" style="display: block;">
                                    <?= $validation->getError('pelimpahan_bulan_6'); ?>
                                </div>
                            </td>
                            <td><input type="text" value="<?= (old('pelimpahan_bulan_7')) ? old('pelimpahan_bulan_7') : $pelimpahan[0]['bulan_7']; ?>" name="pelimpahan_bulan_7" />
                                <div id="validation_pelimpahan_bulan_7" class="invalid-feedback" style="display: block;">
                                    <?= $validation->getError('pelimpahan_bulan_7'); ?>
                                </div>
                            </td>
                            <td><input type="text" value="<?= (old('pelimpahan_bulan_8')) ? old('pelimpahan_bulan_8') : $pelimpahan[0]['bulan_8']; ?>" name="pelimpahan_bulan_8" />
                                <div id="validation_pelimpahan_bulan_8" class="invalid-feedback" style="display: block;">
                                    <?= $validation->getError('pelimpahan_bulan_8'); ?>
                                </div>
                            </td>
                            <td><input type="text" value="<?= (old('pelimpahan_bulan_9')) ? old('pelimpahan_bulan_9') : $pelimpahan[0]['bulan_9']; ?>" name="pelimpahan_bulan_9" />
                                <div id="validation_pelimpahan_bulan_9" class="invalid-feedback" style="display: block;">
                                    <?= $validation->getError('pelimpahan_bulan_9'); ?>
                                </div>
                            </td>
                            <td><input type="text" value="<?= (old('pelimpahan_bulan_10')) ? old('pelimpahan_bulan_10') : $pelimpahan[0]['bulan_10']; ?>" name="pelimpahan_bulan_10" />
                                <div id="validation_pelimpahan_bulan_10" class="invalid-feedback" style="display: block;">
                                    <?= $validation->getError('pelimpahan_bulan_10'); ?>
                                </div>
                            </td>
                            <td><input type="text" value="<?= (old('pelimpahan_bulan_11')) ? old('pelimpahan_bulan_11') : $pelimpahan[0]['bulan_11']; ?>" name="pelimpahan_bulan_11" />
                                <div id="validation_pelimpahan_bulan_11" class="invalid-feedback" style="display: block;">
                                    <?= $validation->getError('pelimpahan_bulan_11'); ?>
                                </div>
                            </td>
                            <td><input type="text" value="<?= (old('pelimpahan_bulan_12')) ? old('pelimpahan_bulan_12') : $pelimpahan[0]['bulan_12']; ?>" name="pelimpahan_bulan_12" />
                                <div id="validation_pelimpahan_bulan_12" class="invalid-feedback" style="display: block;">
                                    <?= $validation->getError('pelimpahan_bulan_12'); ?>
                                </div>
                            </td>
                        </tr>-->
                    </tbody>
                </table>
            </div>
            </br>
            <?php if ($_SESSION['data']['id_jabatan'] != 1) { ?><button type="submit" class="btn btn-success float-left">Save Data</button><?php } ?>
        </form>
    </div>
</div>

<!-- Page level plugins -->
<script src="/Themes/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.buttons.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/jszip.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.html5.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.print.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous"></script>

<!-- Page level custom scripts -->

<script>
    $(document).ready(function() {
        var table = $("#dataTable").DataTable({
            dom: "Bfrt",
            buttons: ["copyHtml5", "excelHtml5", {
                extend: "print",
                autoPrint: false
            }],
        });
    });

    // function checkDpa(params) {
    //     var dpa = '<?= $anggaran[0]['jumlah_dpa']; ?>';
    //     console.log(params + " -- " + dpa);
    // }
</script>

<script>
    function formatAmountNoDecimals(number) {
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(number)) {
            number = number.replace(rgx, '$1' + '.' + '$2');
        }
        return number;
    }

    function formatAmount(number, name) {
        // remove all the characters except the numeric values
        number = number.replace(/[^0-9]/g, '');

        // set the default value
        // if (number.length == 0) number = "0.00";
        // else if (number.length == 1) number = "0.0" + number;
        // else if (number.length == 2) number = "0." + number;
        // else number = number.substring(0, number.length - 2) + '.' + number.substring(number.length - 2, number.length);

        // set the precision
        number = new Number(number);
        $("[name=" + name + "]").val(number);
        number = number.toFixed(2); // only works with the "."

        // change the splitter to ","
        number = number.replace(/\./g, ',');

        // format the amount
        x = number.split(',');
        x1 = x[0];
        x2 = x.length > 1 ? ',' + x[1] : '';

        return formatAmountNoDecimals(x1);
    }


    $(function() {

        $('.amount').keyup(function() {
            console.log("masuk kan");
            var name = $(this).attr('nama');
            $(this).val(formatAmount($(this).val(), name));
        });

    });
</script>

<?= $this->endSection(); ?>