<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<!-- Custom styles for this page -->
<link href="/Themes/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="/Themes/vendor/datatables/buttons.dataTables.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" />


<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">List Data</h6>
    </div>
    <div class="card-body">
        <?= $this->include('layout/Alert.php'); ?>
        <form action="/realisasi/store" method="POST">
            <?= csrf_field(); ?>
            <input type="hidden" name="id_anggaran_kas" value="<?= $anggaran_kas[0]['id']; ?>" />
            <input type="hidden" name="id_realisasisp2d" value="<?= $realisasi_sp2d[0]['id']; ?>" />
            <input type="hidden" name="id_realisasispj" value="<?= $realisasi_spj[0]['id']; ?>" />
            <?php if ($_SESSION['data']['id_jabatan'] != 1) { ?><button type="submit" class="btn btn-success txt-right">Save</button><?php } ?>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Bidang</th>
                            <th>BPP</th>
                            <th>DPA</th>
                            <th>Jenis</th>
                            <th>Januari</th>
                            <th>Februari</th>
                            <th>Maret</th>
                            <th>April</th>
                            <th>Mei</th>
                            <th>Juni</th>
                            <th>Juli</th>
                            <th>Agustus</th>
                            <th>Sepetember</th>
                            <th>Oktober</th>
                            <th>November</th>
                            <th>Desember</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?= $bidang['nama_bidang']; ?></td>
                            <td><?= $pegawai['nama_pegawai']; ?></td>
                            <td><?= $anggaran['jumlah_dpa']; ?></td>
                            <td>Angsuran Kas</td>
                            <td><?= $anggaran_kas[0]['bulan_1']; ?></td>
                            <td><?= $anggaran_kas[0]['bulan_2']; ?></td>
                            <td><?= $anggaran_kas[0]['bulan_3']; ?></td>
                            <td><?= $anggaran_kas[0]['bulan_4']; ?></td>
                            <td><?= $anggaran_kas[0]['bulan_5']; ?></td>
                            <td><?= $anggaran_kas[0]['bulan_6']; ?></td>
                            <td><?= $anggaran_kas[0]['bulan_7']; ?></td>
                            <td><?= $anggaran_kas[0]['bulan_8']; ?></td>
                            <td><?= $anggaran_kas[0]['bulan_9']; ?></td>
                            <td><?= $anggaran_kas[0]['bulan_10']; ?></td>
                            <td><?= $anggaran_kas[0]['bulan_11']; ?></td>
                            <td><?= $anggaran_kas[0]['bulan_12']; ?></td>
                        </tr>
                        <tr>
                            <td><?= $bidang['nama_bidang']; ?></td>
                            <td><?= $pegawai['nama_pegawai']; ?></td>
                            <td><?= $anggaran['jumlah_dpa']; ?></td>
                            <td>Rekap SP2D</td>
                            <td><?= $realisasi_sp2d[0]['bulan_1'];   ?></td>
                            <td><?= $realisasi_sp2d[0]['bulan_2'];   ?></td>
                            <td><?= $realisasi_sp2d[0]['bulan_3'];   ?></td>
                            <td><?= $realisasi_sp2d[0]['bulan_4'];   ?></td>
                            <td><?= $realisasi_sp2d[0]['bulan_5'];   ?></td>
                            <td><?= $realisasi_sp2d[0]['bulan_6'];   ?></td>
                            <td><?= $realisasi_sp2d[0]['bulan_7'];   ?></td>
                            <td><?= $realisasi_sp2d[0]['bulan_8'];   ?></td>
                            <td><?= $realisasi_sp2d[0]['bulan_9'];   ?></td>
                            <td><?= $realisasi_sp2d[0]['bulan_10'];   ?></td>
                            <td><?= $realisasi_sp2d[0]['bulan_11'];   ?></td>
                            <td><?= $realisasi_sp2d[0]['bulan_12'];   ?></td>
                        </tr>
                        <tr>
                            <td><?= $bidang['nama_bidang']; ?></td>
                            <td><?= $pegawai['nama_pegawai']; ?></td>
                            <td><?= $anggaran['jumlah_dpa']; ?></td>
                            <td>Rekap SPJ</td>
                            <td><?= $realisasi_spj[0]['bulan_1']; ?></td>
                            <td><?= $realisasi_spj[0]['bulan_2']; ?></td>
                            <td><?= $realisasi_spj[0]['bulan_3']; ?></td>
                            <td><?= $realisasi_spj[0]['bulan_4']; ?></td>
                            <td><?= $realisasi_spj[0]['bulan_5']; ?></td>
                            <td><?= $realisasi_spj[0]['bulan_6']; ?></td>
                            <td><?= $realisasi_spj[0]['bulan_7']; ?></td>
                            <td><?= $realisasi_spj[0]['bulan_8']; ?></td>
                            <td><?= $realisasi_spj[0]['bulan_9']; ?></td>
                            <td><?= $realisasi_spj[0]['bulan_10']; ?></td>
                            <td><?= $realisasi_spj[0]['bulan_11']; ?></td>
                            <td><?= $realisasi_spj[0]['bulan_12']; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>
</div>

<!-- Page level plugins -->
<script src="/Themes/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.buttons.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/jszip.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.html5.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.print.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous"></script>

<!-- Page level custom scripts -->
<script src="/js/Salary.js"></script>

<?= $this->endSection(); ?>