<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<!-- Custom styles for this page -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" />

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">List Data</h6>
    </div>
    <div class="card-body">
        <?= $this->include('layout/Alert.php'); ?>

        <form action="/users/update_password/<?= $data[0]['id'] ?>" method="POST">
            <?= csrf_field(); ?>
            <input type="hidden" name="id" value="<?= $data[0]['id'] ?>" />
            <input type="hidden" name="unique_id" value="<?= $data[0]['unique_id'] ?>" />
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="old_password">Old Password</label>
                    <input type="password" class="form-control <?= (session()->getFlashData('err-old_password')) ? 'is-invalid' : ''; ?>" value="<?= old('old_password'); ?>" name="old_password" id="old_password" <?= (session()->getFlashData('err-old_password')) ? 'autofocus' : ''; ?>>
                    <div id="validation_old_password" class="invalid-feedback">
                        <?= (session()->getFlashData('err-old_password')) ? session()->getFlashData('err-old_password') : ''; ?>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="password_pegawai">Password</label>
                    <input type="password" class="form-control <?= ($validation->hasError('password_pegawai')) ? 'is-invalid' : ''; ?>" name="password_pegawai" value="<?= old('password_pegawai'); ?>" id="password_pegawai" <?= ($validation->hasError('password_pegawai')) ? 'autofocus' : ''; ?>>
                    <div id="validation_password_pegawai" class="invalid-feedback">
                        <?= $validation->getError('password_pegawai'); ?>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label for="re_password_pegawai">Re Password</label>
                    <input type="password" class="form-control <?= ($validation->hasError('re_password_pegawai')) ? 'is-invalid' : ''; ?>" name="re_password_pegawai" value="<?= old('re_password_pegawai'); ?>" id="re_password_pegawai" <?= ($validation->hasError('re_password_pegawai')) ? 'autofocus' : ''; ?>>
                    <div id="validation_re_password_pegawai" class="invalid-feedback">
                        <?= $validation->getError('re_password_pegawai'); ?>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>

<!-- Page level plugins -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous"></script>


<!-- Page level custom scripts -->
<script src="/js/Anggaran.js"></script>

<?= $this->endSection(); ?>