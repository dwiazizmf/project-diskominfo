<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<!-- Custom styles for this page -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" />

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">List Data</h6>
    </div>
    <div class="card-body">
        <?= $this->include('layout/Alert.php'); ?>
        <form action="/users/store" method="POST">
            <input type="hidden" value="12345" class="form-control <?= ($validation->hasError('password_pegawai')) ? 'is-invalid' : ''; ?>" name="password_pegawai" id="password_pegawai" <?= ($validation->hasError('password_pegawai')) ? 'autofocus' : ''; ?>>
            <input type="hidden" value="12345" class="form-control <?= ($validation->hasError('re_password_pegawai')) ? 'is-invalid' : ''; ?>" name="re_password_pegawai" id="re_password_pegawai" <?= ($validation->hasError('re_password_pegawai')) ? 'autofocus' : ''; ?>>

            <?= csrf_field(); ?>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="nip_pegawai">NIP</label>
                    <input type="text" class="form-control <?= ($validation->hasError('nip_pegawai')) ? 'is-invalid' : ''; ?>" value="<?= old('nip_pegawai'); ?>" name="nip_pegawai" id="nip_pegawai" <?= ($validation->hasError('nip_pegawai')) ? 'autofocus' : ''; ?>>
                    <div id="validation_nip_pegawai" class="invalid-feedback">
                        <?= $validation->getError('nip_pegawai'); ?>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label for="nama_pegawai">Nama</label>
                    <input type="text" class="form-control <?= ($validation->hasError('nama_pegawai')) ? 'is-invalid' : ''; ?>" value="<?= old('nama_pegawai'); ?>" name="nama_pegawai" id="nama_pegawai" <?= ($validation->hasError('nama_pegawai')) ? 'autofocus' : ''; ?>>
                    <div id="validation_nama_pegawai" class="invalid-feedback">
                        <?= $validation->getError('nama_pegawai'); ?>
                    </div>
                </div>
            </div>
            <!--<div class="form-row">
                <div class="form-group col-md-6">
                    <label for="password_pegawai">Password</label>
                    <input type="password" class="form-control <?= ($validation->hasError('password_pegawai')) ? 'is-invalid' : ''; ?>" name="password_pegawai" id="password_pegawai" <?= ($validation->hasError('password_pegawai')) ? 'autofocus' : ''; ?>>
                    <div id="validation_password_pegawai" class="invalid-feedback">
                        <?= $validation->getError('password_pegawai'); ?>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label for="re_password_pegawai">Re Password</label>
                    <input type="password" class="form-control <?= ($validation->hasError('re_password_pegawai')) ? 'is-invalid' : ''; ?>" name="re_password_pegawai" id="re_password_pegawai" <?= ($validation->hasError('re_password_pegawai')) ? 'autofocus' : ''; ?>>
                    <div id="validation_re_password_pegawai" class="invalid-feedback">
                        <?= $validation->getError('re_password_pegawai'); ?>
                    </div>
                </div>
            </div>-->
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="pangkat">Pangkat</label>
                    <input type="text" class="form-control <?= ($validation->hasError('pangkat')) ? 'is-invalid' : ''; ?>" name="pangkat" value="<?= old('pangkat'); ?>" id="pangkat" <?= ($validation->hasError('pangkat')) ? 'autofocus' : ''; ?>>
                    <div id="validation_pangkat" class="invalid-feedback">
                        <?= $validation->getError('pangkat'); ?>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label for="gol_pangkat">Golongan</label>
                    <input type="text" class="form-control <?= ($validation->hasError('gol_pangkat')) ? 'is-invalid' : ''; ?>" name="gol_pangkat" value="<?= old('gol_pangkat'); ?>" id="gol_pangkat" <?= ($validation->hasError('gol_pangkat')) ? 'autofocus' : ''; ?>>
                    <div id="validation_gol_pangkat" class="invalid-feedback">
                        <?= $validation->getError('gol_pangkat'); ?>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="nama_jabatan">Nama Jabatan</label>
                    <input type="text" class="form-control <?= ($validation->hasError('nama_jabatan')) ? 'is-invalid' : ''; ?>" name="nama_jabatan" value="<?= old('nama_jabatan'); ?>" id="nama_jabatan" <?= ($validation->hasError('nama_jabatan')) ? 'autofocus' : ''; ?>>
                    <div id="nama_jabatan" class="invalid-feedback">
                        <?= $validation->getError('nama_jabatan'); ?>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label for="id_jabatan">Jabatan di DISKOMINFO</label>
                    <select class="form-control <?= ($validation->hasError('id_jabatan')) ? 'is-invalid' : ''; ?>" name="id_jabatan" id="id_jabatan" <?= ($validation->hasError('id_jabatan')) ? 'autofocus' : ''; ?>>
                        <?php foreach ($jabatan as $value) { ?>
                            <option value="<?= $value['id']; ?>"><?= $value['nama_jabatan']; ?></option>
                        <?php } ?>
                    </select>
                    <div id="validation_id_jabatan" class="invalid-feedback">
                        <?= $validation->getError('id_jabatan'); ?>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>

<!-- Page level plugins -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous"></script>


<!-- Page level custom scripts -->
<script src="/js/Anggaran.js"></script>

<?= $this->endSection(); ?>