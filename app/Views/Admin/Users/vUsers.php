<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<!-- Custom styles for this page -->
<link href="/Themes/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="/Themes/vendor/datatables/buttons.dataTables.min.css" rel="stylesheet">

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">List Data</h6>
    </div>
    <div class="card-body">
        <?= $this->include('layout/Alert.php'); ?>
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>NIP Pegawai</th>
                        <th>Nama Pegawai</th>
                        <th>Pangkat</th>
                        <th>Golongan</th>
                        <th>Nama Jabatan</th>
                        <th>Jabatan</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1;
                    foreach ($data as $value) : ?>
                        <tr>
                            <td><?= $no; ?></td>
                            <td><?= $value['nip_pegawai']; ?></td>
                            <td><?= $value['nama_pegawai']; ?></td>
                            <td><?= $value['pangkat']; ?></td>
                            <td><?= $value['gol_pangkat']; ?></td>
                            <td><?= $value['nama_jabatan']; ?></td>
                            <td><?= $jabatan[$value['id_jabatan']]; ?></td>
                            <td><?= ($value['status']) ? '<label style="color:green;">Aktif</label>' : '<label style="color:red;">Non Aktif</label>'; ?></td>
                            <td>
                                <a href="/users/edit/<?= $value['unique_id']; ?>" class="btn btn-primary btn-sm">Edit</a>
                                <!--<form action="/users/<?= $value['id']; ?>" method="POST" class="d-inline">
                                    <?= csrf_field(); ?>
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="btn btn-danger btn-sm">DELETE</button>
                                </form> -->

                                <form action="/users/<?= $value['unique_id']; ?>" method="POST" class="d-inline">
                                    <?= csrf_field(); ?>
                                    <input type="hidden" name="_method" value="PUT">
                                    <button class="btn btn-warning btn-sm">STATUS</button>
                                </form>
                            </td>
                        </tr>
                    <?php $no++;
                    endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Page level plugins -->
<script src="/Themes/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.buttons.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/jszip.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.html5.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.print.min.js"></script>


<!-- Page level custom scripts -->
<script src="/js/Users.js"></script>

<?= $this->endSection(); ?>