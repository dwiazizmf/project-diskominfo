<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<!-- Custom styles for this page -->
<link href="/Themes/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="/Themes/vendor/datatables/buttons.dataTables.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" />

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">List Data</h6>
    </div>
    <div class="card-body">
        <?= $this->include('layout/Alert.php'); ?>
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th rowspan="2">No</th>
                        <th rowspan="2">Kodin</th>
                        <th rowspan="2">Rek Internal</th>
                        <th rowspan="2">Rek Eksternal</th>
                        <th rowspan="2">Nama</th>
                        <th rowspan="2">Gaji Kotor</th>
                        <th colspan="7" class="text-center">Angsuran BJB</th>
                        <th rowspan="2">POT Dinas</th>
                        <th rowspan="2">Gaji Bersih</th>
                        <th rowspan="2">Action</th>
                    </tr>
                    <tr>
                        <th>CAB Utama</th>
                        <th>Kab. Bandung</th>
                        <th>Suci</th>
                        <th>Bubat</th>
                        <th>Gd. Sate</th>
                        <th>OTISTA</th>
                        <th>Asia Afrika</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1;
                    foreach ($data as $value) : ?>
                        <tr>
                            <td><?= $no; ?></td>
                            <td><?= $value['kodin']; ?></td>
                            <td><?= $value['rek_internal']; ?></td>
                            <td><?= $value['rek_eksternal']; ?></td>
                            <td><?= $value['nama']; ?></td>
                            <td><?= $value['gaji_kotor']; ?></td>
                            <td><?= $value['angs_cab_utama']; ?></td>
                            <td><?= $value['angs_kab_bdg']; ?></td>
                            <td><?= $value['angs_suci']; ?></td>
                            <td><?= $value['angs_bubat']; ?></td>
                            <td><?= $value['angs_gd_sate']; ?></td>
                            <td><?= $value['angs_otista']; ?></td>
                            <td><?= $value['angs_asia_afrika']; ?></td>
                            <td><?= $value['pot_dinas']; ?></td>
                            <td><?= $value['gaji_bersih']; ?></td>
                            <td>
                                <a href="/salary/edit/<?= $value['id']; ?>" class="btn btn-warning btn-sm">Edit</a>
                                <form action="/salary/<?= $value['id']; ?>" method="POST" class="d-inline">
                                    <?= csrf_field(); ?>
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="btn btn-danger btn-sm">DELETE</button>
                                </form>
                            </td>
                        </tr>
                    <?php $no++;
                    endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Page level plugins -->
<script src="/Themes/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.buttons.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/jszip.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.html5.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.print.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous"></script>

<!-- Page level custom scripts -->
<script src="/js/Salary.js"></script>

<?= $this->endSection(); ?>