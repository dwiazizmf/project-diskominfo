<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<!-- Custom styles for this page -->
<link href="/Themes/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="/Themes/vendor/datatables/buttons.dataTables.min.css" rel="stylesheet">

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">List Data</h6>
    </div>
    <div class="card-body">
        <?= $this->include('layout/Alert.php'); ?>
        <form action="/salary/update/<?= $data['id']; ?>" method="POST">
            <?= csrf_field(); ?>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="kodin">Kodin</label>
                    <input type="text" class="form-control <?= ($validation->hasError('kodin')) ? 'is-invalid' : ''; ?>" name="kodin" id="kodin" value="<?= (old('kodin')) ? old('kodin') : $data['kodin']; ?>" <?= ($validation->hasError('kodin')) ? 'autofocus' : ''; ?>>
                    <div id="validation_kodin" class="invalid-feedback">
                        <?= $validation->getError('kodin'); ?>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control <?= ($validation->hasError('nama')) ? 'is-invalid' : ''; ?>" name="nama" id="nama" value="<?= (old('nama')) ? old('nama') : $data['nama']; ?>" <?= ($validation->hasError('nama')) ? 'autofocus' : ''; ?>>
                    <div id="validation_nama" class="invalid-feedback">
                        <?= $validation->getError('nama'); ?>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="rek_internal">Rekening Internal</label>
                    <input type="text" class="form-control <?= ($validation->hasError('rek_internal')) ? 'is-invalid' : ''; ?>" name="rek_internal" id="rek_internal" value="<?= (old('rek_internal')) ? old('rek_internal') : $data['rek_internal']; ?>" <?= ($validation->hasError('rek_internal')) ? 'autofocus' : ''; ?>>
                    <div id="validation_rek_internal" class="invalid-feedback">
                        <?= $validation->getError('rek_internal'); ?>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label for="rek_eksternal">Rekening Eksternal</label>
                    <input type="text" class="form-control <?= ($validation->hasError('rek_eksternal')) ? 'is-invalid' : ''; ?>" name="rek_eksternal" id="rek_eksternal" value="<?= (old('rek_eksternal')) ? old('rek_eksternal') : $data['rek_eksternal']; ?>" <?= ($validation->hasError('rek_eksternal')) ? 'autofocus' : ''; ?>>
                    <div id="validation_rek_eksternal" class="invalid-feedback">
                        <?= $validation->getError('rek_eksternal'); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="gaji_kotor">Gaji Kotor</label>
                <input type="text" class="form-control <?= ($validation->hasError('gaji_kotor')) ? 'is-invalid' : ''; ?>" name="gaji_kotor" id="gaji_kotor" value="<?= (old('gaji_kotor')) ? old('gaji_kotor') : $data['gaji_kotor']; ?>" <?= ($validation->hasError('gaji_kotor')) ? 'autofocus' : ''; ?>>
                <div id="validation_gaji_kotor" class="invalid-feedback">
                    <?= $validation->getError('gaji_kotor'); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="angs_cab_utama">Angsuran Cabang Utama</label>
                <input type="text" class="form-control <?= ($validation->hasError('angs_cab_utama')) ? 'is-invalid' : ''; ?>" name="angs_cab_utama" id="angs_cab_utama" value="<?= (old('angs_cab_utama')) ? old('angs_cab_utama') : $data['angs_cab_utama']; ?>" <?= ($validation->hasError('angs_cab_utama')) ? 'autofocus' : ''; ?>>
                <div id="validation_angs_cab_utama" class="invalid-feedback">
                    <?= $validation->getError('angs_cab_utama'); ?>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="angs_kab_bdg">Angsuran Kab Bandung</label>
                    <input type="text" class="form-control <?= ($validation->hasError('angs_kab_bdg')) ? 'is-invalid' : ''; ?>" name="angs_kab_bdg" id="angs_kab_bdg" value="<?= (old('angs_kab_bdg')) ? old('angs_kab_bdg') : $data['angs_kab_bdg']; ?>" <?= ($validation->hasError('angs_kab_bdg')) ? 'autofocus' : ''; ?>>
                    <div id="validation_angs_kab_bdg" class="invalid-feedback">
                        <?= $validation->getError('angs_kab_bdg'); ?>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <label for="angs_suci">Angsuran Suci</label>
                    <input type="text" class="form-control <?= ($validation->hasError('angs_suci')) ? 'is-invalid' : ''; ?>" name="angs_suci" id="angs_suci" value="<?= (old('angs_suci')) ? old('angs_suci') : $data['angs_suci']; ?>" <?= ($validation->hasError('angs_suci')) ? 'autofocus' : ''; ?>>
                    <div id="validation_angs_suci" class="invalid-feedback">
                        <?= $validation->getError('angs_suci'); ?>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <label for="angs_bubat">Angsuran Buah Batu</label>
                    <input type="text" class="form-control <?= ($validation->hasError('angs_bubat')) ? 'is-invalid' : ''; ?>" name="angs_bubat" id="angs_bubat" value="<?= (old('angs_bubat')) ? old('angs_bubat') : $data['angs_bubat']; ?>" <?= ($validation->hasError('angs_bubat')) ? 'autofocus' : ''; ?>>
                    <div id="validation_angs_bubat" class="invalid-feedback">
                        <?= $validation->getError('angs_bubat'); ?>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="angs_gd_sate">Angsuran Gedung Sate</label>
                    <input type="text" class="form-control <?= ($validation->hasError('angs_gd_sate')) ? 'is-invalid' : ''; ?>" name="angs_gd_sate" id="angs_gd_sate" value="<?= (old('angs_gd_sate')) ? old('angs_gd_sate') : $data['angs_gd_sate']; ?>" <?= ($validation->hasError('angs_gd_sate')) ? 'autofocus' : ''; ?>>
                    <div id="validation_angs_gd_sate" class="invalid-feedback">
                        <?= $validation->getError('angs_gd_sate'); ?>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <label for="angs_otista">Angsuran Otista</label>
                    <input type="text" class="form-control <?= ($validation->hasError('angs_otista')) ? 'is-invalid' : ''; ?>" name="angs_otista" id="angs_otista" value="<?= (old('angs_otista')) ? old('angs_otista') : $data['angs_otista']; ?>" <?= ($validation->hasError('angs_otista')) ? 'autofocus' : ''; ?>>
                    <div id="validation_angs_otista" class="invalid-feedback">
                        <?= $validation->getError('angs_otista'); ?>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <label for="angs_asia_afrika">Angsuran Asia Afrika</label>
                    <input type="text" class="form-control <?= ($validation->hasError('angs_asia_afrika')) ? 'is-invalid' : ''; ?>" name="angs_asia_afrika" id="angs_asia_afrika" value="<?= (old('angs_asia_afrika')) ? old('angs_asia_afrika') : $data['angs_asia_afrika']; ?>" <?= ($validation->hasError('angs_asia_afrika')) ? 'autofocus' : ''; ?>>
                    <div id="validation_angs_asia_afrika" class="invalid-feedback">
                        <?= $validation->getError('angs_asia_afrika'); ?>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="pot_dinas">Pot Dinas</label>
                    <input type="text" class="form-control <?= ($validation->hasError('pot_dinas')) ? 'is-invalid' : ''; ?>" name="pot_dinas" id="pot_dinas" value="<?= (old('pot_dinas')) ? old('pot_dinas') : $data['pot_dinas']; ?>" <?= ($validation->hasError('pot_dinas')) ? 'autofocus' : ''; ?>>
                    <div id="validation_pot_dinas" class="invalid-feedback">
                        <?= $validation->getError('pot_dinas'); ?>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label for="gaji_bersih">Gaji Bersih</label>
                    <input type="text" class="form-control <?= ($validation->hasError('gaji_bersih')) ? 'is-invalid' : ''; ?>" name="gaji_bersih" id="gaji_bersih" value="<?= (old('gaji_bersih')) ? old('gaji_bersih') : $data['gaji_bersih']; ?>" <?= ($validation->hasError('gaji_bersih')) ? 'autofocus' : ''; ?>>
                    <div id="validation_gaji_bersih" class="invalid-feedback">
                        <?= $validation->getError('gaji_bersih'); ?>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>

<!-- Page level plugins -->
<script src="/Themes/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.buttons.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/jszip.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.html5.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.print.min.js"></script>


<!-- Page level custom scripts -->
<script src="/js/Salary.js"></script>

<?= $this->endSection(); ?>