<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<!-- Custom styles for this page -->
<link href="/Themes/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="/Themes/vendor/datatables/buttons.dataTables.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" />

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">List Data</h6>
    </div>
    <div class="card-body">
        <?= $this->include('layout/Alert.php'); ?>
        <form action="/gaji/store" enctype="multipart/form-data" method="POST">
            <?= csrf_field(); ?>
            <div class="form-group row">
                <label for="periode" class="col-sm-2 col-form-label">Periode</label>
                <div class="col-sm-10">
                    <input type="text" readonly placeholder="Input Periode" value="<?= old('periode'); ?>" class="form-control <?= ($validation->hasError('periode')) ? 'is-invalid' : ''; ?>" name="periode" id="periode" <?= ($validation->hasError('periode')) ? 'autofocus' : ''; ?>>
                    <div id="validationPeriode" class="invalid-feedback">
                        <?= $validation->getError('periode'); ?>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="file-upload" class="col-sm-2 col-form-label">File</label>
                <div class="col-sm-10">
                    <div class="custom-file">
                        <input type="file" onchange="labeling()" name="file-upload" class="custom-file-input <?= ($validation->hasError('file-upload')) ? 'is-invalid' : ''; ?>" id="file-upload" <?= ($validation->hasError('file-upload')) ? 'autofocus' : ''; ?>>
                        <label class="custom-file-label" id="label-upload" for="file-upload">Pilih Excel ..</label>
                        <div id="validationExcel" class="invalid-feedback">
                            <?= $validation->getError('file-upload'); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </div>
        </form>
        <?php if (session()->getFlashData('result')) :
            $result = session()->getFlashData('result');
            $data = $result['data'];
        ?>
            <div class="alert alert-danger" role="alert">
                <?php foreach ($data as $key => $value) { ?>
                    <p>- Data dengan NIP : <?= $value[2]; ?>, Nama : <?= $value[1]; ?>, gagal di upload</p>
                <?php } ?>

                <hr>
                <p class="mb-0">*Silahkan cek kembali inputan yang anda masukan.</p>
                <p class="mb-0">*Jangan lupa isian yang berupa angka harus di set dulu di excel dengan format nummber.</p>
                <p class="mb-0">*Jika sudah di edit datanya dan ternyata masih eror, segera hubungi administrator.</p>

            </div>
        <?php endif;
        ?>
    </div>
</div>

<!-- Page level plugins -->
<script src="/Themes/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.buttons.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/jszip.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.html5.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.print.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous"></script>


<!-- Page level custom scripts -->
<script src="/js/Salary.js"></script>

<?= $this->endSection(); ?>