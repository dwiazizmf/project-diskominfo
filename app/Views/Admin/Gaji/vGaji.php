<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<!-- Custom styles for this page -->
<link href="/Themes/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="/Themes/vendor/datatables/buttons.dataTables.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" />

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">List Data</h6>
    </div>
    <div class="card-body">
        <?= $this->include('layout/Alert.php'); ?>
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>NIP</th>
                        <th>Pangkat</th>
                        <th>Gol. Pangkat</th>
                        <th>Nama Jabatan</th>
                        <th>Bulan Gaji</th>
                        <th>Gaji Pokok</th>
                        <th>Tunjangan Istri / suami</th>
                        <th>Tunjangan Anak</th>
                        <th>Tunjangan Struktural</th>
                        <th>Tunjangan Beras</th>
                        <th>Tunjangan Pajak Penghasilan</th>
                        <th>Pembulatan</th>
                        <th>Penghasilan Kotor</th>
                        <th>IWP</th>
                        <th>ASKES</th>
                        <th>PPH 21</th>
                        <th>Tabungan Bapertarum</th>
                        <th>Jumlah Potongan</th>
                        <th>Jumlah Penghasilan Bersih</th>
                        <th>Simpanan Kop. KOMPAK</th>
                        <th>Pot. Koperasi KOMPAK</th>
                        <th>Potongan Angsuran BJB</th>
                        <th>Potongan Dharma Wanita</th>
                        <th>Jumlah Potongan Dipindahbukukan</th>
                        <th>Jumlah Penghasilan A</th>
                        <th>Tunjangan Kompensasi Mobilitas</th>
                        <th>Potongan PPh 21</th>
                        <th>Jumlah Potongan</th>
                        <th>Jumlah Penghasilan B</th>
                        <th>Tunjangan Kompensasi Kerja PNS</th>
                        <th>Potongan PPh 21</th>
                        <th>Pot. Koperasi KOMPAK</th>
                        <th>Jumlah Potongan</th>
                        <th>Jumlah Penghasilan C</th>
                        <th>TPP Maksimal</th>
                        <th>Proses Kinerja</th>
                        <th>TPP Bruto</th>
                        <th>PPh 21</th>
                        <th>TPP Netto</th>
                        <th>Potongan Zakat</th>
                        <th>Potongan Simpanan KPPS</th>
                        <th>Potongan angsuran KPPS</th>
                        <th>Potongan angsuran BJB</th>
                        <th>Pot. Angsuran KOMPAK</th>
                        <th>Pot. Angs. BJB Syariah</th>
                        <th>Jumlah Potongan</th>
                        <th>Jumlah Penghasilan D</th>
                        <th>Total Penghasilan</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1;
                    foreach ($data as $value) : ?>
                        <tr>
                            <td><?= $no; ?></t>
                            <td><?= $value['nama']; ?></td>
                            <td><?= $value['nip']; ?></td>
                            <td><?= $value['pangkat']; ?></td>
                            <td><?= $value['golongan_pangkat']; ?></td>
                            <td><?= $value['nama_jabatan']; ?></td>
                            <td><?= $value['bulan_gaji']; ?></td>
                            <td class="text-right"><?= number_format($value['gaji_pokok'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['tunjangan_issu'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['tunjangan_anak'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['tunjangan_struktural'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['tunjangan_beras'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['tunjangan_pajak_penghasilan'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['pembulatan'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['penghasilan_kotor'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['iwp'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['askes'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['pph21'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['bapetarum'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['jumlah_potongan'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['penghasilan_bersih'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['sim_kompak'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['pot_kompak'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['pot_bjb'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['pot_dw'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['total_pot'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['gaji_set_pot'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['bbm'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['bbm_pph21'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['bbm_pph21'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['bbm_bersih'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['kompensasi'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['kompensasi_pph21'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['pot_kop_kompen'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['jumlah_pot_komponen'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['kompensasi_bersih'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['tpp'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= $value['persen']; ?></td>
                            <td class="text-right"><?= number_format($value['bruto'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['pph21_tpp'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['tpp_net'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['zakat'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['kpps'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['a_kpps'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['a_bjb'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['a_kompak'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['a_syariah'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['a_total_pot'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['tpp_bersih'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['total_penerimaan_gaji'], 0, ",", "."); ?></td>
                            <td>
                                <!--<a href="/gaji/edit/<?= $value['id']; ?>" class="btn btn-warning btn-sm">Edit</a>-->
                                <form id="deleteForm" action="/gaji/<?= $value['id']; ?>" method="POST" class="d-inline">
                                    <?= csrf_field(); ?>
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="btn btn-danger btn-sm" onclick="deleteConfirm()">DELETE</button>
                                </form>
                            </td>
                        </tr>
                    <?php $no++;
                    endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Page level plugins -->
<script src="/Themes/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.buttons.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/jszip.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.html5.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.print.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous"></script>

<!-- Page level custom scripts -->
<script src="/js/Salary.js"></script>

<script>
    function deleteConfirm() {
        var result = confirm("Yakin akan menghapus data ini?");
        if (result) {
            document.getElementById("deleteForm").submit();
        }
    }
</script>

<?= $this->endSection(); ?>