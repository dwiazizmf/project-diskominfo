<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<!-- Custom styles for this page -->
<link href="/Themes/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="/Themes/vendor/datatables/buttons.dataTables.min.css" rel="stylesheet">

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">List Data</h6>
    </div>
    <div class="card-body">
        <?= $this->include('layout/Alert.php'); ?>
        <form action="/tpp/update/<?= $data['id']; ?>" method="POST">
            <?= csrf_field(); ?>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="kodin">Kodin</label>
                    <input type="text" class="form-control <?= ($validation->hasError('kodin')) ? 'is-invalid' : ''; ?>" name="kodin" id="kodin" value="<?= (old('kodin')) ? old('kodin') : $data['kodin']; ?>" <?= ($validation->hasError('kodin')) ? 'autofocus' : ''; ?>>
                    <div id="validation_kodin" class="invalid-feedback">
                        <?= $validation->getError('kodin'); ?>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control <?= ($validation->hasError('nama')) ? 'is-invalid' : ''; ?>" name="nama" id="nama" value="<?= (old('nama')) ? old('nama') : $data['nama']; ?>" <?= ($validation->hasError('nama')) ? 'autofocus' : ''; ?>>
                    <div id="validation_nama" class="invalid-feedback">
                        <?= $validation->getError('nama'); ?>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="rek_internal">Rekening Internal</label>
                    <input type="text" class="form-control <?= ($validation->hasError('rek_internal')) ? 'is-invalid' : ''; ?>" name="rek_internal" id="rek_internal" value="<?= (old('rek_internal')) ? old('rek_internal') : $data['rek_internal']; ?>" <?= ($validation->hasError('rek_internal')) ? 'autofocus' : ''; ?>>
                    <div id="validation_rek_internal" class="invalid-feedback">
                        <?= $validation->getError('rek_internal'); ?>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label for="rek_eksternal">Rekening Eksternal</label>
                    <input type="text" class="form-control <?= ($validation->hasError('rek_eksternal')) ? 'is-invalid' : ''; ?>" name="rek_eksternal" id="rek_eksternal" value="<?= (old('rek_eksternal')) ? old('rek_eksternal') : $data['rek_eksternal']; ?>" <?= ($validation->hasError('rek_eksternal')) ? 'autofocus' : ''; ?>>
                    <div id="validation_rek_eksternal" class="invalid-feedback">
                        <?= $validation->getError('rek_eksternal'); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="tpp">TPP</label>
                <input type="text" class="form-control <?= ($validation->hasError('tpp')) ? 'is-invalid' : ''; ?>" name="tpp" id="tpp" value="<?= (old('tpp')) ? old('tpp') : $data['tpp']; ?>" <?= ($validation->hasError('tpp')) ? 'autofocus' : ''; ?>>
                <div id="validation_tpp" class="invalid-feedback">
                    <?= $validation->getError('tpp'); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="zakat">Zakat</label>
                <input type="text" class="form-control <?= ($validation->hasError('zakat')) ? 'is-invalid' : ''; ?>" name="zakat" id="zakat" value="<?= (old('zakat')) ? old('zakat') : $data['zakat']; ?>" <?= ($validation->hasError('zakat')) ? 'autofocus' : ''; ?>>
                <div id="validation_tpp" class="invalid-feedback">
                    <?= $validation->getError('zakat'); ?>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="simpanan_kkps">Simpanan KKPS</label>
                    <input type="text" class="form-control <?= ($validation->hasError('simpanan_kkps')) ? 'is-invalid' : ''; ?>" name="simpanan_kkps" id="simpanan_kkps" value="<?= (old('simpanan_kkps')) ? old('simpanan_kkps') : $data['simpanan_kkps']; ?>" <?= ($validation->hasError('simpanan_kkps')) ? 'autofocus' : ''; ?>>
                    <div id="validation_tpp" class="invalid-feedback">
                        <?= $validation->getError('simpanan_kkps'); ?>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label for="potongan_kkps">Potongan KKPS</label>
                    <input type="text" class="form-control <?= ($validation->hasError('potongan_kkps')) ? 'is-invalid' : ''; ?>" name="potongan_kkps" id="potongan_kkps" value="<?= (old('potongan_kkps')) ? old('potongan_kkps') : $data['potongan_kkps']; ?>" <?= ($validation->hasError('potongan_kkps')) ? 'autofocus' : ''; ?>>
                    <div id="validation_potongan_kkps" class="invalid-feedback">
                        <?= $validation->getError('potongan_kkps'); ?>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-3">
                    <label for="ptg_cab_utama">Potongan Cabang Utama</label>
                    <input type="text" class="form-control <?= ($validation->hasError('ptg_cab_utama')) ? 'is-invalid' : ''; ?>" name="ptg_cab_utama" id="ptg_cab_utama" value="<?= (old('ptg_cab_utama')) ? old('ptg_cab_utama') : $data['ptg_cab_utama']; ?>" <?= ($validation->hasError('ptg_cab_utama')) ? 'autofocus' : ''; ?>>
                    <div id="validation_potongan_kkps" class="invalid-feedback">
                        <?= $validation->getError('ptg_cab_utama'); ?>
                    </div>
                </div>
                <div class="form-group col-md-3">
                    <label for="ptg_gd_sate">Potongan Gedung Sate</label>
                    <input type="text" class="form-control <?= ($validation->hasError('ptg_gd_sate')) ? 'is-invalid' : ''; ?>" name="ptg_gd_sate" id="ptg_gd_sate" value="<?= (old('ptg_gd_sate')) ? old('ptg_gd_sate') : $data['ptg_gd_sate']; ?>" <?= ($validation->hasError('ptg_gd_sate')) ? 'autofocus' : ''; ?>>
                    <div id="validation_ptg_gd_sate" class="invalid-feedback">
                        <?= $validation->getError('ptg_gd_sate'); ?>
                    </div>
                </div>
                <div class="form-group col-md-3">
                    <label for="ptg_otista">Potongan Otista</label>
                    <input type="text" class="form-control <?= ($validation->hasError('ptg_otista')) ? 'is-invalid' : ''; ?>" name="ptg_otista" id="ptg_otista" value="<?= (old('ptg_otista')) ? old('ptg_otista') : $data['ptg_otista']; ?>" <?= ($validation->hasError('ptg_otista')) ? 'autofocus' : ''; ?>>
                    <div id="validation_ptg_otista" class="invalid-feedback">
                        <?= $validation->getError('ptg_otista'); ?>
                    </div>
                </div>
                <div class="form-group col-md-3">
                    <label for="pot_dinas">Pot Dinas</label>
                    <input type="text" class="form-control <?= ($validation->hasError('pot_dinas')) ? 'is-invalid' : ''; ?>" name="pot_dinas" id="pot_dinas" value="<?= (old('pot_dinas')) ? old('pot_dinas') : $data['pot_dinas']; ?>" <?= ($validation->hasError('pot_dinas')) ? 'autofocus' : ''; ?>>
                    <div id="validation_pot_dinas" class="invalid-feedback">
                        <?= $validation->getError('pot_dinas'); ?>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="tpp_bersih">TPP Bersih</label>
                    <input type="text" class="form-control <?= ($validation->hasError('tpp_bersih')) ? 'is-invalid' : ''; ?>" name="tpp_bersih" id="tpp_bersih" value="<?= (old('tpp_bersih')) ? old('tpp_bersih') : $data['tpp_bersih']; ?>" <?= ($validation->hasError('tpp_bersih')) ? 'autofocus' : ''; ?>>
                    <div id="validation_tpp_bersih" class="invalid-feedback">
                        <?= $validation->getError('tpp_bersih'); ?>
                    </div>
                </div>

            </div>

            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>

<!-- Page level plugins -->
<script src="/Themes/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.buttons.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/jszip.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.html5.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.print.min.js"></script>


<!-- Page level custom scripts -->
<script src="/js/Salary.js"></script>

<?= $this->endSection(); ?>