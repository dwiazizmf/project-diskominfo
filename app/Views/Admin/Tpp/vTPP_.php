<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<!-- Custom styles for this page -->
<link href="/Themes/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="/Themes/vendor/datatables/buttons.dataTables.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" />


<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">List Data</h6>
    </div>
    <div class="card-body">
        <?= $this->include('layout/Alert.php'); ?>
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Jenis</th>
                        <th>Januari</th>
                        <th>Februari</th>
                        <th>Maret</th>
                        <th>April</th>
                        <th>Mei</th>
                        <th>Juni</th>
                        <th>Juli</th>
                        <th>Agustus</th>
                        <th>Sepetember</th>
                        <th>Oktober</th>
                        <th>November</th>
                        <th>Desember</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data as $key => $value) { ?>
                        <tr>
                            <td>TPP</td>
                            <td class="text-right"><?= number_format($value['bulan_1'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['bulan_2'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['bulan_3'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['bulan_4'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['bulan_5'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['bulan_6'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['bulan_7'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['bulan_8'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['bulan_9'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['bulan_10'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['bulan_11'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['bulan_12'], 0, ",", "."); ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Page level plugins -->
<script src="/Themes/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.buttons.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/jszip.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.html5.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.print.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous"></script>

<!-- Page level custom scripts -->

<script>
    $(document).ready(function() {
        var table = $("#dataTable").DataTable({
            dom: "Bfrt",
            buttons: ["copyHtml5", "excelHtml5", {
                extend: "print",
                autoPrint: false
            }],
        });
    });
</script>

<?= $this->endSection(); ?>