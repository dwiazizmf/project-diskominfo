<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
    </div>
    <div class="card-body">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="<?= base_url(); ?>/tree" allowfullscreen></iframe>
        </div>
    </div>
</div>

<?= $this->endSection(); ?>