<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Bootstrap core CSS -->

<div class="container">
    <div class="row">
        <div class="col-sm-4">
            <h2>Input</h2>
            <div class="form-group">
                <label for="input-check-node" class="sr-only">Search Tree:</label>
                <input type="input" class="form-control" id="input-check-node" placeholder="Identify node..." value="Parent 1">
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" class="checkbox" id="chk-check-silent" value="false">
                    Silent (No events)
                </label>
            </div>
            <div class="form-group row">
                <div class="col-sm-6">
                    <button type="button" class="btn btn-success check-node" id="btn-check-node">Check Node</button>
                </div>
            </div>
            <div class="form-group">
                <button type="button" class="btn btn-danger check-node" id="btn-uncheck-node">Uncheck Node</button>
            </div>
            <div class="form-group">
                <button type="button" class="btn btn-primary check-node" id="btn-toggle-checked">Toggle Node</button>
            </div>
            <hr>
            <div class="form-group row">
                <div class="col-sm-6">
                    <button type="button" class="btn btn-success" id="btn-check-all">Check All</button>
                </div>
            </div>
            <button type="button" class="btn btn-danger" id="btn-uncheck-all">Uncheck All</button>
        </div>
        <div class="col-sm-4">
            <h2>Tree</h2>
            <div id="treeview-checkable" class=""></div>
        </div>
        <div class="col-sm-4">
            <h2>Events</h2>
            <div id="checkable-output"></div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<script src="/Themes/vendor/bootstrap/js/bootstrap-treeview.js"></script>

<script type="text/javascript">
    $(function() {
        var defaultData = [{
                text: 'Parent 1',
                href: '#parent1',
                tags: ['4'],
                nodes: [{
                        text: 'Child 1',
                        href: '#child1',
                        tags: ['2'],
                        nodes: [{
                                text: 'Grandchild 1',
                                href: '#grandchild1',
                                tags: ['0']
                            },
                            {
                                text: 'Grandchild 2',
                                href: '#grandchild2',
                                tags: ['0']
                            }
                        ]
                    },
                    {
                        text: 'Child 2',
                        href: '#child2',
                        tags: ['0']
                    }
                ]
            },
            {
                text: 'Parent 2',
                href: '#parent2',
                tags: ['0']
            },
            {
                text: 'Parent 3',
                href: '#parent3',
                tags: ['0']
            },
            {
                text: 'Parent 4',
                href: '#parent4',
                tags: ['0']
            },
            {
                text: 'Parent 5',
                href: '#parent5',
                tags: ['0']
            }
        ];
        var $checkableTree = $('#treeview-checkable').treeview({
            data: defaultData,
            showIcon: false,
            showCheckbox: true,
            onNodeChecked: function(event, node) {
                $('#checkable-output').prepend('<p>' + node.text + ' was checked</p>');
            },
            onNodeUnchecked: function(event, node) {
                $('#checkable-output').prepend('<p>' + node.text + ' was unchecked</p>');
            }
        });

        var findCheckableNodess = function() {
            return $checkableTree.treeview('search', [$('#input-check-node').val(), {
                ignoreCase: false,
                exactMatch: false
            }]);
        };
        var checkableNodes = findCheckableNodess();

        // Check/uncheck/toggle nodes
        $('#input-check-node').on('keyup', function(e) {
            checkableNodes = findCheckableNodess();
            $('.check-node').prop('disabled', !(checkableNodes.length >= 1));
        });

        $('#btn-check-node.check-node').on('click', function(e) {
            $checkableTree.treeview('checkNode', [checkableNodes, {
                silent: $('#chk-check-silent').is(':checked')
            }]);
        });

        $('#btn-uncheck-node.check-node').on('click', function(e) {
            $checkableTree.treeview('uncheckNode', [checkableNodes, {
                silent: $('#chk-check-silent').is(':checked')
            }]);
        });

        $('#btn-toggle-checked.check-node').on('click', function(e) {
            $checkableTree.treeview('toggleNodeChecked', [checkableNodes, {
                silent: $('#chk-check-silent').is(':checked')
            }]);
        });

        // Check/uncheck all
        $('#btn-check-all').on('click', function(e) {
            $checkableTree.treeview('checkAll', {
                silent: $('#chk-check-silent').is(':checked')
            });
        });

        $('#btn-uncheck-all').on('click', function(e) {
            $checkableTree.treeview('uncheckAll', {
                silent: $('#chk-check-silent').is(':checked')
            });
        });
    });
</script>