<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<!-- Custom styles for this page -->
<link href="/Themes/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="/Themes/vendor/datatables/buttons.dataTables.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" />


<!-- Content Row -->
<div class="row">
    <!-- Earnings (Monthly) Card Example -->
    <?php $i = 0;
    foreach ($data as $key => $value) { ?>
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-<?= $color[$i]; ?> shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-<?= $color[$i]; ?> text-uppercase mb-1"><?= $value['bidang']; ?></div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= number_format(($value['spj']), 0, ",", "."); ?></div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= round($value['persen'], 2) . "%"; ?></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas  fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php
        $i++;
    } ?>

</div>


<?php foreach ($data_ as $key => $value) {
    $jml_anggaran = $value['anggaran_kas'][0]['bulan_1']
        + $value['anggaran_kas'][0]['bulan_2']
        + $value['anggaran_kas'][0]['bulan_3']
        + $value['anggaran_kas'][0]['bulan_4']
        + $value['anggaran_kas'][0]['bulan_5']
        + $value['anggaran_kas'][0]['bulan_6']
        + $value['anggaran_kas'][0]['bulan_7']
        + $value['anggaran_kas'][0]['bulan_8']
        + $value['anggaran_kas'][0]['bulan_9']
        + $value['anggaran_kas'][0]['bulan_10']
        + $value['anggaran_kas'][0]['bulan_11']
        + $value['anggaran_kas'][0]['bulan_12'];
    $jml_sp2d = $value['sp2d'][0]['bulan_1']
        + $value['sp2d'][0]['bulan_2']
        + $value['sp2d'][0]['bulan_3']
        + $value['sp2d'][0]['bulan_4']
        + $value['sp2d'][0]['bulan_5']
        + $value['sp2d'][0]['bulan_6']
        + $value['sp2d'][0]['bulan_7']
        + $value['sp2d'][0]['bulan_8']
        + $value['sp2d'][0]['bulan_9']
        + $value['sp2d'][0]['bulan_10']
        + $value['sp2d'][0]['bulan_11']
        + $value['sp2d'][0]['bulan_12'];
    $jml_spj = $value['spj'][0]['bulan_1']
        + $value['spj'][0]['bulan_2']
        + $value['spj'][0]['bulan_3']
        + $value['spj'][0]['bulan_4']
        + $value['spj'][0]['bulan_5']
        + $value['spj'][0]['bulan_6']
        + $value['spj'][0]['bulan_7']
        + $value['spj'][0]['bulan_8']
        + $value['spj'][0]['bulan_9']
        + $value['spj'][0]['bulan_10']
        + $value['spj'][0]['bulan_11']
        + $value['spj'][0]['bulan_12'];

?>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><?= $bidang[$value['id_bidang']]; ?></h6>
        </div>
        <div class="card-body">
            <?= $this->include('layout/Alert.php'); ?>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable_<?= $value['id']; ?>" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>BULAN</th>
                            <th>JANUARI</th>
                            <th>FEBRUARI</th>
                            <th>MARET</th>
                            <th>APRIL</th>
                            <th>MEI</th>
                            <th>JUNI</th>
                            <th>JULI</th>
                            <th>AGUSTUS</th>
                            <th>SEPTEMBER</th>
                            <th>OKTOBER</th>
                            <th>NOVEMBER</th>
                            <th>DESEMBER</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Anggaran (Rp)</td>
                            <td class="text-right"><?= number_format($value['anggaran_kas'][0]['bulan_1'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['anggaran_kas'][0]['bulan_2'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['anggaran_kas'][0]['bulan_3'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['anggaran_kas'][0]['bulan_4'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['anggaran_kas'][0]['bulan_5'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['anggaran_kas'][0]['bulan_6'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['anggaran_kas'][0]['bulan_7'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['anggaran_kas'][0]['bulan_8'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['anggaran_kas'][0]['bulan_9'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['anggaran_kas'][0]['bulan_10'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['anggaran_kas'][0]['bulan_11'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['anggaran_kas'][0]['bulan_12'], 0, ",", "."); ?></td>
                        </tr>
                        <tr>
                            <td>SP2D (Rp)</td>
                            <td class="text-right"><?= number_format($value['sp2d'][0]['bulan_1'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['sp2d'][0]['bulan_2'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['sp2d'][0]['bulan_3'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['sp2d'][0]['bulan_4'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['sp2d'][0]['bulan_5'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['sp2d'][0]['bulan_6'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['sp2d'][0]['bulan_7'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['sp2d'][0]['bulan_8'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['sp2d'][0]['bulan_9'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['sp2d'][0]['bulan_10'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['sp2d'][0]['bulan_11'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['sp2d'][0]['bulan_12'], 0, ",", "."); ?></td>
                        </tr>
                        <tr>
                            <td>SPJ (Rp)</td>
                            <td class="text-right"><?= number_format($value['spj'][0]['bulan_1'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['spj'][0]['bulan_2'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['spj'][0]['bulan_3'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['spj'][0]['bulan_4'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['spj'][0]['bulan_5'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['spj'][0]['bulan_6'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['spj'][0]['bulan_7'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['spj'][0]['bulan_8'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['spj'][0]['bulan_9'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['spj'][0]['bulan_10'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['spj'][0]['bulan_11'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['spj'][0]['bulan_12'], 0, ",", "."); ?></td>
                        </tr>
                        <tr>
                            <td>Anggaran (%)</td>
                            <?php for ($i = 0; $i < 12; $i++) { ?>
                                <td class="text-right"><?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['anggaran_kas'][0]['bulan_' . ($i + 1)] > 0) ?  round(($value['anggaran_kas'][0]['bulan_' . ($i + 1)] / $value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>SP2D (%)</td>
                            <?php for ($i = 0; $i < 12; $i++) { ?>
                                <td class="text-right"><?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['sp2d'][0]['bulan_' . ($i + 1)] > 0) ?  round(($value['sp2d'][0]['bulan_' . ($i + 1)] / $value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>SPJ (%)</td>
                            <?php for ($i = 0; $i < 12; $i++) { ?>
                                <td class="text-right"><?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['spj'][0]['bulan_' . ($i + 1)] > 0) ?  round(($value['spj'][0]['bulan_' . ($i + 1)] / $value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <?php } ?>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <!-- Area Chart -->
        <div class="col-xl-6 col-lg-7">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Earnings Overview</h6>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                            <div class="dropdown-header">Dropdown Header:</div>
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-area">
                        <canvas id="myAreaChart_1_<?= $value['id']; ?>"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6 col-lg-7">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Earnings Overview</h6>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                            <div class="dropdown-header">Dropdown Header:</div>
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-area">
                        <canvas id="myAreaChart_2_<?= $value['id']; ?>"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<!-- Page level plugins -->
<script src="/Themes/vendor/chart.js/Chart.min.js"></script>


<!-- Page level plugins -->
<script src="/Themes/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.buttons.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/jszip.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.html5.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.print.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous"></script>

<!-- Page level custom scripts -->
<script>
    var anggaran_kas;
    $(document).ready(function() {
        <?php foreach ($data_ as $key => $value) {
            $jml_anggaran = $value['anggaran_kas'][0]['bulan_1']
                + $value['anggaran_kas'][0]['bulan_2']
                + $value['anggaran_kas'][0]['bulan_3']
                + $value['anggaran_kas'][0]['bulan_4']
                + $value['anggaran_kas'][0]['bulan_5']
                + $value['anggaran_kas'][0]['bulan_6']
                + $value['anggaran_kas'][0]['bulan_7']
                + $value['anggaran_kas'][0]['bulan_8']
                + $value['anggaran_kas'][0]['bulan_9']
                + $value['anggaran_kas'][0]['bulan_10']
                + $value['anggaran_kas'][0]['bulan_11']
                + $value['anggaran_kas'][0]['bulan_12'];
            $jml_sp2d = $value['sp2d'][0]['bulan_1']
                + $value['sp2d'][0]['bulan_2']
                + $value['sp2d'][0]['bulan_3']
                + $value['sp2d'][0]['bulan_4']
                + $value['sp2d'][0]['bulan_5']
                + $value['sp2d'][0]['bulan_6']
                + $value['sp2d'][0]['bulan_7']
                + $value['sp2d'][0]['bulan_8']
                + $value['sp2d'][0]['bulan_9']
                + $value['sp2d'][0]['bulan_10']
                + $value['sp2d'][0]['bulan_11']
                + $value['sp2d'][0]['bulan_12'];
            $jml_spj = $value['spj'][0]['bulan_1']
                + $value['spj'][0]['bulan_2']
                + $value['spj'][0]['bulan_3']
                + $value['spj'][0]['bulan_4']
                + $value['spj'][0]['bulan_5']
                + $value['spj'][0]['bulan_6']
                + $value['spj'][0]['bulan_7']
                + $value['spj'][0]['bulan_8']
                + $value['spj'][0]['bulan_9']
                + $value['spj'][0]['bulan_10']
                + $value['spj'][0]['bulan_11']
                + $value['spj'][0]['bulan_12'];
        ?>

            var sp2d_data = [0];
            var spj_data = [0];
            var table = $("#dataTable_<?= $value['id']; ?>").DataTable({
                ordering: false,
                dom: "Bfrt",
                buttons: ["copyHtml5", "excelHtml5", {
                    extend: 'print',
                    footer: true,
                    text: 'Print',
                    title: '<label style="display:block;text-align:center;line-height:150%;">TARGET DAN REALISASI KEUANGAN ANGGARAN <?= $tahun; ?> </br> BIDANG <?= $bidang[$value['id_bidang']]; ?></label>',
                    customize: function(win) {
                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }],
            });


            var anggaran_kas = new Array(
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['anggaran_kas'][0]['bulan_1'] > 0) ? ($value['anggaran_kas'][0]['bulan_1'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['anggaran_kas'][0]['bulan_2'] > 0) ? ($value['anggaran_kas'][0]['bulan_2'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['anggaran_kas'][0]['bulan_3'] > 0) ? ($value['anggaran_kas'][0]['bulan_3'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['anggaran_kas'][0]['bulan_4'] > 0) ? ($value['anggaran_kas'][0]['bulan_4'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['anggaran_kas'][0]['bulan_5'] > 0) ? ($value['anggaran_kas'][0]['bulan_5'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['anggaran_kas'][0]['bulan_6'] > 0) ? ($value['anggaran_kas'][0]['bulan_6'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['anggaran_kas'][0]['bulan_7'] > 0) ? ($value['anggaran_kas'][0]['bulan_7'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['anggaran_kas'][0]['bulan_8'] > 0) ? ($value['anggaran_kas'][0]['bulan_8'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['anggaran_kas'][0]['bulan_9'] > 0) ? ($value['anggaran_kas'][0]['bulan_9'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['anggaran_kas'][0]['bulan_10'] > 0) ? ($value['anggaran_kas'][0]['bulan_10'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['anggaran_kas'][0]['bulan_11'] > 0) ? ($value['anggaran_kas'][0]['bulan_11'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['anggaran_kas'][0]['bulan_12'] > 0) ? ($value['anggaran_kas'][0]['bulan_12'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>');

            var sp2d = new Array(
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['sp2d'][0]['bulan_1'] > 0) ? ($value['sp2d'][0]['bulan_1'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['sp2d'][0]['bulan_2'] > 0) ? ($value['sp2d'][0]['bulan_2'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['sp2d'][0]['bulan_3'] > 0) ? ($value['sp2d'][0]['bulan_3'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['sp2d'][0]['bulan_4'] > 0) ? ($value['sp2d'][0]['bulan_4'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['sp2d'][0]['bulan_5'] > 0) ? ($value['sp2d'][0]['bulan_5'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['sp2d'][0]['bulan_6'] > 0) ? ($value['sp2d'][0]['bulan_6'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['sp2d'][0]['bulan_7'] > 0) ? ($value['sp2d'][0]['bulan_7'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['sp2d'][0]['bulan_8'] > 0) ? ($value['sp2d'][0]['bulan_8'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['sp2d'][0]['bulan_9'] > 0) ? ($value['sp2d'][0]['bulan_9'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['sp2d'][0]['bulan_10'] > 0) ? ($value['sp2d'][0]['bulan_10'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['sp2d'][0]['bulan_11'] > 0) ? ($value['sp2d'][0]['bulan_11'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['sp2d'][0]['bulan_12'] > 0) ? ($value['sp2d'][0]['bulan_12'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>');

            for (let index = 0; index < 13; index++) {
                if (sp2d[index] > 0) {
                    sp2d_data.push(sp2d[index]);
                }
            }

            var spj = new Array(
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['spj'][0]['bulan_1'] > 0) ? ($value['spj'][0]['bulan_1'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['spj'][0]['bulan_2'] > 0) ? ($value['spj'][0]['bulan_2'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['spj'][0]['bulan_3'] > 0) ? ($value['spj'][0]['bulan_3'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['spj'][0]['bulan_4'] > 0) ? ($value['spj'][0]['bulan_4'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['spj'][0]['bulan_5'] > 0) ? ($value['spj'][0]['bulan_5'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['spj'][0]['bulan_6'] > 0) ? ($value['spj'][0]['bulan_6'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['spj'][0]['bulan_7'] > 0) ? ($value['spj'][0]['bulan_7'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['spj'][0]['bulan_8'] > 0) ? ($value['spj'][0]['bulan_8'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['spj'][0]['bulan_9'] > 0) ? ($value['spj'][0]['bulan_9'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['spj'][0]['bulan_10'] > 0) ? ($value['spj'][0]['bulan_10'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['spj'][0]['bulan_11'] > 0) ? ($value['spj'][0]['bulan_11'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>',
                '<?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['spj'][0]['bulan_12'] > 0) ? ($value['spj'][0]['bulan_12'] / $value['anggaran_kas'][0]['bulan_12']) * 100 : ''; ?>');

            for (let index = 0; index < 13; index++) {
                if (spj[index] > 0) {
                    spj_data.push(spj[index]);
                }
            }

            //console.log(anggaran_kas);
            // Set new default font family and font color to mimic Bootstrap's default styling
            (Chart.defaults.global.defaultFontFamily = "Nunito"),
            '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
            Chart.defaults.global.defaultFontColor = "#858796";

            function number_format(number, decimals, dec_point, thousands_sep) {
                // *     example: number_format(1234.56, 2, ',', ' ');
                // *     return: '1 234,56'
                number = (number + "").replace(",", "").replace(" ", "");
                var n = !isFinite(+number) ? 0 : +number,
                    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                    sep = typeof thousands_sep === "undefined" ? "," : thousands_sep,
                    dec = typeof dec_point === "undefined" ? "." : dec_point,
                    s = "",
                    toFixedFix = function(n, prec) {
                        var k = Math.pow(10, prec);
                        return "" + Math.round(n * k) / k;
                    };
                // Fix for IE parseFloat(0.55).toFixed(0) = 0;
                s = (prec ? toFixedFix(n, prec) : "" + Math.round(n)).split(".");
                if (s[0].length > 3) {
                    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
                }
                if ((s[1] || "").length < prec) {
                    s[1] = s[1] || "";
                    s[1] += new Array(prec - s[1].length + 1).join("0");
                }
                return s.join(dec);
            }

            // Area Chart Example
            var ctx = document.getElementById("myAreaChart_1_<?= $value['id']; ?>");
            var myLineChart = new Chart(ctx, {
                type: "line",
                data: {
                    labels: [
                        "Bulan",
                        "Jan",
                        "Feb",
                        "Mar",
                        "Apr",
                        "Mei",
                        "Jun",
                        "Jul",
                        "Aug",
                        "Sep",
                        "Oct",
                        "Nov",
                        "Dec",
                    ],
                    datasets: [{
                            label: "Anggaran",
                            lineTension: 0.3,
                            backgroundColor: "rgba(78, 115, 223, 0.05)",
                            borderColor: "rgba(78, 115, 223, 1)",
                            pointRadius: 3,
                            pointBackgroundColor: "rgba(78, 115, 223, 1)",
                            pointBorderColor: "rgba(78, 115, 223, 1)",
                            pointHoverRadius: 3,
                            pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
                            pointHoverBorderColor: "rgba(78, 115, 223, 1)",
                            pointHitRadius: 10,
                            pointBorderWidth: 2,
                            data: [
                                0,
                                anggaran_kas[0],
                                anggaran_kas[1],
                                anggaran_kas[2],
                                anggaran_kas[3],
                                anggaran_kas[4],
                                anggaran_kas[5],
                                anggaran_kas[6],
                                anggaran_kas[7],
                                anggaran_kas[8],
                                anggaran_kas[9],
                                anggaran_kas[10],
                                anggaran_kas[11],
                            ],
                        },
                        {
                            label: "SP2D",
                            lineTension: 0.3,
                            backgroundColor: "rgba(252, 186, 3, 0.05)",
                            borderColor: "rgba(252, 186, 3, 1)",
                            pointRadius: 3,
                            pointBackgroundColor: "rgba(252, 186, 3, 1)",
                            pointBorderColor: "rgba(252, 186, 3, 1)",
                            pointHoverRadius: 3,
                            pointHoverBackgroundColor: "rgba(252, 186, 3, 1)",
                            pointHoverBorderColor: "rgba(252, 186, 3, 1)",
                            pointHitRadius: 10,
                            pointBorderWidth: 2,
                            data: sp2d_data,
                        },
                    ],
                },
                options: {
                    maintainAspectRatio: false,
                    layout: {
                        padding: {
                            left: 10,
                            right: 25,
                            top: 25,
                            bottom: 0,
                        },
                    },
                    scales: {
                        xAxes: [{
                            time: {
                                unit: "date",
                            },
                            gridLines: {
                                display: false,
                                drawBorder: false,
                            },
                            ticks: {
                                maxTicksLimit: 7,
                            },
                        }, ],
                        yAxes: [{
                            ticks: {
                                maxTicksLimit: 5,
                                padding: 10,
                                // Include a dollar sign in the ticks
                                callback: function(value, index, values) {
                                    return number_format(value) + "%";
                                },
                            },
                            gridLines: {
                                color: "rgb(234, 236, 244)",
                                zeroLineColor: "rgb(234, 236, 244)",
                                drawBorder: false,
                                borderDash: [2],
                                zeroLineBorderDash: [2],
                            },
                        }, ],
                    },
                    legend: {
                        display: false,
                    },
                    tooltips: {
                        backgroundColor: "rgb(255,255,255)",
                        bodyFontColor: "#858796",
                        titleMarginBottom: 10,
                        titleFontColor: "#6e707e",
                        titleFontSize: 14,
                        borderColor: "#dddfeb",
                        borderWidth: 1,
                        xPadding: 15,
                        yPadding: 15,
                        displayColors: false,
                        intersect: false,
                        mode: "index",
                        caretPadding: 10,
                        callbacks: {
                            label: function(tooltipItem, chart) {
                                var datasetLabel =
                                    chart.datasets[tooltipItem.datasetIndex].label || "";
                                return datasetLabel + ": " + number_format(tooltipItem.yLabel) + "%";
                            },
                        },
                    },
                },
            });

            // Area Chart Example
            var ctx = document.getElementById("myAreaChart_2_<?= $value['id']; ?>");
            var myLineChart = new Chart(ctx, {
                type: "line",
                data: {
                    labels: [
                        "Bulan",
                        "Jan",
                        "Feb",
                        "Mar",
                        "Apr",
                        "Mei",
                        "Jun",
                        "Jul",
                        "Aug",
                        "Sep",
                        "Oct",
                        "Nov",
                        "Dec",
                    ],
                    datasets: [{
                            label: "Anggaran",
                            lineTension: 0.3,
                            backgroundColor: "rgba(78, 115, 223, 0.05)",
                            borderColor: "rgba(78, 115, 223, 1)",
                            pointRadius: 3,
                            pointBackgroundColor: "rgba(78, 115, 223, 1)",
                            pointBorderColor: "rgba(78, 115, 223, 1)",
                            pointHoverRadius: 3,
                            pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
                            pointHoverBorderColor: "rgba(78, 115, 223, 1)",
                            pointHitRadius: 10,
                            pointBorderWidth: 2,
                            data: [
                                0,
                                anggaran_kas[0],
                                anggaran_kas[1],
                                anggaran_kas[2],
                                anggaran_kas[3],
                                anggaran_kas[4],
                                anggaran_kas[5],
                                anggaran_kas[6],
                                anggaran_kas[7],
                                anggaran_kas[8],
                                anggaran_kas[9],
                                anggaran_kas[10],
                                anggaran_kas[11],
                            ],
                        },
                        {
                            label: "SPJ",
                            lineTension: 0.3,
                            backgroundColor: "rgba(252, 186, 3, 0.05)",
                            borderColor: "rgba(252, 186, 3, 1)",
                            pointRadius: 3,
                            pointBackgroundColor: "rgba(252, 186, 3, 1)",
                            pointBorderColor: "rgba(252, 186, 3, 1)",
                            pointHoverRadius: 3,
                            pointHoverBackgroundColor: "rgba(252, 186, 3, 1)",
                            pointHoverBorderColor: "rgba(252, 186, 3, 1)",
                            pointHitRadius: 10,
                            pointBorderWidth: 2,
                            data: spj_data,
                        },
                    ],
                },
                options: {
                    maintainAspectRatio: false,
                    layout: {
                        padding: {
                            left: 10,
                            right: 25,
                            top: 25,
                            bottom: 0,
                        },
                    },
                    scales: {
                        xAxes: [{
                            time: {
                                unit: "date",
                            },
                            gridLines: {
                                display: false,
                                drawBorder: false,
                            },
                            ticks: {
                                maxTicksLimit: 7,
                            },
                        }, ],
                        yAxes: [{
                            ticks: {
                                maxTicksLimit: 5,
                                padding: 10,
                                // Include a dollar sign in the ticks
                                callback: function(value, index, values) {
                                    return number_format(value) + "%";
                                },
                            },
                            gridLines: {
                                color: "rgb(234, 236, 244)",
                                zeroLineColor: "rgb(234, 236, 244)",
                                drawBorder: false,
                                borderDash: [2],
                                zeroLineBorderDash: [2],
                            },
                        }, ],
                    },
                    legend: {
                        display: false,
                    },
                    tooltips: {
                        backgroundColor: "rgb(255,255,255)",
                        bodyFontColor: "#858796",
                        titleMarginBottom: 10,
                        titleFontColor: "#6e707e",
                        titleFontSize: 14,
                        borderColor: "#dddfeb",
                        borderWidth: 1,
                        xPadding: 15,
                        yPadding: 15,
                        displayColors: false,
                        intersect: false,
                        mode: "index",
                        caretPadding: 10,
                        callbacks: {
                            label: function(tooltipItem, chart) {
                                var datasetLabel =
                                    chart.datasets[tooltipItem.datasetIndex].label || "";
                                return datasetLabel + ": " + number_format(tooltipItem.yLabel) + "%";
                            },
                        },
                    },
                },
            });

        <?php } ?>
    });
</script>


<?= $this->endSection(); ?>