<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<!-- Custom styles for this page -->
<link href="/Themes/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="/Themes/vendor/datatables/buttons.dataTables.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" />

<?= $this->include('layout/Alert.php'); ?>

<?php
if (!session()->getFlashData('result')) :
    foreach ($data as $key => $value) {
        $jml_anggaran = 0;
        $jml_sp2d = 0;
        $jml_spj = 0;
?>
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary"><?php if ($key == 0) {
                                                                    echo "Target Dan Realisasi";
                                                                } elseif ($key == 1) {
                                                                    echo "Belanja Tidak Langsung";
                                                                } else {
                                                                    echo "Belanja Langsung";
                                                                } ?></h6>
            </div>
            <div class="card-body">
                <?= $this->include('layout/Alert.php'); ?>
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable_<?= $key; ?>" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>BULAN</th>
                                <th>JANUARI</th>
                                <th>FEBRUARI</th>
                                <th>MARET</th>
                                <th>APRIL</th>
                                <th>MEI</th>
                                <th>JUNI</th>
                                <th>JULI</th>
                                <th>AGUSTUS</th>
                                <th>SEPTEMBER</th>
                                <th>OKTOBER</th>
                                <th>NOVEMBER</th>
                                <th>DESEMBER</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Anggaran (Rp)</td>
                                <?php
                                for ($i = 0; $i < 12; $i++) {
                                    $jml_anggaran = $jml_anggaran + $value[0]['bulan_' . ($i + 1)];
                                ?>
                                    <td class="text-right"><?= number_format($value[0]['bulan_' . ($i + 1)], 0, ",", "."); ?></td>
                                <?php } ?>

                            </tr>
                            <tr>
                                <td>SP2D (Rp)</td>
                                <?php
                                for ($i = 0; $i < 12; $i++) {
                                    $jml_sp2d = $jml_sp2d + $value[1]['bulan_' . ($i + 1)];
                                ?>
                                    <td class="text-right"><?= number_format($value[1]['bulan_' . ($i + 1)], 0, ",", "."); ?></td>
                                <?php } ?>
                            </tr>
                            <tr>
                                <td>SPJ (Rp)</td>
                                <?php
                                for ($i = 0; $i < 12; $i++) {
                                    $jml_spj = $jml_spj + $value[2]['bulan_' . ($i + 1)];
                                ?>
                                    <td class="text-right"><?= number_format($value[2]['bulan_' . ($i + 1)], 0, ",", "."); ?></td>
                                <?php } ?>
                            </tr>
                            <tr>
                                <td>Anggaran (%)</td>
                                <?php for ($i = 0; $i < 12; $i++) { ?>
                                    <td class="text-right"><?= ((int)$value[0]['bulan_12'] > 0 && (int)$value[0]['bulan_' . ($i + 1)] > 0) ? round(((int)$value[0]['bulan_' . ($i + 1)] / (int)$value[0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                                <?php } ?>
                            </tr>
                            <tr>
                                <td>SP2D (%)</td>
                                <?php for ($i = 0; $i < 12; $i++) { ?>
                                    <td class="text-right"><?= ((int)$value[0]['bulan_12'] > 0 && (int)$value[1]['bulan_' . ($i + 1)] > 0) ? round(((int)$value[1]['bulan_' . ($i + 1)] / (int)$value[0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                                <?php } ?>
                            </tr>
                            <tr>
                                <td>SPJ (%)</td>
                                <?php for ($i = 0; $i < 12; $i++) { ?>
                                    <td class="text-right"><?= ((int)$value[0]['bulan_12'] > 0 && (int)$value[2]['bulan_' . ($i + 1)] > 0) ? round(((int)$value[2]['bulan_' . ($i + 1)] / (int)$value[0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                                <?php } ?>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    <?php } ?>
    <!-- Page level plugins -->
    <script src="/Themes/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="/Themes/vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="/Themes/vendor/datatables/dataTables.buttons.min.js"></script>
    <script src="/Themes/vendor/datatables/buttons.bootstrap4.min.js"></script>
    <script src="/Themes/vendor/datatables/jszip.min.js"></script>
    <script src="/Themes/vendor/datatables/buttons.html5.min.js"></script>
    <script src="/Themes/vendor/datatables/buttons.print.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous"></script>

    <!-- Page level custom scripts -->
    <script>
        $(document).ready(function() {
            <?php foreach ($data as $key => $value) { ?>
                var table = $("#dataTable_<?= $key; ?>").DataTable({
                    ordering: false,
                    dom: "Bfrt",
                    buttons: [{
                            extend: 'copyHtml5',
                            footer: true,
                            text: 'Copy',
                            title: 'TARGET DAN REALISASI KEUANGAN ANGGARAN <?= $tahun; ?> ',

                        },
                        {
                            extend: 'excelHtml5',
                            footer: true,
                            text: 'Excel',
                            title: 'TARGET DAN REALISASI KEUANGAN ANGGARAN <?= $tahun; ?> ',

                        }, {
                            extend: 'print',
                            footer: true,
                            text: 'Print',
                            autoPrint: false,
                            title: '<label style="display:block;text-align:center;line-height:150%;">TARGET DAN REALISASI KEUANGAN ANGGARAN <?= $tahun; ?> </br> </label>',
                            customize: function(win) {
                                $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                            }
                        }
                    ],
                });
            <?php } ?>
        });
    </script>
<?php endif;  ?>
<?= $this->endSection(); ?>