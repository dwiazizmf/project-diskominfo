<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<!-- Custom styles for this page -->
<link href="/Themes/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="/Themes/vendor/datatables/buttons.dataTables.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" />


<?php foreach ($data as $key => $value) {
    $jml_anggaran = (int)$value['anggaran_kas'][0]['bulan_1']
        + (int)$value['anggaran_kas'][0]['bulan_2']
        + (int)$value['anggaran_kas'][0]['bulan_3']
        + (int)$value['anggaran_kas'][0]['bulan_4']
        + (int)$value['anggaran_kas'][0]['bulan_5']
        + (int)$value['anggaran_kas'][0]['bulan_6']
        + (int)$value['anggaran_kas'][0]['bulan_7']
        + (int)$value['anggaran_kas'][0]['bulan_8']
        + (int)$value['anggaran_kas'][0]['bulan_9']
        + (int)$value['anggaran_kas'][0]['bulan_10']
        + (int)$value['anggaran_kas'][0]['bulan_11']
        + (int)$value['anggaran_kas'][0]['bulan_12'];
    $jml_sp2d = (int)$value['sp2d'][0]['bulan_1']
        + (int)$value['sp2d'][0]['bulan_2']
        + (int)$value['sp2d'][0]['bulan_3']
        + (int)$value['sp2d'][0]['bulan_4']
        + (int)$value['sp2d'][0]['bulan_5']
        + (int)$value['sp2d'][0]['bulan_6']
        + (int)$value['sp2d'][0]['bulan_7']
        + (int)$value['sp2d'][0]['bulan_8']
        + (int)$value['sp2d'][0]['bulan_9']
        + (int)$value['sp2d'][0]['bulan_10']
        + (int)$value['sp2d'][0]['bulan_11']
        + (int)$value['sp2d'][0]['bulan_12'];
    $jml_spj = (int)$value['spj'][0]['bulan_1']
        + (int)$value['spj'][0]['bulan_2']
        + (int)$value['spj'][0]['bulan_3']
        + (int)$value['spj'][0]['bulan_4']
        + (int)$value['spj'][0]['bulan_5']
        + (int)$value['spj'][0]['bulan_6']
        + (int)$value['spj'][0]['bulan_7']
        + (int)$value['spj'][0]['bulan_8']
        + (int)$value['spj'][0]['bulan_9']
        + (int)$value['spj'][0]['bulan_10']
        + (int)$value['spj'][0]['bulan_11']
        + (int)$value['spj'][0]['bulan_12'];

?>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><?= $bidang[$value['id_bidang']]; ?></h6>
        </div>
        <div class="card-body">
            <?= $this->include('layout/Alert.php'); ?>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable_<?= $value['id']; ?>" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>BULAN</th>
                            <th>JANUARI</th>
                            <th>FEBRUARI</th>
                            <th>MARET</th>
                            <th>APRIL</th>
                            <th>MEI</th>
                            <th>JUNI</th>
                            <th>JULI</th>
                            <th>AGUSTUS</th>
                            <th>SEPTEMBER</th>
                            <th>OKTOBER</th>
                            <th>NOVEMBER</th>
                            <th>DESEMBER</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Anggaran (Rp)</td>
                            <td class="text-right"><?= number_format($value['anggaran_kas'][0]['bulan_1'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['anggaran_kas'][0]['bulan_2'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['anggaran_kas'][0]['bulan_3'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['anggaran_kas'][0]['bulan_4'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['anggaran_kas'][0]['bulan_5'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['anggaran_kas'][0]['bulan_6'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['anggaran_kas'][0]['bulan_7'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['anggaran_kas'][0]['bulan_8'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['anggaran_kas'][0]['bulan_9'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['anggaran_kas'][0]['bulan_10'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['anggaran_kas'][0]['bulan_11'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['anggaran_kas'][0]['bulan_12'], 0, ",", "."); ?></td>
                        </tr>
                        <tr>
                            <td>SP2D (Rp)</td>
                            <td class="text-right"><?= number_format($value['sp2d'][0]['bulan_1'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['sp2d'][0]['bulan_2'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['sp2d'][0]['bulan_3'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['sp2d'][0]['bulan_4'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['sp2d'][0]['bulan_5'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['sp2d'][0]['bulan_6'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['sp2d'][0]['bulan_7'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['sp2d'][0]['bulan_8'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['sp2d'][0]['bulan_9'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['sp2d'][0]['bulan_10'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['sp2d'][0]['bulan_11'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['sp2d'][0]['bulan_12'], 0, ",", "."); ?></td>
                        </tr>
                        <tr>
                            <td>SPJ (Rp)</td>
                            <td class="text-right"><?= number_format($value['spj'][0]['bulan_1'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['spj'][0]['bulan_2'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['spj'][0]['bulan_3'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['spj'][0]['bulan_4'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['spj'][0]['bulan_5'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['spj'][0]['bulan_6'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['spj'][0]['bulan_7'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['spj'][0]['bulan_8'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['spj'][0]['bulan_9'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['spj'][0]['bulan_10'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['spj'][0]['bulan_11'], 0, ",", "."); ?></td>
                            <td class="text-right"><?= number_format($value['spj'][0]['bulan_12'], 0, ",", "."); ?></td>
                        </tr>
                        <tr>
                            <td>Anggaran (%)</td>
                            <td class="text-right"><?= ((int)$value['anggaran_kas'][0]['bulan_12'] > 0 && (int)$value['anggaran_kas'][0]['bulan_1'] > 0) ? round(((int)$value['anggaran_kas'][0]['bulan_1'] / (int)$value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ((int)$value['anggaran_kas'][0]['bulan_12'] > 0 && (int)$value['anggaran_kas'][0]['bulan_2'] > 0) ? round(((int)$value['anggaran_kas'][0]['bulan_2'] / (int)$value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ((int)$value['anggaran_kas'][0]['bulan_12'] > 0 && (int)$value['anggaran_kas'][0]['bulan_3'] > 0) ? round(((int)$value['anggaran_kas'][0]['bulan_3'] / (int)$value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ((int)$value['anggaran_kas'][0]['bulan_12'] > 0 && (int)$value['anggaran_kas'][0]['bulan_4'] > 0) ? round(((int)$value['anggaran_kas'][0]['bulan_4'] / (int)$value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ((int)$value['anggaran_kas'][0]['bulan_12'] > 0 && (int)$value['anggaran_kas'][0]['bulan_5'] > 0) ? round(((int)$value['anggaran_kas'][0]['bulan_5'] / (int)$value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ((int)$value['anggaran_kas'][0]['bulan_12'] > 0 && (int)$value['anggaran_kas'][0]['bulan_6'] > 0) ? round(((int)$value['anggaran_kas'][0]['bulan_6'] / (int)$value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ((int)$value['anggaran_kas'][0]['bulan_12'] > 0 && (int)$value['anggaran_kas'][0]['bulan_7'] > 0) ? round(((int)$value['anggaran_kas'][0]['bulan_7'] / (int)$value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ((int)$value['anggaran_kas'][0]['bulan_12'] > 0 && (int)$value['anggaran_kas'][0]['bulan_8'] > 0) ? round(((int)$value['anggaran_kas'][0]['bulan_8'] / (int)$value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ((int)$value['anggaran_kas'][0]['bulan_12'] > 0 && (int)$value['anggaran_kas'][0]['bulan_9'] > 0) ? round(((int)$value['anggaran_kas'][0]['bulan_9'] / (int)$value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ((int)$value['anggaran_kas'][0]['bulan_12'] > 0 && (int)$value['anggaran_kas'][0]['bulan_10'] > 0) ? round(((int)$value['anggaran_kas'][0]['bulan_10'] / (int)$value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ((int)$value['anggaran_kas'][0]['bulan_12'] > 0 && (int)$value['anggaran_kas'][0]['bulan_11'] > 0) ? round(((int)$value['anggaran_kas'][0]['bulan_11'] / (int)$value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ((int)$value['anggaran_kas'][0]['bulan_12'] > 0 && (int)$value['anggaran_kas'][0]['bulan_12'] > 0) ? round(((int)$value['anggaran_kas'][0]['bulan_12'] / (int)$value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                        </tr>
                        <tr>
                            <td>SP2D (%)</td>
                            <td class="text-right"><?= ((int)$value['anggaran_kas'][0]['bulan_12'] > 0 && $value['sp2d'][0]['bulan_1'] > 0) ? round(($value['sp2d'][0]['bulan_1'] / (int)$value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ((int)$value['anggaran_kas'][0]['bulan_12'] > 0 && $value['sp2d'][0]['bulan_2'] > 0) ? round(($value['sp2d'][0]['bulan_2'] / (int)$value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ((int)$value['anggaran_kas'][0]['bulan_12'] > 0 && $value['sp2d'][0]['bulan_3'] > 0) ? round(($value['sp2d'][0]['bulan_3'] / (int)$value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ((int)$value['anggaran_kas'][0]['bulan_12'] > 0 && $value['sp2d'][0]['bulan_4'] > 0) ? round(($value['sp2d'][0]['bulan_4'] / (int)$value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ((int)$value['anggaran_kas'][0]['bulan_12'] > 0 && $value['sp2d'][0]['bulan_5'] > 0) ? round(($value['sp2d'][0]['bulan_5'] / (int)$value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ((int)$value['anggaran_kas'][0]['bulan_12'] > 0 && $value['sp2d'][0]['bulan_6'] > 0) ? round(($value['sp2d'][0]['bulan_6'] / (int)$value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ((int)$value['anggaran_kas'][0]['bulan_12'] > 0 && $value['sp2d'][0]['bulan_7'] > 0) ? round(($value['sp2d'][0]['bulan_7'] / (int)$value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ((int)$value['anggaran_kas'][0]['bulan_12'] > 0 && $value['sp2d'][0]['bulan_8'] > 0) ? round(($value['sp2d'][0]['bulan_8'] / (int)$value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ((int)$value['anggaran_kas'][0]['bulan_12'] > 0 && $value['sp2d'][0]['bulan_9'] > 0) ? round(($value['sp2d'][0]['bulan_9'] / (int)$value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ((int)$value['anggaran_kas'][0]['bulan_12'] > 0 && $value['sp2d'][0]['bulan_10'] > 0) ? round(($value['sp2d'][0]['bulan_10'] / (int)$value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ((int)$value['anggaran_kas'][0]['bulan_12'] > 0 && $value['sp2d'][0]['bulan_11'] > 0) ? round(($value['sp2d'][0]['bulan_11'] / (int)$value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ((int)$value['anggaran_kas'][0]['bulan_12'] > 0 && $value['sp2d'][0]['bulan_12'] > 0) ? round(($value['sp2d'][0]['bulan_12'] / (int)$value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                        </tr>
                        <tr>
                            <td>SPJ (%)</td>
                            <td class="text-right"><?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['spj'][0]['bulan_1'] > 0) ? round(($value['spj'][0]['bulan_1'] / $value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['spj'][0]['bulan_2'] > 0) ? round(($value['spj'][0]['bulan_2'] / $value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['spj'][0]['bulan_3'] > 0) ? round(($value['spj'][0]['bulan_3'] / $value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['spj'][0]['bulan_4'] > 0) ? round(($value['spj'][0]['bulan_4'] / $value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['spj'][0]['bulan_5'] > 0) ? round(($value['spj'][0]['bulan_5'] / $value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['spj'][0]['bulan_6'] > 0) ? round(($value['spj'][0]['bulan_6'] / $value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['spj'][0]['bulan_7'] > 0) ? round(($value['spj'][0]['bulan_7'] / $value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['spj'][0]['bulan_8'] > 0) ? round(($value['spj'][0]['bulan_8'] / $value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['spj'][0]['bulan_9'] > 0) ? round(($value['spj'][0]['bulan_9'] / $value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['spj'][0]['bulan_10'] > 0) ? round(($value['spj'][0]['bulan_10'] / $value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['spj'][0]['bulan_11'] > 0) ? round(($value['spj'][0]['bulan_11'] / $value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                            <td class="text-right"><?= ($value['anggaran_kas'][0]['bulan_12'] > 0 && $value['spj'][0]['bulan_12'] > 0) ? round(($value['spj'][0]['bulan_12'] / $value['anggaran_kas'][0]['bulan_12']) * 100, 2) . "%" : ''; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php } ?>
<!-- Page level plugins -->
<script src="/Themes/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.buttons.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/jszip.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.html5.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.print.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous"></script>

<!-- Page level custom scripts -->
<script>
    $(document).ready(function() {
        <?php foreach ($data as $key => $value) { ?>
            var table = $("#dataTable_<?= $value['id']; ?>").DataTable({
                ordering: false,
                dom: "Bfrt",
                buttons: [{
                        extend: 'copyHtml5',
                        footer: true,
                        text: 'Copy',
                        title: 'TARGET DAN REALISASI KEUANGAN ANGGARAN <?= $tahun; ?> BIDANG <?= $bidang[$value['id_bidang']]; ?>',

                    },
                    {
                        extend: 'excelHtml5',
                        footer: true,
                        text: 'Excel',
                        title: 'TARGET DAN REALISASI KEUANGAN ANGGARAN <?= $tahun; ?> BIDANG <?= $bidang[$value['id_bidang']]; ?>',

                    }, {
                        extend: 'print',
                        footer: true,
                        text: 'Print',
                        autoPrint: false,
                        title: '<label style="display:block;text-align:center;line-height:150%;">TARGET DAN REALISASI KEUANGAN ANGGARAN <?= $tahun; ?> </br> BIDANG <?= $bidang[$value['id_bidang']]; ?></label>',
                        customize: function(win) {
                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ],
            });
        <?php } ?>
    });
</script>

<?= $this->endSection(); ?>