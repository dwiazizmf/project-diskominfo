<div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>No</th>
                <th>Bidang</th>
                <th>DPA</th>
                <th>SP2D</th>
                <th>SPJ</th>
                <th>SPJ/DPA</th>
                <th>SP2D/DPA</th>
                <th>SISA DPA</th>
                <th>SISA PAGU ANGGARAN</th>
                <th>SISA KAS</th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 1;
            foreach ($data as $value) : ?>
                <tr>
                    <td><?= $no; ?></td>
                    <td><?= $bidang[$value['id_bidang']]; ?></td>
                    <td><?= $value['jumlah_dpa']; ?></td>
                    <td><?= $value['sp2d']; ?></td>
                    <td><?= $value['spj']; ?></td>
                    <td><?= $value['spj/dpa']; ?></td>
                    <td><?= $value['sp2d/dpa']; ?></td>
                    <td><?= $value['sisa_dpa']; ?></td>
                    <td><?= $value['sisa_pagu_anggaran']; ?></td>
                    <td><?= $value['sisa_kas']; ?></td>
                </tr>
            <?php $no++;
            endforeach; ?>
        </tbody>
    </table>
</div>