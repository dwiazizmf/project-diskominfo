<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<!-- Custom styles for this page -->
<link href="/Themes/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="/Themes/vendor/datatables/buttons.dataTables.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" />

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">List Data</h6>
    </div>
    <div class="card-body">
        <?= $this->include('layout/Alert.php'); ?>
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Uraian</th>
                        <th>Jumlah DPA</th>
                        <th>Jumlah SP2D</th>
                        <th>Realisasi SPJ</th>
                        <th>SPJ/DPA</th>
                        <th>SP2D/DPA</th>
                        <th>SISA DPA</th>
                        <th>SISA PAGU ANGGARAN</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Belanja</td>
                        <td class="text-right"><?= number_format($jml_dpa_all, 0, ",", "."); ?></td>
                        <td class="text-right"><?= number_format($jml_sp2d_all, 0, ",", "."); ?></td>
                        <td class="text-right"><?= number_format($jml_spj_all, 0, ",", "."); ?></td>
                        <td class="text-right"><?= round($jml_spjdpa_all, 2); ?>%</td>
                        <td class="text-right"><?= round($jml_sp2ddpa_all, 2); ?>%</td>
                        <td class="text-right"><?= number_format($jml_sisa_dpa_all, 0, ",", "."); ?></td>
                        <td class="text-right"><?= number_format($jml_sisa_pagu_all, 0, ",", "."); ?></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>Belanja Tidak Langsung</td>
                        <td class="text-right"><?= number_format($jml_dpa_gaji, 0, ",", "."); ?></td>
                        <td class="text-right"><?= number_format($jml_sp2d_gaji, 0, ",", "."); ?></td>
                        <td class="text-right"><?= number_format($jml_spj_gaji, 0, ",", "."); ?></td>
                        <td class="text-right"><?= round($jml_spjdpa_gaji, 2); ?>%</td>
                        <td class="text-right"><?= round($jml_sp2ddpa_gaji, 2); ?>%</td>
                        <td class="text-right"><?= number_format($jml_sisa_dpa_gaji, 0, ",", "."); ?></td>
                        <td class="text-right"><?= number_format($jml_sisa_pagu_gaji, 0, ",", "."); ?></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>Belanja Langsung</td>
                        <td class="text-right"><?= number_format($jml_dpa, 0, ",", "."); ?></td>
                        <td class="text-right"><?= number_format($jml_sp2d, 0, ",", "."); ?></td>
                        <td class="text-right"><?= number_format($jml_spj, 0, ",", "."); ?></td>
                        <td class="text-right"><?= round($jml_spjdpa, 2); ?>%</td>
                        <td class="text-right"><?= round($jml_sp2ddpa, 2); ?>%</td>
                        <td class="text-right"><?= number_format($jml_sisa_dpa, 0, ",", "."); ?></td>
                        <td class="text-right"><?= number_format($jml_sisa_pagu, 0, ",", "."); ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Page level plugins -->
<script src="/Themes/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.buttons.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/jszip.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.html5.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.print.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous"></script>

<!-- Page level custom scripts -->
<script>
    $(document).ready(function() {
        var table = $("#dataTable").DataTable({
            order: false,
            dom: "Bfrt",
            buttons: [{
                    extend: 'copyHtml5',
                    footer: true,
                    text: 'Copy',
                    title: 'PENYERAPAN PER BIDANG BULAN <?= $bulan; ?> <?= $tahun; ?>',
                }, {
                    extend: 'excelHtml5',
                    footer: true,
                    text: 'Excel',
                    title: 'PENYERAPAN PER BIDANG BULAN <?= $bulan; ?> <?= $tahun; ?>',
                },
                {
                    extend: 'print',
                    footer: true,
                    text: 'Print',
                    autoPrint: false,
                    title: '<label style="display:block;text-align:center;line-height:150%;">PENYERAPAN PER BIDANG </br> BULAN <?= $bulan; ?> <?= $tahun; ?></label>',
                    customize: function(win) {
                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ],
        });
    });
</script>

<?= $this->endSection(); ?>