<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>

<!-- Custom styles for this page -->
<link href="/Themes/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="/Themes/vendor/datatables/buttons.dataTables.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" />

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">List Data</h6>
    </div>
    <div class="card-body">
        <?= $this->include('layout/Alert.php'); ?>
        <form action="/files/store" enctype="multipart/form-data" method="POST">
            <?= csrf_field(); ?>
            <div class="form-group row">
                <label for="nama_file" class="col-sm-2 col-form-label">Nama File</label>
                <div class="col-sm-10">
                    <input type="text" placeholder="Input Nama File" value="<?= old('nama_file'); ?>" class="form-control <?= ($validation->hasError('nama_file')) ? 'is-invalid' : ''; ?>" name="nama_file" id="nama_file" <?= ($validation->hasError('nama_file')) ? 'autofocus' : ''; ?>>
                    <div id="validation_nama_file" class="invalid-feedback">
                        <?= $validation->getError('nama_file'); ?>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="keterangan" class="col-sm-2 col-form-label">Keterangan</label>
                <div class="col-sm-10">
                    <textarea placeholder="Input keterangan" class="form-control <?= ($validation->hasError('keterangan')) ? 'is-invalid' : ''; ?>" name="keterangan" id="keterangan" <?= ($validation->hasError('keterangan')) ? 'autofocus' : ''; ?>><?= old('keterangan'); ?></textarea>
                    <div id="validation_keterangan" class="invalid-feedback">
                        <?= $validation->getError('keterangan'); ?>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="file-upload" class="col-sm-2 col-form-label">File</label>
                <div class="col-sm-10">
                    <div class="custom-file">
                        <input type="file" onchange="labeling()" name="file-upload" class="custom-file-input <?= ($validation->hasError('file-upload')) ? 'is-invalid' : ''; ?>" id="file-upload" <?= ($validation->hasError('file-upload')) ? 'autofocus' : ''; ?>>
                        <label class="custom-file-label" id="label-upload" for="file-upload">Pilih File ..</label>
                        <div id="validationExcel" class="invalid-feedback">
                            <?= $validation->getError('file-upload'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </div>
        </form>

    </div>
</div>

<!-- Page level plugins -->
<script src="/Themes/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/dataTables.buttons.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.bootstrap4.min.js"></script>
<script src="/Themes/vendor/datatables/jszip.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.html5.min.js"></script>
<script src="/Themes/vendor/datatables/buttons.print.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous"></script>


<!-- Page level custom scripts -->
<script src="/js/Salary.js"></script>

<?= $this->endSection(); ?>