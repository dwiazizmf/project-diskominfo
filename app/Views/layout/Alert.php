<?php if (session()->getFlashData('result')) :
    $result = session()->getFlashData('result');
?>
    <div class="alert alert-<?= $result['status'] ?> alert-dismissible fade show" role="alert">
        <?= $result['message']; ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php endif; ?>