<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="dashboard">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-file"></i>
        </div>
        <div class="sidebar-brand-text mx-3">SIMPEL-KEUN</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="/users/print_gaji">
            <i class="fas fa-fw fa-file"></i>
            <span>Download</span>
        </a>
        <?php if ($_SESSION['data']['id_jabatan'] == 6 || $_SESSION['data']['id_jabatan'] == 2 || $_SESSION['data']['id_jabatan'] == 3) { ?>
            <a class="nav-link" href="/dashboard">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span>
            </a>
        <?php } ?>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Setting
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <?php if ($_SESSION['data']['id_jabatan'] == 3) { ?>
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUsers" aria-expanded="true" aria-controls="collapseUsers">
                <i class="fas fa-fw fa-user"></i>
                <span>Manajemen User</span>
            </a>
            <div id="collapseUsers" class="collapse" aria-labelledby="headingUsers" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Manajemen User</h6>
                    <a class="collapse-item" href="/users">User List</a>
                    <!--<a class="collapse-item" href="/roles">User Roles</a>-->
                </div>
            </div>
        </li>
    <?php } ?>
    <?php if ($_SESSION['data']['id_jabatan'] == 5) { ?>
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseGaji_" aria-expanded="true" aria-controls="collapseGaji_">
                <i class="fas fa-fw fa-hands-helping"></i>
                <span>Gaji</span>
            </a>
            <div id="collapseGaji_" class="collapse" aria-labelledby="headingGaji_" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Gaji</h6>
                    <?php if ($_SESSION['data']['id_jabatan'] == 5 || $_SESSION['data']['id_jabatan'] == 2) { ?>
                        <a class="collapse-item" href="/gaji">Slip Gaji</a>
                        <a class="collapse-item" href="/salary">Gaji</a>
                        <a class="collapse-item" href="/bbm">BBM</a>
                        <a class="collapse-item" href="/kompensasi">Kompensasi</a>
                        <a class="collapse-item" href="/tpp">TPP</a>
                    <?php } ?>
                </div>
            </div>
        </li>
    <?php }
    if ($_SESSION['data']['id_jabatan'] == 3 || $_SESSION['data']['id_jabatan'] == 2) { ?>
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseGaji" aria-expanded="true" aria-controls="collapseGaji">
                <i class="fas fa-fw fa-hands-helping"></i>
                <span>Realisasi Dinas</span>
            </a>
            <div id="collapseGaji" class="collapse" aria-labelledby="headingGaji" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Realisasi Dinas</h6>
                    <a class="collapse-item" href="/anggaran_gaji">BTL</a>
                    <a class="collapse-item" href="/report/bulan_gaji">Per Bulan</a>
                    <a class="collapse-item" href="/report/tahun_gaji">Per Tahun</a>
                </div>
            </div>
        </li>
    <?php } ?>
    <?php if ($_SESSION['data']['id_jabatan'] == 1 || $_SESSION['data']['id_jabatan'] == 3 || $_SESSION['data']['id_jabatan'] == 2 || $_SESSION['data']['id_jabatan'] == 6) { ?>
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseRealisasi" aria-expanded="true" aria-controls="collapseRealisasi">
                <i class="fas fa-fw fa-chart-area"></i>
                <span>Realisasi Unit/Bidang</span>
            </a>
            <div id="collapseRealisasi" class="collapse" aria-labelledby="headingRealisasi" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Realisasi Unit/Bidang</h6>
                    <?php if ($_SESSION['data']['id_jabatan'] == 3 || $_SESSION['data']['id_jabatan'] == 2 || $_SESSION['data']['id_jabatan'] == 6) { ?>
                        <a class="collapse-item" href="/anggaran">Realisasi</a>
                        <a class="collapse-item" href="/pelimpahan">Pelimpahan</a>
                    <?php } ?>

                    <?php if ($_SESSION['data']['id_jabatan'] == 3 || $_SESSION['data']['id_jabatan'] == 6 || $_SESSION['data']['id_jabatan'] == 2 || $_SESSION['data']['id_jabatan'] == 1) { ?>
                        <a class="collapse-item" href="/report/bulan">Per Bulan</a>

                    <?php }
                    if ($_SESSION['data']['id_jabatan'] == 3 || $_SESSION['data']['id_jabatan'] == 6 || $_SESSION['data']['id_jabatan'] == 2) { ?>
                        <a class="collapse-item" href="/report/tahun">Per Tahun</a>
                    <?php } ?>
                    <!-- <a class="collapse-item" href="/kompensasi">Kompensasi</a>
                    <a class="collapse-item" href="/tpp">TPP 5</a> -->
                </div>
            </div>
        </li>
    <?php } ?>
    <!--
    <?php if ($_SESSION['data']['id_jabatan'] == 3 || $_SESSION['data']['id_jabatan'] == 6 || $_SESSION['data']['id_jabatan'] == 2 || $_SESSION['data']['id_jabatan'] == 1) { ?>
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseReport" aria-expanded="true" aria-controls="collapseReport">
                <i class="fas fa-fw fa-file-word"></i>
                <span>Report</span>
            </a>
            <div id="collapseReport" class="collapse" aria-labelledby="headingReport" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Report</h6>
                    <?php if ($_SESSION['data']['id_jabatan'] == 3 || $_SESSION['data']['id_jabatan'] == 6 || $_SESSION['data']['id_jabatan'] == 2 || $_SESSION['data']['id_jabatan'] == 1) { ?>
                        <a class="collapse-item" href="/report/bulan">Per Bulan</a>

                    <?php }
                    if ($_SESSION['data']['id_jabatan'] == 3 || $_SESSION['data']['id_jabatan'] == 6 || $_SESSION['data']['id_jabatan'] == 2) { ?>
                        <a class="collapse-item" href="/report/tahun">Per Tahun</a>
                    <?php } ?>
                </div>
            </div>
        </li>
        <?php } ?>
                    -->
    <hr class="sidebar-divider my-0">


    <li class="nav-item">
        <?php if ($_SESSION['data']['id_jabatan'] == 2) { ?>
            <a class="nav-link" href="/files">
                <i class="fas fa-fw fa-folder"></i>
                <span>Files</span>
            </a>
        <?php } ?>
        <a class="nav-link" href="/users/edit_password/<?= $_SESSION['data']['unique_id']; ?>">
            <i class="fas fa-fw fa-lock"></i>
            <span>Ubah Password</span>
        </a>
    </li>
    <!-- Nav Item - Utilities Collapse Menu -->
    <!-- <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Utilities</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Custom Utilities:</h6>
                <a class="collapse-item" href="utilities-color.html">Colors</a>
                <a class="collapse-item" href="utilities-border.html">Borders</a>
                <a class="collapse-item" href="utilities-animation.html">Animations</a>
                <a class="collapse-item" href="utilities-other.html">Other</a>
            </div>
        </div>
    </li> -->

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <!-- <div class="sidebar-heading">
        Addons
    </div> -->

    <!-- Nav Item - Pages Collapse Menu -->
    <!-- <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
            <i class="fas fa-fw fa-folder"></i>
            <span>Pages</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Login Screens:</h6>
                <a class="collapse-item" href="login.html">Login</a>
                <a class="collapse-item" href="register.html">Register</a>
                <a class="collapse-item" href="forgot-password.html">Forgot Password</a>
                <div class="collapse-divider"></div>
                <h6 class="collapse-header">Other Pages:</h6>
                <a class="collapse-item" href="404.html">404 Page</a>
                <a class="collapse-item" href="blank.html">Blank Page</a>
            </div>
        </div>
    </li> -->

    <!-- Nav Item - Charts -->
    <!-- <li class="nav-item">
        <a class="nav-link" href="charts.html">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Charts</span></a>
    </li> -->

    <!-- Nav Item - Tables -->
    <!-- <li class="nav-item">
        <a class="nav-link" href="tables.html">
            <i class="fas fa-fw fa-table"></i>
            <span>Tables</span></a>
    </li> -->

    <!-- Divider -->
    <!-- <hr class="sidebar-divider d-none d-md-block"> -->

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
    <div class="text-center d-none d-md-inline" style="margin-top:70px;">
        <img src="/Themes/img/Logodiskominfo.png" height="120" width="200" alt="">
    </div>

</ul>
<!-- End of Sidebar -->