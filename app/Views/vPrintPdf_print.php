 <!-- Custom fonts for this template-->
 <link href="/Themes/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
 <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

 <!-- Custom styles for this template-->
 <link href="/Themes/css/sb-admin-2.min.css" rel="stylesheet">

 <!-- Bootstrap core JavaScript-->
 <script src="/Themes/vendor/jquery/jquery.min.js"></script>
 <script src="/Themes/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

 <?php foreach ($data as $key => $value) { ?>
     <div class="container" style="background: white;padding-left: 30px;padding-right: 30px; padding-top: 100px;font-size:14px;">
         <div class="row">
             <div class="col-md-12 text-center">
                 <h5>KETERANGAN PENGHASILAN</h5>
             </div>
             <div class="col-md-12 text-center">
                 Yang bertanda tangan di bawah ini Pengelola Gaji Dinas Komunikasi dan Informatika Provinsi Jawa Barat, mengatakan bahwa :
             </div>
         </div>
         <div class="row">
             <div class="col-md-2 offset-md-2">
                 Nama
             </div>
             <div class="col-md-4 text-left">
                 : <?= $value['nama']; ?>
             </div>
         </div>
         <div class="row">
             <div class="col-md-2 offset-md-2">
                 NIP
             </div>
             <div class="col-md-4 text-left">
                 : <?= $value['nip']; ?>
             </div>
         </div>
         <div class="row">
             <div class="col-md-2 offset-md-2">
                 Pangkat / Golongan
             </div>
             <div class="col-md-6 text-left">
                 : <?= $value['pangkat'] . " (" . $value['golongan_pangkat'] . ")"; ?>
             </div>
         </div>
         <div class="row">
             <div class="col-md-2 offset-md-2">
                 Jabatan
             </div>
             <div class="col-md-6 text-left">
                 : <?= $value['nama_jabatan']; ?>
             </div>
         </div>
         <div class="row">
             <div class="col-md-2 offset-md-2">
                 Bulan
             </div>
             <div class="col-md-4 text-left">
                 : <?= $value['bulan_gaji']; ?>
             </div>
         </div>

         <div class="offset-md-1">
             <div class="row">
                 <div class="col-md-12 text-left">
                     <p>Mempunya Penghasilan sebagai berikut : </p>
                 </div>
             </div>
             <div class="row">
                 <div class="col-sm-1">
                     A.
                 </div>
                 <div class="col-md-3">
                     - Gaji Pokok
                 </div>
                 <div class="col-sm-1">
                     Rp.
                 </div>
                 <div class="col-md-2 text-right">
                     <?= number_format($value['gaji_pokok'], 2, ",", "."); ?>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-3 offset-sm-1">
                     - Tunjangan Istri / suami
                 </div>
                 <div class="col-sm-1">
                     Rp.
                 </div>
                 <div class="col-md-2 text-right">
                     <?= number_format($value['tunjangan_issu'], 2, ",", "."); ?>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-3 offset-sm-1">
                     - Tunjangan Anak
                 </div>
                 <div class="col-sm-1">
                     Rp.
                 </div>
                 <div class="col-md-2 text-right">
                     <?= number_format($value['tunjangan_anak'], 2, ",", "."); ?>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-3 offset-sm-1">
                     - Tunjangan Struktural
                 </div>
                 <div class="col-sm-1">
                     Rp.
                 </div>
                 <div class="col-md-2 text-right">
                     <?= number_format($value['tunjangan_struktural'], 2, ",", "."); ?>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-3 offset-sm-1">
                     - Tunjangan Beras
                 </div>
                 <div class="col-sm-1">
                     Rp.
                 </div>
                 <div class="col-md-2 text-right">
                     <?= number_format($value['tunjangan_beras'], 2, ",", "."); ?>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-3 offset-sm-1">
                     - Tunjangan Pajak Penghasilan
                 </div>
                 <div class="col-sm-1">
                     Rp.
                 </div>
                 <div class="col-md-2 text-right">
                     <?= number_format($value['tunjangan_pajak_penghasilan'], 2, ",", "."); ?>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-3 offset-sm-1">
                     - Pembulatan
                 </div>
                 <div class="col-sm-1">
                     Rp.
                 </div>
                 <div class="col-md-2 text-right border-bottom">
                     <?= number_format($value['pembulatan'], 2, ",", "."); ?>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-6 offset-sm-1">
                     &nbsp;&nbsp;<strong> Penghasilan Kotor .............................................................................. </strong>
                 </div>
                 <div class="col-sm-1" style="max-width: 5%;">
                     <strong> Rp. </strong>
                 </div>
                 <div class="col-md-2 text-right" style="max-width: 15%;">
                     <strong><?= number_format($value['penghasilan_kotor'], 2, ",", "."); ?></strong>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-3 offset-sm-1">
                     Potongan :
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-3 offset-sm-1">
                     - IWP
                 </div>
                 <div class="col-sm-1">
                     Rp.
                 </div>
                 <div class="col-md-2 text-right">
                     <?= number_format($value['iwp'], 2, ",", "."); ?>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-3 offset-sm-1">
                     - ASKES
                 </div>
                 <div class="col-sm-1">
                     Rp.
                 </div>
                 <div class="col-md-2 text-right">
                     <?= number_format($value['askes'], 2, ",", "."); ?>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-3 offset-sm-1">
                     - PPH 21
                 </div>
                 <div class="col-sm-1">
                     Rp.
                 </div>
                 <div class="col-md-2 text-right">
                     <?= number_format($value['pph21'], 2, ",", "."); ?>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-3 offset-sm-1">
                     - Tabungan Bapertarum
                 </div>
                 <div class="col-sm-1">
                     Rp.
                 </div>
                 <div class="col-md-2 text-right border-bottom">
                     <?= number_format($value['bapetarum'], 2, ",", "."); ?>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-6 offset-sm-1">
                     &nbsp;&nbsp;<strong>Jumlah Potongan .................................................................................</strong>
                 </div>
                 <div class="col-sm-1" style="max-width: 5%;">
                     <strong> Rp. </strong>
                 </div>
                 <div class="col-sm-2 text-right border-bottom" style="max-width: 15%;">
                     <strong><?= number_format($value['jumlah_potongan'], 2, ",", "."); ?></strong>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-6 offset-sm-1">
                     &nbsp;&nbsp;<strong>Jumlah Penghasilan Bersih* ..............................................................</strong>
                 </div>
                 <div class="col-sm-1" style="max-width: 5%;">
                     <strong> Rp. </strong>
                 </div>
                 <div class="col-md-2 text-right" style="max-width: 15%;">
                     <strong><?= number_format($value['penghasilan_bersih'], 2, ",", "."); ?></strong>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-3 offset-sm-1">
                     Potongan dipindah bukukan :
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-3 offset-sm-1">
                     - Simpanan Kop. KOMPAK*
                 </div>
                 <div class="col-sm-1">
                     Rp.
                 </div>
                 <div class="col-md-2 text-right">
                     <?= number_format($value['sim_kompak'], 2, ",", "."); ?>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-3 offset-sm-1">
                     - Pot. Koperasi KOMPAK*
                 </div>
                 <div class="col-sm-1">
                     Rp.
                 </div>
                 <div class="col-md-2 text-right">
                     <?= number_format($value['pot_kompak'], 2, ",", "."); ?>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-3 offset-sm-1">
                     - Potongan Angsuran BJB*
                 </div>
                 <div class="col-sm-1">
                     Rp.
                 </div>
                 <div class="col-md-2 text-right">
                     <?= number_format($value['pot_bjb'], 2, ",", "."); ?>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-3 offset-sm-1">
                     - Potongan Dharma Wanita**
                 </div>
                 <div class="col-sm-1">
                     Rp.
                 </div>
                 <div class="col-md-2 text-right">
                     <?= number_format($value['pot_dw'], 2, ",", "."); ?>
                 </div>
             </div>
             <!--<div class="row">
                 <div class="col-md-3 offset-sm-1">
                     - Potongan KOPRI**
                 </div>
                 <div class="col-sm-1">
                     Rp.
                 </div>
                 <div class="col-md-2 text-right">
                     <?= number_format($value['pot_kopri'], 2, ",", "."); ?>
                 </div>
             </div> -->
             <div class="row">
                 <div class="col-md-6 offset-sm-1">
                     &nbsp;&nbsp;<strong>Jumlah Potongan Dipindahbukukan ................................................</strong>
                 </div>
                 <div class="col-sm-1" style="max-width: 5%;">
                     <strong>Rp.</strong>
                 </div>
                 <div class="col-md-2 text-right border-bottom" style="max-width: 15%;">
                     <strong><?= number_format($value['total_pot'], 2, ",", "."); ?></strong>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-6 offset-sm-1">
                     &nbsp;&nbsp;<strong>Jumlah Penghasilan A .........................................................................</strong>
                 </div>
                 <div class="col-sm-1" style="max-width: 5%;">
                     <strong>Rp.</strong>
                 </div>
                 <div class="col-md-2 text-right" style="max-width: 15%;">
                     <strong><?= number_format($value['gaji_set_pot'], 2, ",", "."); ?></strong>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-12 text-left">
                     <p><strong>Penghasilan tambahan : </strong></p>
                 </div>
             </div>
             <div class="row">
                 <div class="col-sm-1">
                     <strong>B.</strong>
                 </div>
                 <div class="col-md-5 ">
                     <strong>- Tunjangan Kompensasi Mobilitas (BBM)</strong>
                 </div>
                 <div class="col-sm-1 offset-sm-1" style="max-width: 5%;">
                     <strong>Rp.</strong>
                 </div>
                 <div class="col-md-2 text-right" style="max-width: 15%;">
                     <strong><?= number_format($value['bbm'], 2, ",", "."); ?></strong>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-3 offset-sm-1">
                     - Potongan PPh 21
                 </div>
                 <div class="col-sm-1">
                     Rp.
                 </div>
                 <div class="col-md-2 text-right border-bottom">
                     <?= number_format($value['bbm_pph21'], 2, ",", "."); ?>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-6 offset-sm-1">
                     &nbsp;&nbsp;<strong>Jumlah Potongan .................................................................................</strong>
                 </div>
                 <div class="col-sm-1" style="max-width: 5%;">
                     <strong>Rp.</strong>
                 </div>
                 <div class="col-md-2 text-right border-bottom" style="max-width: 15%;">
                     <strong><?= number_format($value['bbm_pph21'], 2, ",", "."); ?></strong>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-6 offset-sm-1">
                     &nbsp;&nbsp;<strong>Jumlah Penghasilan B .........................................................................</strong>
                 </div>
                 <div class="col-sm-1" style="max-width: 5%;">
                     <strong>Rp.</strong>
                 </div>
                 <div class="col-md-2 text-right" style="max-width: 15%;">
                     <strong><?= number_format($value['bbm_bersih'], 2, ",", "."); ?></strong>
                 </div>
             </div>
             <div class="row">
                 <div class="col-sm-1">
                     <strong>C.</strong>
                 </div>
                 <div class="col-md-5 ">
                     <strong>- Tunjangan Kompensasi Kerja PNS</strong>
                 </div>
                 <div class="col-sm-1 offset-sm-1" style="max-width: 5%;">
                     <strong>Rp.</strong>
                 </div>
                 <div class="col-md-2 text-right" style="max-width: 15%;">
                     <strong><?= number_format($value['kompensasi'], 2, ",", "."); ?></strong>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-3 offset-sm-1">
                     - Potongan PPh 21
                 </div>
                 <div class="col-sm-1">
                     Rp.
                 </div>
                 <div class="col-md-2 text-right">
                     <?= number_format($value['kompensasi_pph21'], 2, ",", "."); ?>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-3 offset-sm-1">
                     - Pot. Koperasi KOMPAK*
                 </div>
                 <div class="col-sm-1">
                     Rp.
                 </div>
                 <div class="col-md-2 text-right border-bottom">
                     <?= number_format($value['pot_kop_kompen'], 2, ",", "."); ?>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-6 offset-sm-1 ">
                     &nbsp;&nbsp;<strong>Jumlah Potongan .................................................................................</strong>
                 </div>
                 <div class="col-sm-1" style="max-width: 5%;">
                     <strong>Rp.</strong>
                 </div>
                 <div class="col-md-2 border-bottom text-right" style="max-width: 15%;">
                     <strong><?= number_format($value['jumlah_pot_komponen'], 2, ",", "."); ?></strong>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-6 offset-sm-1">
                     &nbsp;&nbsp;<strong>Jumlah Penghasilan C* .......................................................................</strong>
                 </div>
                 <div class="col-sm-1" style="max-width: 5%;">
                     <strong>Rp.</strong>
                 </div>
                 <div class="col-md-2 text-right" style="max-width: 15%;">
                     <strong><?= number_format($value['kompensasi_bersih'], 2, ",", "."); ?></strong>
                 </div>
             </div>
             <div class="row">
                 <div class="col-sm-1">
                     <strong>D.</strong>
                 </div>
                 <div class="col-md-4 ">
                     <strong>Tunjangan Perbaikan Penghasilan (TPP)</strong>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-3 offset-sm-1">
                     <strong>TPP Maksimal</strong>
                 </div>
                 <div class="col-sm-1">
                     Rp.
                 </div>
                 <div class="col-md-2 text-right">
                     <?= number_format($value['tpp'], 2, ",", "."); ?>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-2 offset-sm-1">
                     <strong>Proses Kinerja</strong>
                 </div>
                 <div class="col-sm-1">
                     <strong><?= $value['persen']; ?></strong>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-3 offset-sm-1">
                     <strong>TPP Bruto</strong>
                 </div>
                 <div class="col-sm-1">
                     Rp.
                 </div>
                 <div class="col-md-2 text-right">
                     <?= number_format($value['bruto'], 2, ",", "."); ?>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-3 offset-sm-1">
                     <strong>PPh 21</strong>
                 </div>
                 <div class="col-sm-1 ">
                     Rp.
                 </div>
                 <div class="col-md-2 text-right">
                     <?= number_format($value['pph21_tpp'], 2, ",", "."); ?>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-6 offset-sm-1">
                     <strong>TPP Netto*</strong>
                 </div>
                 <div class="col-sm-1" style="max-width: 5%;">
                     <strong>Rp.</strong>
                 </div>
                 <div class="col-md-2 text-right" style="max-width: 15%;">
                     <strong><?= number_format($value['tpp_net'], 2, ",", "."); ?></strong>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-3 offset-sm-1">
                     - Potongan Zakat*
                 </div>
                 <div class="col-sm-1">
                     Rp.
                 </div>
                 <div class="col-md-2 text-right">
                     <?= number_format($value['zakat'], 2, ",", "."); ?>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-3 offset-sm-1">
                     - Potongan Simpanan KPPS*
                 </div>
                 <div class="col-sm-1">
                     Rp.
                 </div>
                 <div class="col-md-2 text-right">
                     <?= number_format($value['kpps'], 2, ",", "."); ?>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-3 offset-sm-1">
                     - Potongan angsuran KPPS*
                 </div>
                 <div class="col-sm-1">
                     Rp.
                 </div>
                 <div class="col-md-2 text-right">
                     <?= number_format($value['a_kpps'], 2, ",", "."); ?>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-3 offset-sm-1">
                     - Potongan angsuran BJB*
                 </div>
                 <div class="col-sm-1">
                     Rp.
                 </div>
                 <div class="col-md-2 text-right">
                     <?= number_format($value['a_bjb'], 2, ",", "."); ?>
                 </div>
             </div>

             <div class="row">
                 <div class="col-md-3 offset-sm-1">
                     - Pot. Angsuran KOMPAK*
                 </div>
                 <div class="col-sm-1">
                     Rp.
                 </div>
                 <div class="col-md-2 text-right">
                     <?= number_format($value['a_kompak'], 2, ",", "."); ?>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-3 offset-sm-1">
                     - Pot. Angs. BJB Syariah*
                 </div>
                 <div class="col-sm-1">
                     Rp.
                 </div>
                 <div class="col-md-2 border-bottom text-right">
                     <?= number_format($value['a_syariah'], 2, ",", "."); ?>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-6 offset-sm-1">
                     &nbsp;&nbsp;<strong>Jumlah Potongan .................................................................................</strong>
                 </div>
                 <div class="col-sm-1" style="max-width: 5%;">
                     <strong>Rp.</strong>
                 </div>
                 <div class="col-md-2 text-right" style="max-width: 15%;">
                     <strong><?= number_format($value['a_total_pot'], 2, ",", "."); ?></strong>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-6 offset-sm-1">
                     &nbsp;&nbsp;<strong>Jumlah Penghasilan D .........................................................................</strong>
                 </div>
                 <div class="col-sm-1" style="max-width: 5%;">
                     <strong>Rp.</strong>
                 </div>
                 <div class="col-md-2 text-right" style="max-width: 15%;">
                     <strong><?= number_format($value['tpp_bersih'], 2, ",", "."); ?></strong>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-6 offset-sm-1">
                     &nbsp;&nbsp;<strong>Total Penghasilan .................................................................................</strong>
                 </div>
                 <div class="col-sm-1" style="max-width: 5%;">
                     <strong>Rp.</strong>
                 </div>
                 <div class="col-md-2 text-right" style="max-width: 15%;">
                     <strong><?= number_format($value['total_penerimaan_gaji'], 2, ",", "."); ?></strong>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-12 text-left">
                     <p>Catatan : </p>
                 </div>
             </div>
             <div class="row">
                 <div class="col-md-5 text-left">
                     <p>*Tercetak di Buku Tabungan </p>
                 </div>
                 <div class="col-md-6 text-right">
                     <?php $bulan = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'); ?>
                     <p>Bandung, <?= "&nbsp;&nbsp;&nbsp;&nbsp;" . $bulan[$value['bulan']] . " " . $value['tahun']; ?></p>
                 </div>
             </div>
             <div class="row">
                 <!-- <div class="col-md-5 text-left">
                    <p>*Tmt Bulan September 2020</p>
                </div> -->
                 <div class="col-md-4 text-right" style="margin-left: 520px;">
                     <p>Bendahara Gaji,</p>
                 </div>
             </div>
             <div class="row">
                 <!-- <div class="col-md-5 text-left">
                    <p>*Tmt Bulan September 2020</p>
                </div> -->
                 <div class="col-md-4 text-right" style="margin-left: 500px; margin-top: 40px;">
                     <p>T.Sukmana</p>
                 </div>
             </div>
         </div>
     </div>
 <?php } ?>
 <script>
     $(document).ready(function() {
         window.print();
         window.onafterprint = window.close();
     })
 </script>