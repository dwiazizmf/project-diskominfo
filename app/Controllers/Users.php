<?php

namespace App\Controllers;

use App\Models\mPegawai;
use App\Models\mGaji;

class Users extends BaseController
{
    protected $mPegawai;
    protected $mGaji;
    public function __construct()
    {
        $this->validation =  \Config\Services::validation();
        $this->mPegawai = new mPegawai();
        $this->mGaji = new mGaji();
    }
    public function index()
    {
        if (isset($_SESSION['data'])) {
            return redirect()->to(base_url('dashboard'));
        }

        $data = [
            'title' => 'Login - Diskominfo',
        ];

        return view('vLogin', $data);
    }

    public function print_salary()
    {
        if (!isset($_SESSION['data'])) {
            return redirect()->to(base_url());
        }
        $data = [
            'title' => 'Anggaran - Diskominfo',
            'page' => 'Print Gaji'
        ];

        if ($_SESSION['data'] != 3) {
            $data['insert'] = ['/users/print_pdf', 'Print Slip Gaji'];
        }

        // dd($_GET);

        if (isset($_GET['periode_search'])) {
            $periode = $_GET['periode_search'];
            $exp_ = explode("-", $periode);
            $tahun = $exp_[0];
            $bulan = $exp_[1];
        } else {
            $periode = date('Y-m');
            $tahun = date('Y');
            $bulan = date('n');
        }

        $data['data'] = $this->mGaji->where(['tahun' => $tahun, 'bulan' => $bulan, 'nip' => $_SESSION['data']['nip_pegawai']])->findAll();
        if (count($data['data']) < 1) {
            session()->setFlashdata('result', ['status' => 'danger', 'message' => 'Tidak ada data gaji pada bulan ini']);
        }
        return view('vPrintPdf', $data);
    }

    public function print_pdf()
    {
        if (!isset($_SESSION['data'])) {
            return redirect()->to(base_url());
        }

        if (isset($_GET['periode_search'])) {
            $periode = $_GET['periode_search'];
            $exp_ = explode("-", $periode);
            $tahun = $exp_[0];
            $bulan = $exp_[1];
        } else {
            $periode = date('Y-m');
            $tahun = date('Y');
            $bulan = date('n');
        }

        $data['data'] = $this->mGaji->where(['tahun' => $tahun, 'bulan' => $bulan, 'nip' => $_SESSION['data']['nip_pegawai']])->findAll();
        return view('vPrintPdf_print', $data);
    }

    public function edit_password($id)
    {
        if (!isset($_SESSION['data'])) {
            return redirect()->to(base_url());
        }
        $data = [
            'title' => 'Users - Diskominfo',
            'page' => 'Edit Password',
            'validation' => $this->validation,
            'data' => $this->mPegawai->where('unique_id', $id)->find()
        ];
        if (empty($data['data'])) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Data dengan id ' . $id . ' tidak di temukan');
        }
        return view('/Admin/Users/vEditPassword', $data);
    }

    public function update_password($id)
    {
        if (!isset($_SESSION['data'])) {
            return redirect()->to(base_url());
        }
        $data_update = $this->request->getVar();
        $cek_old = $this->mPegawai->find($id);

        if ($this->validation->run($data_update, 'editPassword') == FALSE) {
            if ($cek_old['password_pegawai'] != $data_update['old_password']) {
                session()->setFlashdata('err-old_password', 'Password lama salah');
            }
            return redirect()->to('/users/edit_password/' . $data_update['unique_id'])->withInput();
        } else {
            $data_update_['id'] = $id;
            $data_update_['password_pegawai'] = $data_update['password_pegawai'];
            $simpan = $this->mPegawai->save($data_update_);
            if (!$simpan) {
                $pesan = 'Gagal ubah password';
                $status_pesan = 'danger';
            } else {
                $pesan = 'Berhasil ubah password';
                $status_pesan = 'success';
            }
            session()->setFlashdata('result', ['status' => $status_pesan, 'message' => $pesan]);
            return redirect()->to('/users/print_gaji');
        }
    }
    //--------------------------------------------------------------------

}
