<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\mBbm;
use App\Models\mPegawai;
use App\Models\mBidang;
use App\Models\mAnggaran;
use App\Models\mAnggaranKas;
use App\Models\mRealisasisp2d;
use App\Models\mRealisasispj;
use App\Models\mPelimpahan;

class Home extends BaseController
{
    protected $validation;
    protected $mBbm;
    protected $mBidang;
    protected $mAnggaran;
    protected $mAnggaranKas;
    protected $mRealisasisp2d;
    protected $mRealisasispj;
    protected $mPegawai;
    protected $mPelimpahan;

    public function __construct()
    {
        session();
        if (!isset($_SESSION['data'])) {
            header('Location: ' . base_url());
            exit();
        }

        $this->validation =  \Config\Services::validation();
        $this->mBbm = new mBbm();
        $this->mBidang = new mBidang();
        $this->mAnggaran = new mAnggaran();
        $this->mAnggaranKas = new mAnggaranKas();
        $this->mRealisasisp2d = new mRealisasisp2d();
        $this->mRealisasispj = new mRealisasispj();
        $this->mPegawai = new mPegawai();
        $this->mPelimpahan = new mPelimpahan();
    }

    public function index()
    {
        if ($_SESSION['data']['id_jabatan'] != 6 && $_SESSION['data']['id_jabatan'] != 2 && $_SESSION['data']['id_jabatan'] != 3) {

            return redirect()->to(base_url('/users/print_gaji'));
        }

        $data = [
            'title' => 'Dashboard - Diskominfo',
            'page' => 'Dashboard'
        ];

        if (isset($_GET['periode_search'])) {
            $tahun = $_GET['periode_search'];
        } else {
            $tahun = date('Y');
        }

        $bidang = $this->mBidang->findAll();
        foreach ($bidang as $value) {
            $data_bidang[$value['id']] = $value['nama_bidang'];
        }

        $data_angaran = $this->mAnggaran->where('tahun_anggaran', $tahun)->findAll();
        foreach ($data_angaran as $key => $value_) {
            $data_spj = $this->mRealisasispj->where('id_anggaran', $value_['id'])->findAll();
            $bulan = date('n');
            for ($i = $bulan; $i > 0; $i--) {
                if ($data_spj[0]['bulan_' . $i] > 0) {
                    $data_spj_isi = $data_spj[0]['bulan_' . $i];
                    //echo $data_spj_isi . " - " . $i . "</br>";
                    break;
                } else {
                    continue;
                }
            }
            $data_anggaranKas = $this->mAnggaranKas->where('id_anggaran', $value_['id'])->findAll();

            $data_kotak[] = array('bidang' => $data_bidang[$value_['id_bidang']], 'spj' => $data_spj_isi, 'persen' => ($data_spj_isi / $data_anggaranKas[0]['bulan_12']) * 100);
        }

        $data['data'] = $data_kotak;

        $data_ = $this->mAnggaran->where('tahun_anggaran', $tahun)->findAll();

        foreach ($data_ as $key => $value) {
            $data_mentah = array();
            $data_mentah['id'] = $value['id'];
            $data_mentah['id_bidang'] = $value['id_bidang'];
            $data_mentah['anggaran_kas'] = $this->mAnggaranKas->where('id_anggaran', $value['id'])->find();
            $data_mentah['sp2d'] = $this->mRealisasisp2d->where('id_anggaran', $value['id'])->find();
            $data_mentah['spj'] = $this->mRealisasispj->where('id_anggaran', $value['id'])->find();
            $data['data_'][] = $data_mentah;
        }


        $data['bidang'] = $data_bidang;
        $data['tahun'] = $tahun;
        $data['color'] = array('primary', 'secondary', 'success', 'danger', 'warning', 'info', 'dark', 'primary', 'secondary', 'success', 'danger', 'warning', 'info', 'dark');

        return view('/Admin/vHome', $data);
    }

    //--------------------------------------------------------------------

}
