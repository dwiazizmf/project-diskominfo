<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\mFileUpload;
use App\Models\mGaji;
use App\Models\mPegawai;

class FileUpload extends BaseController
{
    protected $validation;
    protected $mGaji;
    protected $mPegawai;
    protected $mFileUpload;
    public function __construct()
    {
        session();
        if (!isset($_SESSION['data'])) {
            header('Location: ' . base_url());
            exit();
        }
        if ($_SESSION['data']['id_jabatan'] != 2) {
            header('Location: ' . base_url('/users/print_gaji'));
            exit();
        }
        $this->validation =  \Config\Services::validation();
        $this->mGaji = new mGaji();
        $this->mPegawai = new mPegawai();
        $this->mFileUpload = new mFileUpload();
    }



    public function index()
    {
        $data = [
            'title' => 'Files - Diskominfo',
            'page' => 'List Data File',
            'insert' => ['/files/import', 'Import File'],
            'data' => $this->mFileUpload->findAll()
        ];

        $pegawai = $this->mPegawai->findAll();
        foreach ($pegawai as $value) {
            $data_pegawai[$value['id']] = $value['nama_pegawai'];
        }

        $data['pegawai'] = $data_pegawai;

        return view('/Admin/Files/vFiles', $data);
    }

    public function download($id)
    {
        $data_ = $this->mFileUpload->find($id);
        return $this->response->download('FileUpload/' . $data_['file_name'], null);
    }

    public function delete($id)
    {
        $data_ = $this->mFileUpload->find($id);
        $delete = $this->mFileUpload->delete($id);
        if (!$delete) {
            $pesan = 'data gagal di delete';
            $status_pesan = 'danger';
        } else {
            unlink('FileUpload/' . $data_['file_name']);
            $pesan = 'Berhasil hapus data';
            $status_pesan = 'success';
        }
        session()->setFlashdata('result', ['status' => $status_pesan, 'message' => $pesan]);
        return redirect()->to(base_url('/files'));
    }

    public function import()
    {
        $data = [
            'title' => 'Import File - Diskominfo',
            'page' => 'Import FIle',
            'validation' => $this->validation
        ];

        return view('/Admin/Files/vImport', $data);
    }

    public function store()
    {
        $file = $this->request->getFile('file-upload');
        $nama_file = $this->request->getVar('nama_file');
        $keterangan = $this->request->getVar('keterangan');
        $data = array(
            'file-upload' => $file,
            'nama_file' => $nama_file,
            'keterangan' => $keterangan
        );

        if ($this->validation->run($data, 'uploadFile_file') == FALSE) {
            return redirect()->to('/files/import')->withInput();
        } else {

            $data['user_uploaded'] = $_SESSION['data']['id_pegawai'];
            $data['file_name'] = $file->getRandomName();
            $simpan = $this->mFileUpload->save($data);
            if (!$simpan) {
                $pesan = 'data gagal di simpan';
                $status_pesan = 'danger';
            } else {
                $file->move('FileUpload', $data['file_name']);
                $pesan = 'Berhasil simpan data';
                $status_pesan = 'success';
            }
            session()->setFlashdata('result', ['status' => $status_pesan, 'message' => $pesan]);
            return redirect()->to(base_url('/files'));
        }
    }

    //--------------------------------------------------------------------

}
