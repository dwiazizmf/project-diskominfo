<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\mGaji;
use App\Models\mPegawai;

class Gaji extends BaseController
{
    protected $validation;
    protected $mGaji;
    protected $mPegawai;
    public function __construct()
    {
        session();
        if (!isset($_SESSION['data'])) {
            header('Location: ' . base_url());
            exit();
        }

        if ($_SESSION['data']['id_jabatan'] != 3 && $_SESSION['data']['id_jabatan'] != 5) {
            header('Location: ' . base_url('/users/print_gaji'));
            exit();
        }

        $this->validation =  \Config\Services::validation();
        $this->mGaji = new mGaji();
        $this->mPegawai = new mPegawai();
    }

    public function index()
    {
        $data = [
            'title' => 'Gaji - Diskominfo',
            'page' => 'List Data Gaji'
        ];

        if ($_SESSION['data']['id_jabatan'] == 5) {
            $data['insert'] = ['/gaji/import', 'Import Data Gaji'];
        }

        if (isset($_GET['periode_search'])) {
            $data_exp = explode("-", $_GET['periode_search']);
            $tahun = $data_exp[0];
            $bulan = $data_exp[1];
        } else {
            $tahun = date('Y');
            $bulan = date('n');
        }

        $data['data'] = $this->mGaji->where('tahun', $tahun)->where('bulan', $bulan)->findAll();

        return view('/Admin/Gaji/vGaji', $data);
    }

    public function edit($id)
    {
        $data = [
            'title' => 'Edit Data Gaji - Diskominfo',
            'page' => 'Edit Data Gaji',
            'validation' => $this->validation,
            'data' => $this->mGaji->find($id)
        ];

        if (empty($data['data'])) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Data dengan id ' . $id . ' tidak di temukan');
        }

        return view('/Admin/Gaji/vEdit', $data);
    }

    public function update($id)
    {
        $data_edit = $this->request->getVar();
        if ($this->validation->run($data_edit, 'editSalary') == FALSE) {
            return redirect()->to('/gaji/edit/' . $id)->withInput();
        } else {
            $data_edit['id'] = $id;
            $simpan = $this->mSalary->save($data_edit);
            if (!$simpan) {
                $pesan = 'data gagal di update';
                $status_pesan = 'danger';
            } else {
                $pesan = 'Berhasil edit data';
                $status_pesan = 'success';
            }
            session()->setFlashdata('result', ['status' => $status_pesan, 'message' => $pesan]);
            return redirect()->to(base_url('/gaji/edit/' . $id));
        }
    }

    public function delete($id)
    {
        $delete = $this->mGaji->delete($id);
        if (!$delete) {
            $pesan = 'data gagal di delete';
            $status_pesan = 'danger';
        } else {
            $pesan = 'Berhasil hapus data';
            $status_pesan = 'success';
        }
        session()->setFlashdata('result', ['status' => $status_pesan, 'message' => $pesan]);
        return redirect()->to(base_url('/gaji'));
    }

    public function import()
    {
        $data = [
            'title' => 'Import Gaji - Diskominfo',
            'page' => 'Import Gaji',
            'validation' => $this->validation
        ];

        return view('/Admin/Gaji/vImport', $data);
    }

    public function store()
    {
        $data_result = [];
        $error_data = 0;
        $file = $this->request->getFile('file-upload');
        $periode = $this->request->getVar('periode');
        $data = array(
            'file-upload' => $file,
            'periode' => $periode
        );

        if ($this->validation->run($data, 'uploadFile') == FALSE) {
            return redirect()->to('/gaji/import')->withInput();
        } else {

            // ambil extension dari file excel
            $extension = $file->getClientExtension();

            if ('xls' == $extension) {
                // format excel 2007 ke bawah
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
            } else {
                // format excel 2010 ke atas
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            }

            $spreadsheet = $reader->load($file);
            $data = $spreadsheet->getActiveSheet()->toArray();

            if (count($data) > 1) {

                foreach ($data as $idx => $row) {
                    //lewati baris ke 0 dan 1 pada file excel
                    if ($idx == 0) {
                        continue;
                    }
                    $exp_periode = explode("-", $periode);
                    $cek_data = $this->mPegawai->where('nip_pegawai', $row[2])->find();
                    if ($cek_data < 1) {
                        continue;
                    }

                    $data_insert = [
                        'periode' => $periode,
                        'tahun' => $exp_periode[0],
                        'bulan' => $exp_periode[1],
                        'nama' => $row[1],
                        'nip' => $row[2],
                        'pangkat' => $row[3],
                        'golongan_pangkat' => $row[4],
                        'nama_jabatan' => $row[5],
                        'es' => $row[6],
                        'status' => $row[7],
                        'jiwa' => $row[8],
                        'anak' => $row[9],
                        'bulan_gaji' => $row[10],
                        'gaji_pokok' => $row[11],
                        'tunjangan_issu' => $row[12],
                        'tunjangan_anak' => $row[13],
                        'total_gaji_bersih' => $row[14],
                        'tunjangan_struktural' => $row[15],
                        'tunjangan_beras' => $row[16],
                        'tunjangan_pajak_penghasilan' => $row[17],
                        'pembulatan' => $row[18],
                        'penghasilan_kotor' => $row[19],
                        'iwp' => $row[20],
                        'askes' => $row[21],
                        'pph21' => $row[22],
                        'bapetarum' => $row[23],
                        'jumlah_potongan' => $row[24],
                        'penghasilan_bersih' => $row[25],
                        'sim_kompak' => $row[26],
                        'pot_kompak' => $row[27],
                        'pot_bjb' => $row[28],
                        'pot_dw' => $row[29],
                        'pot_kopri' => $row[30],
                        'total_pot' => $row[31],
                        'gaji_set_pot' => $row[32],
                        'bbm' => $row[33],
                        'bbm_pph21' => $row[34],
                        'bbm_bersih' => $row[35],
                        'kompensasi' => $row[36],
                        'kompensasi_pph21' => $row[37],
                        'pot_kop_kompen' => $row[38],
                        'jumlah_pot_komponen' => $row[39],
                        'kompensasi_bersih' => $row[40],
                        'tpp' => $row[41],
                        'persen' => $row[42],
                        'bruto' => $row[43],
                        'pph21_tpp' => $row[44],
                        'tpp_net' => $row[45],
                        'zakat' => $row[46],
                        'kpps' => $row[47],
                        'a_kpps' => $row[48],
                        'a_bjb' => $row[49],
                        'a_kompak' => $row[50],
                        'a_syariah' => $row[51],
                        'a_total_pot' => $row[52],
                        'tpp_bersih' => $row[53],
                        'total_penerimaan_gaji' => $row[54]
                    ];
                    //d($row[2]."-".$exp_periode[0]."-".$exp_periode[1]);
                    $cek_data_gaji = $this->mGaji->where(['nip' => $row[2], 'tahun' => $exp_periode[0], 'bulan' => $exp_periode[1]])->find();
                    //dd($cek_data_gaji);
                    if (count($cek_data_gaji) > 0) {
                        $data_insert['id'] = $cek_data_gaji[0]['id'];
                    }
                    //d($data_insert);
                    try {
                        $simpan = $this->mGaji->save($data_insert);
                    } catch (\Throwable $th) {
                        $simpan = false;
                    }
                    //dd($simpan);

                    if (!$simpan) {
                        $data_result[] = $row;
                    }
                }

                //dd($data_result);

                if (count($data_result) > 0) {
                    $error_data = 1;
                    $pesan = 'ada data yang gagal di insert ke database';
                    $status_pesan = 'warning';
                } else {
                    $pesan = 'Berhasil upload data';
                    $status_pesan = 'success';
                }
            } else {
                $pesan = 'Tidak ada data untuk di upload';
                $status_pesan = 'danger';
            }
            session()->setFlashdata('result', ['status' => $status_pesan, 'message' => $pesan, 'data' => $data_result]);
            if ($error_data == 1) {
                return redirect()->to(base_url('/gaji/import'));
            } else {
                return redirect()->to(base_url('/gaji'));
            }
        }
    }

    //--------------------------------------------------------------------

}
