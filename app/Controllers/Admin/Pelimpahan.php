<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\mPegawai;
use App\Models\mBidang;
use App\Models\mAnggaran;
use App\Models\mAnggaranKas;
use App\Models\mRealisasisp2d;
use App\Models\mRealisasispj;
use App\Models\mPelimpahan;
use App\Models\mDetailPelimpahan;

class Pelimpahan extends BaseController
{
    protected $validation;
    protected $mBidang;
    protected $mAnggaran;
    protected $mAnggaranKas;
    protected $mRealisasisp2d;
    protected $mRealisasispj;
    protected $mPegawai;
    protected $mPelimpahan;
    protected $mDetailPelimpahan;

    public function __construct()
    {
        session();
        if (!isset($_SESSION['data'])) {
            header('Location: ' . base_url());
            exit();
        }

        $this->validation =  \Config\Services::validation();
        $this->mBidang = new mBidang();
        $this->mAnggaran = new mAnggaran();
        $this->mAnggaranKas = new mAnggaranKas();
        $this->mRealisasisp2d = new mRealisasisp2d();
        $this->mRealisasispj = new mRealisasispj();
        $this->mPegawai = new mPegawai();
        $this->mPelimpahan = new mPelimpahan();
        $this->mDetailPelimpahan = new mDetailPelimpahan();
    }

    public function index()
    {
        $data = [
            'title' => 'Pelimpahan - Diskominfo',
            'page' => 'List Pelimpahan',
        ];


        if (isset($_GET['periode_search'])) {
            $data_exp = explode("-", $_GET['periode_search']);
            $tahun = $data_exp[0];
            $bulan = $data_exp[1];
        } else {
            $tahun = date('Y');
            $bulan = date('n');
        }

        $data_ = $this->mAnggaran->where('tahun_anggaran', $tahun)->findAll();
        foreach ($data_ as $key => $value_) {
            $unique_id = $value_['unique_id'];
            //$data['insert'] = array('/realisasi/edit_pelimpahan/' . $unique_id, 'Insert Pelimpahan');

            $data_detail_pel = $this->mDetailPelimpahan->where('id_anggaran', $value_['id'])->find();
            //d($data_detail_pel);
            foreach ($data_detail_pel as $key_ => $value) {
                //d($value);
                $data1['id_pegawai'] = $value['id_pegawai'];
                $data1['id_bidang'] = $value_['id_bidang'];
                $data_pe = $this->mDetailPelimpahan->where('id', $value['id'])->find();
                foreach ($data_pe as $key_de => $value_de) {
                    $data1['sd_bulan_lalu'] = 0;
                    for ($i = 1; $i < $bulan; $i++) {
                        $data1['sd_bulan_lalu'] = $data1['sd_bulan_lalu'] +  $value_de['bulan_' . $i];
                    }

                    $data1['bulan_ini'] = $value_de['bulan_' . $bulan];
                    $data1['sd_bulan_ini'] = $data1['bulan_ini'] + $data1['sd_bulan_lalu'];
                }

                $data_spj = $this->mRealisasispj->where('id_anggaran', $value['id_anggaran'])->find();
                foreach ($data_spj as $key_pe => $value_pe) {
                    $data1['spj_sd_bulan_lalu'] = 0;
                    for ($i = 1; $i < $bulan; $i++) {
                        $data1['spj_sd_bulan_lalu'] = $data1['spj_sd_bulan_lalu'] +  $value_pe['bulan_' . $i];
                    }

                    $data1['spj_bulan_ini'] = $value_pe['bulan_' . $bulan];
                    $data1['spj_sd_bulan_ini'] = $data1['spj_bulan_ini'] + $data1['spj_sd_bulan_lalu'];
                }

                $data1['bpp_sd_bulan_lalu'] = $data1['sd_bulan_lalu'] - $data1['spj_sd_bulan_lalu'];
                $data1['bpp_bulan_ini'] = $data1['bulan_ini'] - $data1['spj_bulan_ini'];
                $data1['bpp_sd_bulan_ini'] = $data1['sd_bulan_ini'] - $data1['spj_sd_bulan_ini'];
                $data['data'][] = $data1;
            }
        }

        //dd($data['data']);

        $bidang = $this->mBidang->findAll();
        foreach ($bidang as $value) {
            $data_bidang[$value['id']] = $value['nama_bidang'];
        }

        $data['bidang'] = $data_bidang;


        $pegawai = $this->mPegawai->findAll();
        foreach ($pegawai as $value) {
            $data_pegawai[$value['id']] = $value['nama_pegawai'];
        }

        $data['pegawai'] = $data_pegawai;

        return view('/Admin/Anggaran/vPelimpahan', $data);
    }


    public function insert()
    {
        $data = [
            'title' => 'Insert Anggaran - Diskominfo',
            'page' => 'Insert New Anggaran',
            'validation' => $this->validation,
            'bidang' => $this->mBidang->findAll(),
            'pegawai' => $this->mPegawai->where('id_jabatan != 4 AND id != ' . $_SESSION['data']['id_pegawai'])->findAll()
        ];

        return view('/Admin/Anggaran/vInsert', $data);
    }

    public function store()
    {
        $data_insert = $this->request->getVar();
        //if ($this->validation->run($data_edit, 'editBbm') == FALSE) {
        //    return redirect()->to('/bbm/edit/' . $id)->withInput();
        //} else {
        $simpan = $this->mAnggaran->insert($data_insert);
        if (!$simpan) {
            $pesan = 'data yang gagal di insert';
            $status_pesan = 'danger';
        } else {
            $data_ = array(
                'id_anggaran' => $simpan,
                'bulan_1' => 0,
                'bulan_2' => 0,
                'bulan_3' => 0,
                'bulan_4' => 0,
                'bulan_5' => 0,
                'bulan_6' => 0,
                'bulan_7' => 0,
                'bulan_8' => 0,
                'bulan_9' => 0,
                'bulan_10' => 0,
                'bulan_11' => 0,
                'bulan_12' => 0,
                'total' => 0
            );
            $this->mAnggaranKas->save($data_);
            $this->mRealisasisp2d->save($data_);
            $this->mRealisasispj->save($data_);
            $this->mPelimpahan->save($data_);
            $pesan = 'Berhasil insert data';
            $status_pesan = 'success';
        }
        session()->setFlashdata('result', ['status' => $status_pesan, 'message' => $pesan]);
        return redirect()->to(base_url('/anggaran'));
        //}
    }


    //--------------------------------------------------------------------

}
