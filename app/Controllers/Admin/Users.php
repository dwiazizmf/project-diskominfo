<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\mPegawai;
use App\Models\mBidang;
use App\Models\mJabatan;


class Users extends BaseController
{
    protected $mPegawai;
    protected $mBidang;
    protected $mJabatan;
    public function __construct()
    {
        session();
        if (!isset($_SESSION['data'])) {
            header('Location: ' . base_url());
            exit();
        }

        if ($_SESSION['data']['id_jabatan'] != 3) {
            header('Location: ' . base_url('/users/print_gaji'));
            exit();
        }

        $this->validation =  \Config\Services::validation();
        $this->mPegawai = new mPegawai();
        $this->mBidang = new mBidang();
        $this->mJabatan = new mJabatan();
    }
    public function index()
    {
        $data = [
            'title' => 'Users - Diskominfo',
            'page' => 'User List',
            'insert' => ['/users/insert', 'Insert User Baru'],
            'data' => $this->mPegawai->findAll()
        ];

        $bidang = $this->mBidang->findAll();
        $data_bidang[0] = 'Pegawai';
        foreach ($bidang as $value) {
            $data_bidang[$value['id']] = $value['nama_bidang'];
        }
        $data['bidang'] = $data_bidang;

        $jabatan = $this->mJabatan->findAll();
        foreach ($jabatan as $value) {
            $data_jabatan[$value['id']] = $value['nama_jabatan'];
        }
        $data['jabatan'] = $data_jabatan;

        return view('/Admin/Users/vUsers', $data);
    }

    public function insert()
    {
        $data = [
            'title' => 'Users - Diskominfo',
            'page' => 'Insert User',
            'validation' => $this->validation,
        ];

        $bidang = $this->mBidang->findAll();

        $data['bidang'] = $bidang;

        $jabatan = $this->mJabatan->findAll();

        $data['jabatan'] = $jabatan;

        return view('/Admin/Users/vInsert', $data);
    }

    public function store()
    {
        helper('text');

        $unique = random_string('md5', 8);

        $data_insert = $this->request->getVar();
        $data_insert['unique_id'] = $unique;
        if ($this->validation->run($data_insert, 'insertUser') == FALSE) {
            return redirect()->to('/users/insert')->withInput();
        } else {
            $simpan = $this->mPegawai->save($data_insert);
            if (!$simpan) {
                $pesan = 'Gagal insert data';
                $status_pesan = 'danger';
            } else {
                $pesan = 'Berhasil insert data';
                $status_pesan = 'success';
            }
            session()->setFlashdata('result', ['status' => $status_pesan, 'message' => $pesan]);
            return redirect()->to(base_url('/users'));
        }
    }

    public function edit($id)
    {
        $data = [
            'title' => 'Users - Diskominfo',
            'page' => 'Edit User',
            'validation' => $this->validation,
            'data' => $this->mPegawai->where('unique_id', $id)->find()
        ];

        if (empty($data['data'])) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Data dengan id ' . $id . ' tidak di temukan');
        }

        $bidang = $this->mBidang->findAll();

        $data['bidang'] = $bidang;

        $jabatan = $this->mJabatan->findAll();

        $data['jabatan'] = $jabatan;

        return view('/Admin/Users/vEdit', $data);
    }

    public function update($id)
    {
        $data_update = $this->request->getVar();
        if ($this->validation->run($data_update, 'insertUser') == FALSE) {
            return redirect()->to('/users/edit/' . $data_update['unique_id'])->withInput();
        } else {
            $simpan = $this->mPegawai->save($data_update);
            if (!$simpan) {
                $pesan = 'Gagal update data';
                $status_pesan = 'danger';
            } else {
                $pesan = 'Berhasil update data';
                $status_pesan = 'success';
            }
            session()->setFlashdata('result', ['status' => $status_pesan, 'message' => $pesan]);
            return redirect()->to(base_url('/users'));
        }
    }

    public function delete($id)
    {
        $delete = $this->mPegawai->delete($id);
        if (!$delete) {
            $pesan = 'data gagal di delete';
            $status_pesan = 'danger';
        } else {
            $pesan = 'Berhasil hapus data';
            $status_pesan = 'success';
        }
        session()->setFlashdata('result', ['status' => $status_pesan, 'message' => $pesan]);
        return redirect()->to(base_url('/users'));
    }

    public function change_status($uid)
    {
        $data_ = $this->mPegawai->where('unique_id', $uid)->findAll();
        if ($data_[0]['status']) {
            $status = 0;
        } else {
            $status = 1;
        }
        $data = array('id' => $data_[0]['id'], 'status' => $status);
        $delete = $this->mPegawai->save($data);
        if (!$delete) {
            $pesan = 'Gagal ubah status';
            $status_pesan = 'danger';
        } else {
            $pesan = 'Berhasil ubah status';
            $status_pesan = 'success';
        }
        session()->setFlashdata('result', ['status' => $status_pesan, 'message' => $pesan]);
        return redirect()->to(base_url('/users'));
    }
    //--------------------------------------------------------------------
    public function roles()
    {
        $data = [
            'title' => 'Role - Diskominfo',
            'page' => 'User Roles'
        ];

        return view('/Admin/vRoles', $data);
    }

    public function tree()
    {
        $data = [
            'title' => 'Role - Diskominfo',
            'page' => 'User Roles'
        ];

        return view('/Admin/treeView', $data);
    }
}
