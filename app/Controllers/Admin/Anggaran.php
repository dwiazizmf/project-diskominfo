<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\mPegawai;
use App\Models\mBidang;
use App\Models\mAnggaran;
use App\Models\mAnggaranKas;
use App\Models\mRealisasisp2d;
use App\Models\mRealisasispj;
use App\Models\mPelimpahan;
use App\Models\mDetailPelimpahan;

class Anggaran extends BaseController
{
    protected $validation;
    protected $mBidang;
    protected $mAnggaran;
    protected $mAnggaranKas;
    protected $mRealisasisp2d;
    protected $mRealisasispj;
    protected $mPegawai;
    protected $mPelimpahan;
    protected $mDetailPelimpahan;

    public function __construct()
    {
        session();
        if (!isset($_SESSION['data'])) {
            header('Location: ' . base_url());
            exit();
        }

        if ($_SESSION['data']['id_jabatan'] != 3 && $_SESSION['data']['id_jabatan'] != 2 && $_SESSION['data']['id_jabatan'] != 6) {
            header('Location: ' . base_url('/users/print_gaji'));
            exit();
        }

        $this->validation =  \Config\Services::validation();
        $this->mBidang = new mBidang();
        $this->mAnggaran = new mAnggaran();
        $this->mAnggaranKas = new mAnggaranKas();
        $this->mRealisasisp2d = new mRealisasisp2d();
        $this->mRealisasispj = new mRealisasispj();
        $this->mPegawai = new mPegawai();
        $this->mPelimpahan = new mPelimpahan();
        $this->mDetailPelimpahan = new mDetailPelimpahan();
    }

    public function index()
    {
        $data = [
            'title' => 'Anggaran - Diskominfo',
            'page' => 'List Angaran'
        ];

        if (isset($_GET['periode_search'])) {
            $tahun = $_GET['periode_search'];
        } else {
            $tahun = date("Y");
        }

        $data['data'] = $this->mAnggaran->where('tahun_anggaran', $tahun)->orderBy('id', 'DESC')->findAll();

        if ($_SESSION['data']['id_jabatan'] != 1) {
            $data['insert'] = array('/anggaran/insert', 'Insert Anggaran Baru');
        }

        $bidang = $this->mBidang->findAll();
        foreach ($bidang as $value) {
            $data_bidang[$value['id']] = $value['nama_bidang'];
        }

        $pegawai = $this->mPegawai->findAll();
        foreach ($pegawai as $value) {
            $data_pegawai[$value['id']] = $value['nama_pegawai'];
        }

        $data['bidang'] = $data_bidang;
        $data['pegawai'] = $data_pegawai;

        return view('/Admin/Anggaran/vAnggaran', $data);
    }


    public function insert()
    {
        $data = [
            'title' => 'Insert Anggaran - Diskominfo',
            'page' => 'Insert New Anggaran',
            'validation' => $this->validation,
            'bidang' => $this->mBidang->findAll(),
            'pegawai' => $this->mPegawai->where('id_jabatan = 1 and status = 1')->findAll()
        ];

        return view('/Admin/Anggaran/vInsert', $data);
    }

    public function store()
    {
        helper('text');

        $unique = random_string('md5', 8);

        $data_insert = $this->request->getVar();
        if ($this->validation->run($data_insert, 'insertAnggaran1') == FALSE) {
            return redirect()->to('/anggaran/insert')->withInput();
        } else {
            $data_insert['user_inserted'] = $_SESSION['data']['id_pegawai'];
            $data_insert['unique_id'] = $unique;
            $simpan = $this->mAnggaran->insert($data_insert);
            if (!$simpan) {
                $pesan = 'data yang gagal di insert';
                $status_pesan = 'danger';
            } else {
                $data_ = array(
                    'id_anggaran' => $simpan,
                    'bulan_1' => 0,
                    'bulan_2' => 0,
                    'bulan_3' => 0,
                    'bulan_4' => 0,
                    'bulan_5' => 0,
                    'bulan_6' => 0,
                    'bulan_7' => 0,
                    'bulan_8' => 0,
                    'bulan_9' => 0,
                    'bulan_10' => 0,
                    'bulan_11' => 0,
                    'bulan_12' => 0,
                    'total' => 0
                );
                $this->mAnggaranKas->save($data_);
                $this->mRealisasisp2d->save($data_);
                $this->mRealisasispj->save($data_);
                $id_pellimpahan = $this->mPelimpahan->insert($data_);
                $data_['id_pelimpahan'] = $id_pellimpahan;
                $data_['id_pegawai'] = $data_insert['id_pegawai'];
                $this->mDetailPelimpahan->save($data_);
                // if ($data_insert['id_pegawai_2'] != '') {
                //     $data_['id_pegawai_2'] = $data_insert['id_pegawai_2'];
                //     $this->mDetailPelimpahan->save($data_);
                // }
                $pesan = 'Berhasil insert data';
                $status_pesan = 'success';
            }
            session()->setFlashdata('result', ['status' => $status_pesan, 'message' => $pesan]);
            return redirect()->to(base_url('/anggaran'));
        }
    }

    public function delete($id)
    {
        $data = $this->mAnggaran->where('id', $id)->findAll();
        //dd($data);
        if ($data[0]['anggaran_kas'] == 0 && $data[0]['sp2d'] == 0 && $data[0]['spj'] == 0 && $data[0]['pelimpahan'] == 0) {
            $delete = $this->mAnggaran->delete($id);
            $delete = $this->mAnggaranKas->where('id_anggaran', $id)->delete();
            $delete = $this->mRealisasisp2d->where('id_anggaran', $id)->delete();
            $delete = $this->mRealisasispj->where('id_anggaran', $id)->delete();
            $delete = $this->mPelimpahan->where('id_anggaran', $id)->delete();
            if (!$delete) {
                $pesan = 'data gagal di delete';
                $status_pesan = 'danger';
            } else {
                $pesan = 'Berhasil hapus data';
                $status_pesan = 'success';
            }
            session()->setFlashdata('result', ['status' => $status_pesan, 'message' => $pesan]);
            return redirect()->to(base_url('/anggaran'));
        } else {
            $pesan = 'Data ini tidak bisa di hapus karena sudah ada isi nya';
            $status_pesan = 'warning';
            session()->setFlashdata('result', ['status' => $status_pesan, 'message' => $pesan]);
            return redirect()->to(base_url('/anggaran'));
        }
    }

    public function edit_jumlah_anggaran()
    {
        //var_dump($this->request->getVar());
        $data = $this->request->getVar();
        return json_encode($this->mAnggaran->save($data));
    }

    public function confirm($id)
    {
        $data_edit = $this->request->getVar();
        // if ($this->validation->run($data_edit, 'editBbm') == FALSE) {
        //     return redirect()->to('/bbm/edit/' . $id)->withInput();
        // } else {
        $data_edit['id'] = $id;
        //dd($data_edit);
        $simpan = $this->mAnggaran->save($data_edit);
        if (!$simpan) {
            $pesan = 'data yang gagal di update';
            $status_pesan = 'danger';
        } else {
            $pesan = 'Berhasil edit data';
            $status_pesan = 'success';
        }
        session()->setFlashdata('result', ['status' => $status_pesan, 'message' => $pesan]);
        return redirect()->to(base_url('/anggaran'));
        //}
    }


    //--------------------------------------------------------------------

}
