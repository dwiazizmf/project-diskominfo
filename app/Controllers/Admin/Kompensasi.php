<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\mKompensasi;
use App\Models\mGajiKompensasi;
use App\Models\mAnggaranGaji;

class Kompensasi extends BaseController
{
    protected $validation;
    protected $mKompensasi;
    protected $mGajiKompensasi;
    protected $mAnggaranGaji;
    public function __construct()
    {
        session();
        if (!isset($_SESSION['data'])) {
            header('Location: ' . base_url());
            exit();
        }
        $this->validation =  \Config\Services::validation();
        $this->mKompensasi = new mKompensasi();
        $this->mGajiKompensasi = new mGajiKompensasi();
        $this->mAnggaranGaji = new mAnggaranGaji();
    }

    public function index_()
    {
        $data = [
            'title' => 'Kompensasi - Diskominfo',
            'page' => 'List Data Gaji',
            'data' => $this->mGajiKompensasi->findAll()
        ];
        if ($_SESSION['data']['id_jabatan'] == 5) {
            $data['insert'] = ['/kompensasi/import', 'Import Data Kompensasi'];
        }
        //dd($data['data']);

        return view('/Admin/Kompensasi/vKompensasi_', $data);
    }

    public function index()
    {
        $data = [
            'title' => 'Kompensasi - Diskominfo',
            'page' => 'List Data Kompensasi',
            'insert' => ['/kompensasi/import', 'Import Data Kompensasi'],
            'data' => $this->mKompensasi->findAll()
        ];

        return view('/Admin/Kompensasi/vKompensasi', $data);
    }

    public function edit()
    {
        $id = 1;
        $data = [
            'title' => 'Edit Data Kompensasi - Diskominfo',
            'page' => 'Edit Data Kompensasi',
            'validation' => $this->validation,
            'data' => $this->mKompensasi->find($id)
        ];

        if (empty($data['data'])) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Data dengan id ' . $id . ' tidak di temukan');
        }

        return view('/Admin/Kompensasi/vEdit', $data);
    }

    public function update($id)
    {
        $data_edit = $this->request->getVar();
        if ($this->validation->run($data_edit, 'editKompensasi') == FALSE) {
            return redirect()->to('/kompensasi/edit/' . $id)->withInput();
        } else {
            $data_edit['id'] = $id;
            $simpan = $this->mKompensasi->save($data_edit);
            if (!$simpan) {
                $pesan = 'data gagal di update';
                $status_pesan = 'danger';
            } else {
                $pesan = 'Berhasil edit data';
                $status_pesan = 'success';
            }
            session()->setFlashdata('result', ['status' => $status_pesan, 'message' => $pesan]);
            return redirect()->to(base_url('/kompensasi/edit/' . $id));
        }
    }

    public function delete($id)
    {
        $delete = $this->mKompensasi->delete($id);
        if (!$delete) {
            $pesan = 'data gagal di delete';
            $status_pesan = 'danger';
        } else {
            $pesan = 'Berhasil hapus data';
            $status_pesan = 'success';
        }
        session()->setFlashdata('result', ['status' => $status_pesan, 'message' => $pesan]);
        return redirect()->to(base_url('/kompensasi'));
    }

    public function import()
    {
        $data = [
            'title' => 'Import Kompensasi - Diskominfo',
            'page' => 'Import Kompensasi',
            'validation' => $this->validation
        ];

        return view('/Admin/Kompensasi/vImport', $data);
    }

    public function store()
    {
        $data_result = [];
        $file = $this->request->getFile('file-upload');
        $periode = $this->request->getVar('periode');
        $data = array(
            'file-upload' => $file,
            'periode' => $periode
        );

        if ($this->validation->run($data, 'uploadFile') == FALSE) {
            return redirect()->to('/kompensasi/import')->withInput();
        } else {

            // ambil extension dari file excel
            $extension = $file->getClientExtension();

            if ('xls' == $extension) {
                // format excel 2007 ke bawah
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
            } else {
                // format excel 2010 ke atas
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            }

            $spreadsheet = $reader->load($file);
            $data = $spreadsheet->getActiveSheet()->toArray();

            if (count($data) > 1) {

                foreach ($data as $idx => $row) {
                    //lewati baris ke 0 pada file excel
                    if ($idx == 0) {
                        continue;
                    }

                    $data_insert = [
                        'kodin' => $row[1],
                        'rek_internal' => $row[2],
                        'rek_eksternal' => $row[3],
                        'nama' => $row[4],
                        'tpp' => $row[5],
                        'pph_21' => $row[6],
                        'simpanan_kas' => $row[7],
                        'potongan_kkps' => $row[8],
                        'pot_dinas' => $row[9]
                    ];

                    try {
                        $simpan = $this->mKompensasi->save($data_insert);
                    } catch (\Throwable $th) {
                        $simpan = false;
                    }


                    if (!$simpan) {
                        $data_result[] = $row;
                    }
                }

                if (count($data_result) > 0) {
                    $pesan = 'ada data yang gagal di insert ke database';
                    $status_pesan = 'warning';
                } else {
                    $pesan = 'Berhasil upload data';
                    $status_pesan = 'success';
                }
            } else {
                $pesan = 'Tidak ada data untuk di upload';
                $status_pesan = 'danger';
            }

            session()->setFlashdata('result', ['status' => $status_pesan, 'message' => $pesan, 'data' => $data_result]);
            return redirect()->to(base_url('/kompensasi/import'));
        }
    }


    public function store_()
    {
        $data_result = [];
        $file = $this->request->getFile('file-upload');
        $periode = $this->request->getVar('periode');
        $data = array(
            'file-upload' => $file,
            'periode' => $periode
        );


        if ($this->validation->run($data, 'uploadFile') == FALSE) {
            return redirect()->to('/kompensasi/import')->withInput();
        } else {
            $tahun = explode("-", $data['periode']);
            $cek_data = $this->mAnggaranGaji->where('tahun_anggaran', $tahun[0])->findAll();
            if (count($cek_data) > 0) {
                $id_anggaran = $cek_data[0]['id'];
                // ambil extension dari file excel
                $extension = $file->getClientExtension();

                if ('xls' == $extension) {
                    // format excel 2007 ke bawah
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                } else {
                    // format excel 2010 ke atas
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                }

                $spreadsheet = $reader->load($file);
                $data = $spreadsheet->getActiveSheet()->toArray();


                if (count($data) > 2) {
                    $count_ = count($data) - 2;
                    $data_all_gaji_num = str_replace(',', '', $data[$count_][14]);
                    //dd($data[$count_]);
                    $getID = $this->mGajiKompensasi->where('id_anggaran_gaji', $id_anggaran)->find();
                    $data_insert = ['id' => $getID[0]['id'], 'bulan_' . $tahun[1] => (int)$data_all_gaji_num];
                    //dd($data_insert);
                    $simpan = $this->mGajiKompensasi->save($data_insert);
                    //dd($simpan);
                    if ($simpan) {
                        $getSum = $this->mGajiKompensasi->find($getID[0]['id']);
                        $jmlh_gaji = $getSum['bulan_1'] + $getSum['bulan_2'] + $getSum['bulan_3']
                            + $getSum['bulan_4'] + $getSum['bulan_5'] + $getSum['bulan_6'] + $getSum['bulan_7'] + $getSum['bulan_8']
                            + $getSum['bulan_9'] + $getSum['bulan_10'] + $getSum['bulan_11'] + $getSum['bulan_12'];
                        $data_anggaran = ['id' => $id_anggaran, 'kompensasi' => $jmlh_gaji];
                        //dd($data_insert);
                        $simpan = $this->mAnggaranGaji->save($data_anggaran);
                        if ($simpan) {
                            $pesan = 'Berhasil upload data';
                            $status_pesan = 'success';
                        } else {
                            $pesan = 'Gagal simpan data anggaran gaji';
                            $status_pesan = 'warning';
                        }
                    } else {
                        $pesan = 'Gagal upload data';
                        $status_pesan = 'warning';
                    }
                } else {
                    $pesan = 'Tidak ada data untuk di upload';
                    $status_pesan = 'danger';
                }
            } else {
                $pesan = 'Tidak ada data pada tahun ' . $tahun[0];
                $status_pesan = 'danger';
            }


            session()->setFlashdata('result', ['status' => $status_pesan, 'message' => $pesan, 'data' => $data_result]);
            return redirect()->to(base_url('/kompensasi'));
        }
    }


    //--------------------------------------------------------------------

}
