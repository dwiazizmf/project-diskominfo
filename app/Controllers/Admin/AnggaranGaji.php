<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\mPegawai;

use App\Models\mAnggaranGaji;
use App\Models\mAnggaranKasGaji;
use App\Models\mGajiGaji;
use App\Models\mGajiBbm;
use App\Models\mGajiTpp;
use App\Models\mGajiKompensasi;
use App\Models\mRealisasiGajisp2d;
use App\Models\mRealisasiGajispj;


class AnggaranGaji extends BaseController
{
    protected $validation;
    protected $mPegawai;
    protected $mAnggaranGaji;
    protected $mAnggaranKasGaji;
    protected $mGajiGaji;
    protected $mGajiBbm;
    protected $mGajiTpp;
    protected $mGajiKompensasi;
    protected $mRealisasiGajisp2d;
    protected $mRealisasiGajispj;

    public function __construct()
    {
        session();
        if (!isset($_SESSION['data'])) {
            header('Location: ' . base_url());
            exit();
        }

        if ($_SESSION['data']['id_jabatan'] != 2) {
            header('Location: ' . base_url('/users/print_gaji'));
            exit();
        }

        $this->validation =  \Config\Services::validation();
        $this->mPegawai = new mPegawai();

        $this->mAnggaranGaji = new mAnggaranGaji();
        $this->mAnggaranKasGaji = new mAnggaranKasGaji();
        $this->mGajiGaji = new mGajiGaji();
        $this->mGajiBbm = new mGajiBbm();
        $this->mGajiTpp = new mGajiTpp();
        $this->mGajiKompensasi = new mGajiKompensasi();
        $this->mRealisasiGajisp2d = new mRealisasiGajisp2d();
        $this->mRealisasiGajispj = new mRealisasiGajispj();
    }

    public function index()
    {
        $data = [
            'title' => 'Anggaran Gaji - Diskominfo',
            'page' => 'List Angaran Gaji'
        ];

        if (isset($_GET['periode_search'])) {
            $tahun = $_GET['periode_search'];
        } else {
            $tahun = date("Y");
        }

        $data['data'] = $this->mAnggaranGaji->where('tahun_anggaran', $tahun)->orderBy('id', 'DESC')->findAll();

        //if ($_SESSION['data']['id_jabatan'] != 1) {
        $data['insert'] = array('/anggaran_gaji/insert', 'Insert Anggaran gaji Baru');
        //}


        $pegawai = $this->mPegawai->findAll();
        foreach ($pegawai as $value) {
            $data_pegawai[$value['id']] = $value['nama_pegawai'];
        }

        $data['pegawai'] = $data_pegawai;

        return view('/Admin/AnggaranGaji/vAnggaran', $data);
    }


    public function insert()
    {
        $data = [
            'title' => 'Insert Anggaran Gaji - Diskominfo',
            'page' => 'Insert New Anggaran Gaji',
            'validation' => $this->validation
        ];

        return view('/Admin/AnggaranGaji/vInsert', $data);
    }

    public function store()
    {
        dd("masa iya masuk sini");
        helper('text');

        $unique = random_string('md5', 8);

        $data_insert = $this->request->getVar();
        if ($this->validation->run($data_insert, 'insertAnggaran') == FALSE) {
            return redirect()->to('/anggaran_gaji/insert')->withInput();
        } else {
            $data_insert['unique_id'] = $unique;
            $data_insert['user_inserted'] = $_SESSION['data']['id_pegawai'];
            $simpan = $this->mAnggaranGaji->insert($data_insert);
            if (!$simpan) {
                $pesan = 'data yang gagal di insert';
                $status_pesan = 'danger';
            } else {
                $data_ = array(
                    'id_anggaran_gaji' => $simpan,
                    'bulan_1' => 0,
                    'bulan_2' => 0,
                    'bulan_3' => 0,
                    'bulan_4' => 0,
                    'bulan_5' => 0,
                    'bulan_6' => 0,
                    'bulan_7' => 0,
                    'bulan_8' => 0,
                    'bulan_9' => 0,
                    'bulan_10' => 0,
                    'bulan_11' => 0,
                    'bulan_12' => 0,
                    'total' => 0
                );
                $this->mAnggaranKasGaji->save($data_);
                $this->mGajiGaji->save($data_);
                $this->mGajiBbm->save($data_);
                $this->mGajiTpp->save($data_);
                $this->mGajiKompensasi->save($data_);
                $pesan = 'Berhasil insert data';
                $status_pesan = 'success';
            }
            session()->setFlashdata('result', ['status' => $status_pesan, 'message' => $pesan]);
            return redirect()->to(base_url('/anggaran_gaji'));
        }
    }

    public function delete($id)
    {
        $data = $this->mAnggaranGaji->where('id', $id)->findAll();
        //dd($data);
        if ($data[0]['anggaran_kas_gaji'] == 0 && $data[0]['gaji'] == 0 && $data[0]['bbm'] == 0 && $data[0]['tpp'] == 0 && $data[0]['kompensasi'] == 0) {
            $delete = $this->mAnggaranGaji->delete($id);
            $delete = $this->mAnggaranKasGaji->where('id_anggaran_gaji', $id)->delete();
            $delete = $this->mGajiGaji->where('id_anggaran_gaji', $id)->delete();
            $delete = $this->mGajiBbm->where('id_anggaran_gaji', $id)->delete();
            $delete = $this->mGajiTpp->where('id_anggaran_gaji', $id)->delete();
            $delete = $this->mGajiKompensasi->where('id_anggaran_gaji', $id)->delete();
            if (!$delete) {
                $pesan = 'data gagal di delete';
                $status_pesan = 'danger';
            } else {
                $pesan = 'Berhasil hapus data';
                $status_pesan = 'success';
            }
            session()->setFlashdata('result', ['status' => $status_pesan, 'message' => $pesan]);
            return redirect()->to(base_url('/anggaran_gaji'));
        } else {
            $pesan = 'Data ini tidak bisa di hapus karena sudah ada isi nya';
            $status_pesan = 'warning';
            session()->setFlashdata('result', ['status' => $status_pesan, 'message' => $pesan]);
            return redirect()->to(base_url('/anggaran_gaji'));
        }
    }

    public function isiAnggaranGaji($id)
    {
        if ($_SESSION['data']['id_jabatan'] != 2) {
            return redirect()->to(base_url());
        }
        $data_a = $this->mAnggaranGaji->where('unique_id', $id)->find();
        $id_anggaran = $data_a[0]['id'];
        $data = [
            'title' => 'Realisasi Gaji - Diskominfo',
            'page' => 'List Realisasi Gaji',
            'anggaran' => $data_a,
            'validation' => $this->validation,
            'anggaran_kas_gaji' => $this->mAnggaranKasGaji->where('id_anggaran_gaji', $id_anggaran)->findAll(),
            'realisasi_sp2d' => $this->mRealisasiGajisp2d->where('id_anggaran_gaji', $id_anggaran)->findAll(),
            'realisasi_spj' => $this->mRealisasiGajispj->where('id_anggaran_gaji', $id_anggaran)->findAll()
        ];

        if (empty($data['anggaran'])) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Data dengan id ' . $id . ' tidak di temukan');
        }

        $data['pegawai'] = $this->mPegawai->find($data['anggaran'][0]['user_inserted']);

        return view('/Admin/AnggaranGaji/vRealisasi', $data);
    }


    public function store_isiAnggaran()
    {
        if ($_SESSION['data']['id_jabatan'] != 2) {
            return redirect()->to(base_url());
        }
        $data = $this->request->getVar();
        if ($this->validation->run($data, 'isiRealisasiGaji') == FALSE) {
            return redirect()->to('/anggaran_gaji/edit/' . $data['id_unique'])->withInput();
        } else {
            $data_anggaranKas =  array(
                'bulan_1' => $data['anggaran_kas_bulan_1'],
                'bulan_2' => $data['anggaran_kas_bulan_2'],
                'bulan_3' => $data['anggaran_kas_bulan_3'],
                'bulan_4' => $data['anggaran_kas_bulan_4'],
                'bulan_5' => $data['anggaran_kas_bulan_5'],
                'bulan_6' => $data['anggaran_kas_bulan_6'],
                'bulan_7' => $data['anggaran_kas_bulan_7'],
                'bulan_8' => $data['anggaran_kas_bulan_8'],
                'bulan_9' => $data['anggaran_kas_bulan_9'],
                'bulan_10' => $data['anggaran_kas_bulan_10'],
                'bulan_11' => $data['anggaran_kas_bulan_11'],
                'bulan_12' => $data['anggaran_kas_bulan_12']
            );

            $jml_anggaranKas = $data['anggaran_kas_bulan_1'] + $data['anggaran_kas_bulan_2'] + $data['anggaran_kas_bulan_3']
                + $data['anggaran_kas_bulan_4'] + $data['anggaran_kas_bulan_5'] + $data['anggaran_kas_bulan_6']
                + $data['anggaran_kas_bulan_7'] + $data['anggaran_kas_bulan_8'] + $data['anggaran_kas_bulan_9']
                + $data['anggaran_kas_bulan_10'] + $data['anggaran_kas_bulan_11'] + $data['anggaran_kas_bulan_12'];
            $data_realisasisp2d =  array(
                'bulan_1' => $data['realisasi_sp2d_bulan_1'],
                'bulan_2' => $data['realisasi_sp2d_bulan_2'],
                'bulan_3' => $data['realisasi_sp2d_bulan_3'],
                'bulan_4' => $data['realisasi_sp2d_bulan_4'],
                'bulan_5' => $data['realisasi_sp2d_bulan_5'],
                'bulan_6' => $data['realisasi_sp2d_bulan_6'],
                'bulan_7' => $data['realisasi_sp2d_bulan_7'],
                'bulan_8' => $data['realisasi_sp2d_bulan_8'],
                'bulan_9' => $data['realisasi_sp2d_bulan_9'],
                'bulan_10' => $data['realisasi_sp2d_bulan_10'],
                'bulan_11' => $data['realisasi_sp2d_bulan_11'],
                'bulan_12' => $data['realisasi_sp2d_bulan_12']
            );

            $jumlah_sp2d = $data['realisasi_sp2d_bulan_1'] + $data['realisasi_sp2d_bulan_2'] + $data['realisasi_sp2d_bulan_3']
                + $data['realisasi_sp2d_bulan_4'] + $data['realisasi_sp2d_bulan_5'] + $data['realisasi_sp2d_bulan_6']
                + $data['realisasi_sp2d_bulan_7'] + $data['realisasi_sp2d_bulan_8'] + $data['realisasi_sp2d_bulan_9']
                + $data['realisasi_sp2d_bulan_10'] + $data['realisasi_sp2d_bulan_11'] + $data['realisasi_sp2d_bulan_12'];

            $data_realisasispj =  array(
                'bulan_1' => $data['realisasi_spj_bulan_1'],
                'bulan_2' => $data['realisasi_spj_bulan_2'],
                'bulan_3' => $data['realisasi_spj_bulan_3'],
                'bulan_4' => $data['realisasi_spj_bulan_4'],
                'bulan_5' => $data['realisasi_spj_bulan_5'],
                'bulan_6' => $data['realisasi_spj_bulan_6'],
                'bulan_7' => $data['realisasi_spj_bulan_7'],
                'bulan_8' => $data['realisasi_spj_bulan_8'],
                'bulan_9' => $data['realisasi_spj_bulan_9'],
                'bulan_10' => $data['realisasi_spj_bulan_10'],
                'bulan_11' => $data['realisasi_spj_bulan_11'],
                'bulan_12' => $data['realisasi_spj_bulan_12']
            );

            $jumlah_spj = $data['realisasi_spj_bulan_1'] + $data['realisasi_spj_bulan_2'] + $data['realisasi_spj_bulan_3']
                + $data['realisasi_spj_bulan_4'] + $data['realisasi_spj_bulan_5'] + $data['realisasi_spj_bulan_6']
                + $data['realisasi_spj_bulan_7'] + $data['realisasi_spj_bulan_8'] + $data['realisasi_spj_bulan_9']
                + $data['realisasi_spj_bulan_10'] + $data['realisasi_spj_bulan_11'] + $data['realisasi_spj_bulan_12'];




            $hasil = true;
            $data_anggaran['id'] = $data['id_anggaran'];
            $data_anggaran['anggaran_kas_gaji'] = $jml_anggaranKas;
            $data_anggaran['sp2d_gaji'] = $jumlah_sp2d;
            $data_anggaran['spj_gaji'] = $jumlah_spj;

            $simpan = $this->mAnggaranGaji->save($data_anggaran);
            //dd($data_anggaran); 
            $hasil = $hasil & $simpan;

            $data_anggaranKas['id'] = $data['id_anggaran_kas'];
            $data_realisasisp2d['id'] = $data['id_realisasisp2d'];
            $data_realisasispj['id'] = $data['id_realisasispj'];

            $simpan = $this->mAnggaranKasGaji->save($data_anggaranKas);
            $hasil = $hasil & $simpan;
            $simpan = $this->mRealisasiGajisp2d->save($data_realisasisp2d);
            $hasil = $hasil & $simpan;
            $simpan = $this->mRealisasiGajispj->save($data_realisasispj);
            $hasil = $hasil & $simpan;

            if (!$hasil) {
                $pesan = 'data yang gagal di insert';
                $status_pesan = 'danger';
            } else {
                $pesan = 'Berhasil insert data';
                $status_pesan = 'success';
            }
            session()->setFlashdata('result', ['status' => $status_pesan, 'message' => $pesan]);
            return redirect()->to(base_url('/anggaran_gaji'));
        }
    }
    /*public function confirm($id)
    {
        $data_edit = $this->request->getVar();
        // if ($this->validation->run($data_edit, 'editBbm') == FALSE) {
        //     return redirect()->to('/bbm/edit/' . $id)->withInput();
        // } else {
        $data_edit['id'] = $id;
        //dd($data_edit);
        $simpan = $this->mAnggaranGaji->save($data_edit);
        if (!$simpan) {
            $pesan = 'data yang gagal di update';
            $status_pesan = 'danger';
        } else {
            $pesan = 'Berhasil edit data';
            $status_pesan = 'success';
        }
        session()->setFlashdata('result', ['status' => $status_pesan, 'message' => $pesan]);
        return redirect()->to(base_url('/anggaran_gaji'));
        //}
    }*/


    //--------------------------------------------------------------------

}
