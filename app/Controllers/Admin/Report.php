<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\mBbm;
use App\Models\mPegawai;
use App\Models\mBidang;
use App\Models\mAnggaran;
use App\Models\mAnggaranKas;
use App\Models\mRealisasisp2d;
use App\Models\mRealisasispj;
use App\Models\mPelimpahan;

use App\Models\mAnggaranGaji;
use App\Models\mAnggaranKasGaji;
use App\Models\mGajiGaji;
use App\Models\mGajiBbm;
use App\Models\mGajiTpp;
use App\Models\mGajiKompensasi;

use App\Models\mRealisasiGajisp2d;
use App\Models\mRealisasiGajispj;


class Report extends BaseController
{
    protected $validation;
    protected $mBbm;
    protected $mBidang;
    protected $mAnggaran;
    protected $mAnggaranKas;
    protected $mRealisasisp2d;
    protected $mRealisasispj;
    protected $mPegawai;
    protected $mPelimpahan;

    protected $mAnggaranGaji;
    protected $mAnggaranKasGaji;
    protected $mGajiGaji;
    protected $mGajiBbm;
    protected $mGajiTpp;
    protected $mGajiKompensasi;

    protected $mRealisasiGajisp2d;
    protected $mRealisasiGajispj;

    public function __construct()
    {
        session();
        if (!isset($_SESSION['data'])) {
            header('Location: ' . base_url());
            exit();
        }

        if ($_SESSION['data']['id_jabatan'] != 3 && $_SESSION['data']['id_jabatan'] != 2 && $_SESSION['data']['id_jabatan'] != 6 && $_SESSION['data']['id_jabatan'] != 1 && $_SESSION['data']['id_jabatan'] != 5) {
            header('Location: ' . base_url('/users/print_gaji'));
            exit();
        }

        $this->validation =  \Config\Services::validation();
        $this->mBbm = new mBbm();
        $this->mBidang = new mBidang();
        $this->mAnggaran = new mAnggaran();
        $this->mAnggaranKas = new mAnggaranKas();
        $this->mRealisasisp2d = new mRealisasisp2d();
        $this->mRealisasispj = new mRealisasispj();
        $this->mPegawai = new mPegawai();
        $this->mPelimpahan = new mPelimpahan();

        $this->mAnggaranGaji = new mAnggaranGaji();
        $this->mAnggaranKasGaji = new mAnggaranKasGaji();
        $this->mGajiGaji = new mGajiGaji();
        $this->mGajiBbm = new mGajiBbm();
        $this->mGajiTpp = new mGajiTpp();
        $this->mGajiKompensasi = new mGajiKompensasi();

        $this->mRealisasiGajisp2d = new mRealisasiGajisp2d();
        $this->mRealisasiGajispj = new mRealisasiGajispj();
    }

    public function index()
    {
        $data = [
            'title' => 'Report - Diskominfo',
            'page' => 'Report Per Bulan'
        ];

        if (isset($_GET['periode_search'])) {
            $data_exp = explode("-", $_GET['periode_search']);
            $tahun = $data_exp[0];
            $bulan = $data_exp[1];
        } else {
            $tahun = date('Y');
            $bulan = date('n');
        }

        $data['tahun'] = $tahun;
        $array_bulan = ['', 'JANUARI', 'FEBRUARI', 'MARET', 'APRIL', 'MEI', 'JUNI', 'JULI', 'AGUSTUS', 'SEPTEMBER', 'OKTOBER', 'NOVEMBER', 'DESEMBER'];
        $data['bulan'] = $array_bulan[$bulan];

        if ($_SESSION['data']['id_jabatan'] == 1) {
            $where = "tahun_anggaran = '" . $tahun . "' AND id_pegawai = " . $_SESSION['data']['id_pegawai'];
        } else {
            $where = "tahun_anggaran = '$tahun'";
        }
        $data_ = $this->mAnggaran->where($where)->findAll();
        $jml_dpa = 0;
        $jml_sp2d = 0;
        $jml_spj = 0;
        $jml_spjdpa = 0;
        $jml_sp2ddpa = 0;
        $jml_sisa_dpa = 0;
        $jml_sisa_pagu = 0;
        $jml_sisa_kas = 0;
        $data['data'] = array();

        foreach ($data_ as $key => $value) {
            $data_mentah['jumlah_dpa'] = $value['jumlah_dpa'];
            $data_mentah['id_bidang'] = $value['id_bidang'];
            $id_anggaran = $value['id'];
            $data_sp2d = $this->mRealisasisp2d->where('id_anggaran', $id_anggaran)->find();
            $data_mentah['sp2d'] = $data_sp2d[0]['bulan_' . $bulan];
            $data_spj = $this->mRealisasispj->where('id_anggaran', $id_anggaran)->find();
            $data_mentah['spj'] = $data_spj[0]['bulan_' . $bulan];
            $data_mentah['spj/dpa'] = ($data_mentah['spj'] / $data_mentah['jumlah_dpa']) * 100;
            $data_mentah['sp2d/dpa'] = ($data_mentah['sp2d'] / $data_mentah['jumlah_dpa']) * 100;
            $data_mentah['sisa_dpa'] = $data_mentah['jumlah_dpa'] - $data_mentah['sp2d'];
            $data_mentah['sisa_pagu_anggaran'] = $data_mentah['jumlah_dpa'] - $data_mentah['spj'];
            $data_mentah['sisa_kas'] = $data_mentah['sp2d'] - $data_mentah['spj'];
            $data['data'][] = $data_mentah;
            $jml_dpa = $jml_dpa + $data_mentah['jumlah_dpa'];
            $jml_sp2d = $jml_sp2d + $data_mentah['sp2d'];
            $jml_spj = $jml_spj + $data_mentah['spj'];
        }

        $jml_spjdpa = ($jml_spj > 0 && $jml_dpa) ? ($jml_spj / $jml_dpa) * 100 : 0;
        $jml_sp2ddpa = ($jml_sp2d > 0 && $jml_dpa) ? ($jml_sp2d / $jml_dpa) * 100 : 0;
        $jml_sisa_dpa = $jml_dpa - $jml_sp2d;
        $jml_sisa_pagu = $jml_dpa - $jml_spj;
        $jml_sisa_kas = $jml_sp2d - $jml_spj;

        $data['jml_dpa'] = $jml_dpa;
        $data['jml_sp2d'] = $jml_sp2d;
        $data['jml_spj'] = $jml_spj;
        $data['jml_spjdpa'] = $jml_spjdpa;
        $data['jml_sp2ddpa'] = $jml_sp2ddpa;
        $data['jml_sisa_dpa'] = $jml_sisa_dpa;
        $data['jml_sisa_pagu'] = $jml_sisa_pagu;
        $data['jml_sisa_kas'] = $jml_sisa_kas;


        $bidang = $this->mBidang->findAll();
        foreach ($bidang as $value) {
            $data_bidang[$value['id']] = $value['nama_bidang'];
        }

        $data['bidang'] = $data_bidang;

        return view('/Admin/Report/vReportBulan', $data);
    }

    public function bulanGaji()
    {
        $data = [
            'title' => 'Report  - Diskominfo',
            'page' => 'Realisasi Anggaran Bulan'
        ];

        if (isset($_GET['periode_search'])) {
            $data_exp = explode("-", $_GET['periode_search']);
            $tahun = $data_exp[0];
            $bulan = $data_exp[1];
        } else {
            $tahun = date('Y');
            $bulan = date('n');
        }

        $data['tahun'] = $tahun;
        $array_bulan = ['', 'JANUARI', 'FEBRUARI', 'MARET', 'APRIL', 'MEI', 'JUNI', 'JULI', 'AGUSTUS', 'SEPTEMBER', 'OKTOBER', 'NOVEMBER', 'DESEMBER'];
        $data['bulan'] = $array_bulan[$bulan];

        $where = "tahun_anggaran = '" . $tahun . "'";

        $data_ = $this->mAnggaran->where($where)->findAll();
        $jml_dpa = 0;
        $jml_sp2d = 0;
        $jml_spj = 0;
        $jml_spjdpa = 0;
        $jml_sp2ddpa = 0;
        $jml_sisa_dpa = 0;
        $jml_sisa_pagu = 0;
        $jml_spj_percen = 0;
        $jml_sp2d_percen = 0;


        $jml_dpa_gaji = 0;
        $jml_sp2d_gaji = 0;
        $jml_spj_gaji = 0;
        $jml_spjdpa_gaji = 0;
        $jml_sp2ddpa_gaji = 0;
        $jml_sisa_dpa_gaji = 0;
        $jml_sisa_pagu_gaji = 0;
        $jml_spj_percen_gaji = 0;
        $jml_sp2d_percen_gaji = 0;
        $data_gaji = $this->mAnggaranGaji->where($where)->findAll();

        foreach ($data_ as $key => $value) {
            $data_mentah['jumlah_dpa'] = $value['jumlah_dpa'];

            $id_anggaran = $value['id'];
            $data_sp2d = $this->mRealisasisp2d->where('id_anggaran', $id_anggaran)->find();
            $data_mentah['sp2d'] = $data_sp2d[0]['bulan_' . $bulan];
            $data_spj = $this->mRealisasispj->where('id_anggaran', $id_anggaran)->find();
            $data_mentah['spj'] = $data_spj[0]['bulan_' . $bulan];

            $data_kas = $this->mAnggaranKas->where('id_anggaran', $id_anggaran)->find();

            $jml_dpa = $jml_dpa + $data_mentah['jumlah_dpa'];
            $jml_sp2d = $jml_sp2d + $data_mentah['sp2d'];
            $jml_spj = $jml_spj + $data_mentah['spj'];
            $jml_spj_percen = $jml_spj_percen +  $data_kas[0]['bulan_12'];
            $jml_sp2d_percen = $jml_sp2d_percen +  $data_kas[0]['bulan_12'];
        }

        foreach ($data_gaji as $key => $value_) {
            $data_mentah['jumlah_dpa_gaji'] = $value_['jumlah_dpa_gaji'];

            $id_anggaran_gaji = $value_['id'];

            // $data_gaji_ = $this->mGajiGaji->where('id_anggaran_gaji', $id_anggaran_gaji)->find();
            // $data_mentah['gaji'] = $data_gaji_[0]['bulan_' . $bulan];
            // $data_bbm = $this->mGajiBbm->where('id_anggaran_gaji', $id_anggaran_gaji)->find();
            // $data_mentah['bbm'] = $data_bbm[0]['bulan_' . $bulan];
            // $data_kompensasi = $this->mGajiKompensasi->where('id_anggaran_gaji', $id_anggaran_gaji)->find();
            // $data_mentah['kompensasi'] = $data_kompensasi[0]['bulan_' . $bulan];
            // $data_tpp = $this->mGajiTpp->where('id_anggaran_gaji', $id_anggaran_gaji)->find();
            // $data_mentah['tpp'] = $data_tpp[0]['bulan_' . $bulan];

            $data_sp2d_gaji = $this->mRealisasiGajisp2d->where('id_anggaran_gaji', $id_anggaran_gaji)->find();
            $data_mentah['sp2d_gaji'] = $data_sp2d_gaji[0]['bulan_' . $bulan];
            $data_spj_gaji = $this->mRealisasiGajispj->where('id_anggaran_gaji', $id_anggaran_gaji)->find();
            $data_mentah['spj_gaji'] = $data_spj_gaji[0]['bulan_' . $bulan];

            $data_kas_gaji = $this->mAnggaranKasGaji->where('id_anggaran_gaji', $id_anggaran_gaji)->find();

            $jml_dpa_gaji = $jml_dpa_gaji + $data_mentah['jumlah_dpa_gaji'];
            //$jml_sp2d_gaji = $data_mentah['gaji'] + $data_mentah['bbm'] + $data_mentah['kompensasi'] + $data_mentah['tpp'];
            //$jml_spj_gaji = $jml_sp2d_gaji;
            $jml_sp2d_gaji = $data_mentah['sp2d_gaji'];
            $jml_spj_gaji = $data_mentah['spj_gaji'];

            $jml_spj_percen_gaji = $jml_spj_percen_gaji +  $data_kas_gaji[0]['bulan_12'];
            $jml_sp2d_percen_gaji = $jml_sp2d_percen_gaji +  $data_kas_gaji[0]['bulan_12'];
        }

        $jml_spjdpa = ($jml_spj > 0 && $jml_spj_percen) ? ((int)$jml_spj / (int)$jml_spj_percen) * 100 : 0;
        $jml_sp2ddpa = ($jml_sp2d > 0 && $jml_sp2d_percen) ? ((int)$jml_sp2d / (int)$jml_sp2d_percen) * 100 : 0;
        $jml_sisa_dpa = $jml_dpa - $jml_sp2d;
        $jml_sisa_pagu = $jml_dpa - $jml_spj;

        $jml_spjdpa_gaji = ($jml_spj_gaji > 0 && $jml_spj_percen_gaji) ? ($jml_spj_gaji / $jml_spj_percen_gaji) * 100 : 0;
        $jml_sp2ddpa_gaji = ($jml_sp2d_gaji > 0 && $jml_sp2d_percen_gaji) ? ($jml_sp2d_gaji / $jml_sp2d_percen_gaji) * 100 : 0;
        $jml_sisa_dpa_gaji = $jml_dpa_gaji - $jml_sp2d_gaji;
        $jml_sisa_pagu_gaji = $jml_dpa_gaji - $jml_spj_gaji;

        $data['jml_dpa'] = $jml_dpa;
        $data['jml_sp2d'] = $jml_sp2d;
        $data['jml_spj'] = $jml_spj;
        $data['jml_spjdpa'] = $jml_spjdpa;
        $data['jml_sp2ddpa'] = $jml_sp2ddpa;
        $data['jml_sisa_dpa'] = $jml_sisa_dpa;
        $data['jml_sisa_pagu'] = $jml_sisa_pagu;

        $data['jml_dpa_gaji'] = $jml_dpa_gaji;
        $data['jml_sp2d_gaji'] = $jml_sp2d_gaji;
        $data['jml_spj_gaji'] = $jml_spj_gaji;
        $data['jml_spjdpa_gaji'] = $jml_spjdpa_gaji;
        $data['jml_sp2ddpa_gaji'] = $jml_sp2ddpa_gaji;
        $data['jml_sisa_dpa_gaji'] = $jml_sisa_dpa_gaji;
        $data['jml_sisa_pagu_gaji'] = $jml_sisa_pagu_gaji;

        $jml_dpa_all = $jml_dpa + $jml_dpa_gaji;
        $jml_sp2d_all = $jml_sp2d + $jml_sp2d_gaji;
        $jml_spj_all = $jml_spj + $jml_spj_gaji;

        $jml_spjdpa_all = ($jml_spj_all > 0 && $jml_dpa_all) ? ($jml_spj_all / $jml_dpa_all) * 100 : 0;
        $jml_sp2ddpa_all = ($jml_sp2d_all > 0 && $jml_dpa_all) ? ($jml_sp2d_all / $jml_dpa_all) * 100 : 0;
        $jml_sisa_dpa_all = $jml_dpa_all - $jml_sp2d_all;
        $jml_sisa_pagu_all = $jml_dpa_all - $jml_spj_all;

        $data['jml_dpa_all'] = $jml_dpa_all;
        $data['jml_sp2d_all'] = $jml_sp2d_all;
        $data['jml_spj_all'] = $jml_spj_all;
        $data['jml_spjdpa_all'] = $jml_spjdpa_all;
        $data['jml_sp2ddpa_all'] = $jml_sp2ddpa_all;
        $data['jml_sisa_dpa_all'] = $jml_sisa_dpa_all;
        $data['jml_sisa_pagu_all'] = $jml_sisa_pagu_all;


        //dd($data);

        return view('/Admin/Report/vReportBulanGaji', $data);
    }

    public function tahun()
    {
        if ($_SESSION['data']['id_jabatan'] != 3 && $_SESSION['data']['id_jabatan'] != 2 && $_SESSION['data']['id_jabatan'] != 6) {
            redirect()->to(base_url('/users/print_gaji'));
        }
        $data = [
            'title' => 'Report - Diskominfo',
            'page' => 'Report Per Tahun'
        ];

        if (isset($_GET['periode_search'])) {
            $tahun = $_GET['periode_search'];
        } else {
            $tahun = date('Y');
        }

        $data['tahun'] = $tahun;

        $data_ = $this->mAnggaran->where('tahun_anggaran', $tahun)->findAll();

        foreach ($data_ as $key => $value) {
            $data_mentah = array();
            $data_mentah['id'] = $value['id'];
            $data_mentah['id_bidang'] = $value['id_bidang'];
            $data_mentah['anggaran_kas'] = $this->mAnggaranKas->where('id_anggaran', $value['id'])->find();
            $data_mentah['sp2d'] = $this->mRealisasisp2d->where('id_anggaran', $value['id'])->find();
            $data_mentah['spj'] = $this->mRealisasispj->where('id_anggaran', $value['id'])->find();
            $data['data'][] = $data_mentah;
        }

        $bidang = $this->mBidang->findAll();
        foreach ($bidang as $value) {
            $data_bidang[$value['id']] = $value['nama_bidang'];
        }

        $data['bidang'] = $data_bidang;
        $data['tahun'] = $tahun;

        return view('/Admin/Report/vReportTahun', $data);
    }

    public function tahunGaji()
    {
        $db = \Config\Database::connect();

        if ($_SESSION['data']['id_jabatan'] != 3 && $_SESSION['data']['id_jabatan'] != 2 && $_SESSION['data']['id_jabatan'] != 6) {
            redirect()->to(base_url('/users/print_gaji'));
        }
        $data = [
            'title' => 'Report - Diskominfo',
            'page' => 'Report Per Tahun'
        ];

        if (isset($_GET['periode_search'])) {
            $tahun = $_GET['periode_search'];
        } else {
            $tahun = date('Y');
        }

        $data['tahun'] = $tahun;

        $where = "tahun_anggaran = '" . $tahun . "'";

        $data_gaji = $this->mAnggaranGaji->where($where)->findAll();
        if (count($data_gaji) > 0) {

            foreach ($data_gaji as $key => $value_) {
                $data_mentah['jumlah_dpa_gaji'] = $value_['jumlah_dpa_gaji'];

                $id_anggaran_gaji = $value_['id'];
                $data_anggaran_kas_gaji = $this->mAnggaranKasGaji->where('id_anggaran_gaji', $id_anggaran_gaji)->find();

                $data_gaji_ = $this->mGajiGaji->where('id_anggaran_gaji', $id_anggaran_gaji)->find();
                $data_bbm = $this->mGajiBbm->where('id_anggaran_gaji', $id_anggaran_gaji)->find();
                $data_kompensasi = $this->mGajiKompensasi->where('id_anggaran_gaji', $id_anggaran_gaji)->find();
                $data_tpp = $this->mGajiTpp->where('id_anggaran_gaji', $id_anggaran_gaji)->find();

                $data_sp2d_gaji = $this->mRealisasiGajisp2d->where('id_anggaran_gaji', $id_anggaran_gaji)->find();
                $data_spj_gaji = $this->mRealisasiGajispj->where('id_anggaran_gaji', $id_anggaran_gaji)->find();


                $jml_anggaran_kas_gaji = $data_anggaran_kas_gaji;
                for ($i = 1; $i < 13; $i++) {
                    //$jml_sp2d_gaji_['bulan_' . $i] = $data_gaji_[0]['bulan_' . $i] + $data_bbm[0]['bulan_' . $i] + $data_kompensasi[0]['bulan_' . $i] + $data_tpp[0]['bulan_' . $i];
                    $jml_sp2d_gaji_['bulan_' . $i] = $data_sp2d_gaji[0]['bulan_' . $i];
                    $jml_spj_gaji_['bulan_' . $i] = $data_spj_gaji[0]['bulan_' . $i];
                }

                $jml_sp2d_gaji[] = $jml_sp2d_gaji_;
                $jml_spj_gaji[] = $jml_spj_gaji_;
            }


            $q_anggaran_kas = $db->query("SELECT sum(bulan_1) as bulan_1,sum(bulan_2) as bulan_2,sum(bulan_3) as bulan_3,sum(bulan_4) as bulan_4,
            sum(bulan_5) as bulan_5,sum(bulan_6) as bulan_6,sum(bulan_7) as bulan_7,sum(bulan_8) as bulan_8,sum(bulan_9) as bulan_9,
            sum(bulan_10) as bulan_10,sum(bulan_11) as bulan_11,sum(bulan_12) as bulan_12 
            FROM anggaran_kas WHERE id_anggaran in (SELECT id from anggaran where tahun_anggaran=" . $tahun . ")");
            $data_anggaran_kas = $q_anggaran_kas->getResultArray();


            $q_sp2d = $db->query("SELECT sum(bulan_1) as bulan_1,sum(bulan_2) as bulan_2,sum(bulan_3) as bulan_3,sum(bulan_4) as bulan_4,
            sum(bulan_5) as bulan_5,sum(bulan_6) as bulan_6,sum(bulan_7) as bulan_7,sum(bulan_8) as bulan_8,sum(bulan_9) as bulan_9,
            sum(bulan_10) as bulan_10,sum(bulan_11) as bulan_11,sum(bulan_12) as bulan_12 
            FROM realisasi_sp2d WHERE id_anggaran in (SELECT id from anggaran where tahun_anggaran=" . $tahun . ")");
            $data_sp2d = $q_sp2d->getResultArray();


            $q_spj = $db->query("SELECT sum(bulan_1) as bulan_1,sum(bulan_2) as bulan_2,sum(bulan_3) as bulan_3,sum(bulan_4) as bulan_4,
            sum(bulan_5) as bulan_5,sum(bulan_6) as bulan_6,sum(bulan_7) as bulan_7,sum(bulan_8) as bulan_8,sum(bulan_9) as bulan_9,
            sum(bulan_10) as bulan_10,sum(bulan_11) as bulan_11,sum(bulan_12) as bulan_12  
            FROM realisasi_spj WHERE id_anggaran in (SELECT id from anggaran where tahun_anggaran=" . $tahun . ")");
            $data_spj = $q_spj->getResultArray();

            for ($i = 0; $i < 12; $i++) {
                $data_anggaran_kas_all['bulan_' . ($i + 1)] = $jml_anggaran_kas_gaji[0]['bulan_' . ($i + 1)] + $data_anggaran_kas[0]['bulan_' . ($i + 1)];
                $data_sp2d_all['bulan_' . ($i + 1)] = $jml_sp2d_gaji[0]['bulan_' . ($i + 1)] + $data_sp2d[0]['bulan_' . ($i + 1)];
                $data_spj_all['bulan_' . ($i + 1)] = $jml_spj_gaji[0]['bulan_' . ($i + 1)] + $data_spj[0]['bulan_' . ($i + 1)];
            }
            $data['data'][] = [$data_anggaran_kas_all, $data_sp2d_all, $data_spj_all];
            $data['data'][] = [$jml_anggaran_kas_gaji[0], $jml_sp2d_gaji[0], $jml_spj_gaji[0]];
            $data['data'][] = [$data_anggaran_kas[0], $data_sp2d[0], $data_spj[0]];

            //dd($data['data']);
            $data['tahun'] = $tahun;
            # code...
        } else {
            $pesan = 'Tidak ada report data yang bisa di tampilkan';
            $status_pesan = 'danger';
            session()->setFlashdata('result', ['status' => $status_pesan, 'message' => $pesan]);
        }
        return view('/Admin/Report/vReportTahunGaji', $data);
    }


    //--------------------------------------------------------------------

}
