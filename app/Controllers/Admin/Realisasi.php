<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\mBbm;

use App\Models\mBidang;
use App\Models\mPegawai;
use App\Models\mAnggaran;
use App\Models\mAnggaranKas;
use App\Models\mRealisasisp2d;
use App\Models\mRealisasispj;
use App\Models\mPelimpahan;
use App\Models\mDetailPelimpahan;

class Realisasi extends BaseController
{
    protected $validation;
    protected $mBbm;
    protected $mBidang;
    protected $mAnggaran;
    protected $mAnggaranKas;
    protected $mRealisasisp2d;
    protected $mRealisasispj;
    protected $mPegawai;
    protected $mPelimpahan;
    protected $mDetailPelimpahan;
    public function __construct()
    {
        session();
        if (!isset($_SESSION['data'])) {
            header('Location: ' . base_url());
            exit();
        }
        $this->validation =  \Config\Services::validation();
        $this->mBbm = new mBbm();
        $this->mBidang = new mBidang();
        $this->mAnggaran = new mAnggaran();
        $this->mAnggaranKas = new mAnggaranKas();
        $this->mRealisasisp2d = new mRealisasisp2d();
        $this->mRealisasispj = new mRealisasispj();
        $this->mPegawai = new mPegawai();
        $this->mPelimpahan = new mPelimpahan();
        $this->mDetailPelimpahan = new mDetailPelimpahan();
    }

    public function index($id)
    {
        $data_a = $this->mAnggaran->where('unique_id', $id)->find();
        $id_anggaran = $data_a[0]['id'];
        $data = [
            'title' => 'Realisasi - Diskominfo',
            'page' => 'List Realisasi',
            'anggaran' => $data_a,
            'validation' => $this->validation,
            'anggaran_kas' => $this->mAnggaranKas->where('id_anggaran', $id_anggaran)->findAll(),
            'realisasi_sp2d' => $this->mRealisasisp2d->where('id_anggaran', $id_anggaran)->findAll(),
            'realisasi_spj' => $this->mRealisasispj->where('id_anggaran', $id_anggaran)->findAll(),
            'pelimpahan' => $this->mPelimpahan->where('id_anggaran', $id_anggaran)->findAll()
        ];

        if (empty($data['anggaran'])) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Data dengan id ' . $id . ' tidak di temukan');
        }

        $data['pegawai'] = $this->mPegawai->find($data['anggaran'][0]['id_pegawai']);
        $data['bidang'] = $this->mBidang->find($data['anggaran'][0]['id_bidang']);

        return view('/Admin/Realisasi/vRealisasi', $data);
    }

    public function index_bpp($id = 0)
    {
        $data = [
            'title' => 'Realisasi - Diskominfo',
            'page' => 'List Realisasi',
            'anggaran' => $this->mAnggaran->find($id),
            'anggaran_kas' => $this->mAnggaranKas->where('id_anggaran', $id)->findAll(),
            'realisasi_sp2d' => $this->mRealisasisp2d->where('id_anggaran', $id)->findAll(),
            'realisasi_spj' => $this->mRealisasispj->where('id_anggaran', $id)->findAll(),
            'realisasi_pelimpahan' => $this->mRealisasispj->where('id_anggaran', $id)->findAll()
        ];

        $data['bidang'] = $this->mBidang->find($data['anggaran']['id_bidang']);
        $data['pegawai'] = $this->mPegawai->find($data['anggaran']['id_pegawai']);

        return view('/Admin/Realisasi/vRealisasi_bpp', $data);
    }

    public function index_pelimpahan($id)
    {
        $data_a = $this->mAnggaran->where('unique_id', $id)->find();
        $id_anggaran = $data_a[0]['id'];
        $data = [
            'title' => 'Realisasi - Diskominfo',
            'page' => 'List Pelimpahan',
            'anggaran' => $data_a,
            'validation' => $this->validation,
            'realisasi_spj' => $this->mRealisasispj->where('id_anggaran', $id_anggaran)->findAll(),
            'pelimpahan' => $this->mPelimpahan->where('id_anggaran', $id_anggaran)->findAll(),
            'detail_pelimpahan' => $this->mDetailPelimpahan->where('id_anggaran', $id_anggaran)->findAll()
        ];

        if (empty($data['anggaran'])) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Data dengan id ' . $id . ' tidak di temukan');
        }

        $pegawai = $this->mPegawai->findAll();
        foreach ($pegawai as $value) {
            $data_pegawai[$value['id']] = $value['nama_pegawai'];
        }

        $data['pegawai'] = $data_pegawai;
        $data['bidang'] = $this->mBidang->find($data['anggaran'][0]['id_bidang']);

        return view('/Admin/Realisasi/vRealisasiPelimpahan', $data);
    }

    public function store()
    {
        $data = $this->request->getVar();
        if ($this->validation->run($data, 'isiRealisasi') == FALSE) {
            return redirect()->to('/realisasi/edit/' . $data['unique_id'])->withInput();
        } else {
            $data_anggaranKas =  array(
                'bulan_1' => $data['anggaran_kas_bulan_1'],
                'bulan_2' => $data['anggaran_kas_bulan_2'],
                'bulan_3' => $data['anggaran_kas_bulan_3'],
                'bulan_4' => $data['anggaran_kas_bulan_4'],
                'bulan_5' => $data['anggaran_kas_bulan_5'],
                'bulan_6' => $data['anggaran_kas_bulan_6'],
                'bulan_7' => $data['anggaran_kas_bulan_7'],
                'bulan_8' => $data['anggaran_kas_bulan_8'],
                'bulan_9' => $data['anggaran_kas_bulan_9'],
                'bulan_10' => $data['anggaran_kas_bulan_10'],
                'bulan_11' => $data['anggaran_kas_bulan_11'],
                'bulan_12' => $data['anggaran_kas_bulan_12']
            );

            $jml_anggaranKas = $data['anggaran_kas_bulan_1'] + $data['anggaran_kas_bulan_2'] + $data['anggaran_kas_bulan_3']
                + $data['anggaran_kas_bulan_4'] + $data['anggaran_kas_bulan_5'] + $data['anggaran_kas_bulan_6']
                + $data['anggaran_kas_bulan_7'] + $data['anggaran_kas_bulan_8'] + $data['anggaran_kas_bulan_9']
                + $data['anggaran_kas_bulan_10'] + $data['anggaran_kas_bulan_11'] + $data['anggaran_kas_bulan_12'];

            $data_realisasisp2d =  array(
                'bulan_1' => $data['realisasi_sp2d_bulan_1'],
                'bulan_2' => $data['realisasi_sp2d_bulan_2'],
                'bulan_3' => $data['realisasi_sp2d_bulan_3'],
                'bulan_4' => $data['realisasi_sp2d_bulan_4'],
                'bulan_5' => $data['realisasi_sp2d_bulan_5'],
                'bulan_6' => $data['realisasi_sp2d_bulan_6'],
                'bulan_7' => $data['realisasi_sp2d_bulan_7'],
                'bulan_8' => $data['realisasi_sp2d_bulan_8'],
                'bulan_9' => $data['realisasi_sp2d_bulan_9'],
                'bulan_10' => $data['realisasi_sp2d_bulan_10'],
                'bulan_11' => $data['realisasi_sp2d_bulan_11'],
                'bulan_12' => $data['realisasi_sp2d_bulan_12']
            );

            $jumlah_sp2d = $data['realisasi_sp2d_bulan_1'] + $data['realisasi_sp2d_bulan_2'] + $data['realisasi_sp2d_bulan_3']
                + $data['realisasi_sp2d_bulan_4'] + $data['realisasi_sp2d_bulan_5'] + $data['realisasi_sp2d_bulan_6']
                + $data['realisasi_sp2d_bulan_7'] + $data['realisasi_sp2d_bulan_8'] + $data['realisasi_sp2d_bulan_9']
                + $data['realisasi_sp2d_bulan_10'] + $data['realisasi_sp2d_bulan_11'] + $data['realisasi_sp2d_bulan_12'];

            $data_realisasispj =  array(
                'bulan_1' => $data['realisasi_spj_bulan_1'],
                'bulan_2' => $data['realisasi_spj_bulan_2'],
                'bulan_3' => $data['realisasi_spj_bulan_3'],
                'bulan_4' => $data['realisasi_spj_bulan_4'],
                'bulan_5' => $data['realisasi_spj_bulan_5'],
                'bulan_6' => $data['realisasi_spj_bulan_6'],
                'bulan_7' => $data['realisasi_spj_bulan_7'],
                'bulan_8' => $data['realisasi_spj_bulan_8'],
                'bulan_9' => $data['realisasi_spj_bulan_9'],
                'bulan_10' => $data['realisasi_spj_bulan_10'],
                'bulan_11' => $data['realisasi_spj_bulan_11'],
                'bulan_12' => $data['realisasi_spj_bulan_12']
            );

            $jumlah_spj = $data['realisasi_spj_bulan_1'] + $data['realisasi_spj_bulan_2'] + $data['realisasi_spj_bulan_3']
                + $data['realisasi_spj_bulan_4'] + $data['realisasi_spj_bulan_5'] + $data['realisasi_spj_bulan_6']
                + $data['realisasi_spj_bulan_7'] + $data['realisasi_spj_bulan_8'] + $data['realisasi_spj_bulan_9']
                + $data['realisasi_spj_bulan_10'] + $data['realisasi_spj_bulan_11'] + $data['realisasi_spj_bulan_12'];

            /*$data_pelimpahan =  array(
                'bulan_1' => $data['pelimpahan_bulan_1'],
                'bulan_2' => $data['pelimpahan_bulan_2'],
                'bulan_3' => $data['pelimpahan_bulan_3'],
                'bulan_4' => $data['pelimpahan_bulan_4'],
                'bulan_5' => $data['pelimpahan_bulan_5'],
                'bulan_6' => $data['pelimpahan_bulan_6'],
                'bulan_7' => $data['pelimpahan_bulan_7'],
                'bulan_8' => $data['pelimpahan_bulan_8'],
                'bulan_9' => $data['pelimpahan_bulan_9'],
                'bulan_10' => $data['pelimpahan_bulan_10'],
                'bulan_11' => $data['pelimpahan_bulan_11'],
                'bulan_12' => $data['pelimpahan_bulan_12']
            );

            $jumlah_pelimpahan = $data['pelimpahan_bulan_1'] + $data['pelimpahan_bulan_2'] + $data['pelimpahan_bulan_3']
                + $data['pelimpahan_bulan_4'] + $data['pelimpahan_bulan_5'] + $data['pelimpahan_bulan_6']
                + $data['pelimpahan_bulan_7'] + $data['pelimpahan_bulan_8'] + $data['pelimpahan_bulan_9']
                + $data['pelimpahan_bulan_10'] + $data['pelimpahan_bulan_11'] + $data['pelimpahan_bulan_12'];*/

            $hasil = true;
            $data_anggaran['id'] = $data['id_anggaran'];
            $data_anggaran['anggaran_kas'] = $jml_anggaranKas;
            $data_anggaran['sp2d'] = $jumlah_sp2d;
            $data_anggaran['spj'] = $jumlah_spj;
            /*$data_anggaran['pelimpahan'] = $jumlah_pelimpahan;*/

            $simpan = $this->mAnggaran->save($data_anggaran);
            //dd($data_anggaran);
            $hasil = $hasil & $simpan;

            $data_anggaranKas['id'] = $data['id_anggaran_kas'];
            $data_realisasisp2d['id'] = $data['id_realisasisp2d'];
            $data_realisasispj['id'] = $data['id_realisasispj'];
            //$data_pelimpahan['id'] = $data['id_pelimpahan'];

            $simpan = $this->mAnggaranKas->save($data_anggaranKas);
            $hasil = $hasil & $simpan;
            $simpan = $this->mRealisasisp2d->save($data_realisasisp2d);
            $hasil = $hasil & $simpan;
            $simpan = $this->mRealisasispj->save($data_realisasispj);
            $hasil = $hasil & $simpan;
            //$simpan = $this->mPelimpahan->save($data_pelimpahan);
            //$hasil = $hasil & $simpan;
            if (!$hasil) {
                $pesan = 'data yang gagal di insert';
                $status_pesan = 'danger';
            } else {
                $pesan = 'Berhasil insert data';
                $status_pesan = 'success';
            }
            session()->setFlashdata('result', ['status' => $status_pesan, 'message' => $pesan]);
            return redirect()->to(base_url('/anggaran'));
        }
    }

    public function store_pelimpahan()
    {
        $data = $this->request->getVar();
        //d($data);
        if ($this->validation->run($data, 'isiRealisasiPe') == FALSE) {
            return redirect()->to('/realisasi/edit_pelimpahan/' . $data['unique_id'])->withInput();
            //dd($this->validation);
        } else {
            $data_pelimpahan =  array(
                'bulan_1' => 0,
                'bulan_2' => 0,
                'bulan_3' => 0,
                'bulan_4' => 0,
                'bulan_5' => 0,
                'bulan_6' => 0,
                'bulan_7' => 0,
                'bulan_8' => 0,
                'bulan_9' => 0,
                'bulan_10' => 0,
                'bulan_11' => 0,
                'bulan_12' => 0
            );

            $hasil = true;

            for ($i = 0; $i < $data['index_count']; $i++) {
                $data_pelimpahan['bulan_1'] =  $data_pelimpahan['bulan_1'] + $data['pelimpahan_bulan_1_' . ($i + 1)];
                $data_pelimpahan['bulan_2'] =  $data_pelimpahan['bulan_2'] + $data['pelimpahan_bulan_2_' . ($i + 1)];
                $data_pelimpahan['bulan_3'] =  $data_pelimpahan['bulan_3'] + $data['pelimpahan_bulan_3_' . ($i + 1)];
                $data_pelimpahan['bulan_4'] =  $data_pelimpahan['bulan_4'] + $data['pelimpahan_bulan_4_' . ($i + 1)];
                $data_pelimpahan['bulan_5'] =  $data_pelimpahan['bulan_5'] + $data['pelimpahan_bulan_5_' . ($i + 1)];
                $data_pelimpahan['bulan_6'] =  $data_pelimpahan['bulan_6'] + $data['pelimpahan_bulan_6_' . ($i + 1)];
                $data_pelimpahan['bulan_7'] =  $data_pelimpahan['bulan_7'] + $data['pelimpahan_bulan_7_' . ($i + 1)];
                $data_pelimpahan['bulan_8'] =  $data_pelimpahan['bulan_8'] + $data['pelimpahan_bulan_8_' . ($i + 1)];
                $data_pelimpahan['bulan_9'] =  $data_pelimpahan['bulan_9'] + $data['pelimpahan_bulan_9_' . ($i + 1)];
                $data_pelimpahan['bulan_10'] =  $data_pelimpahan['bulan_10'] + $data['pelimpahan_bulan_10_' . ($i + 1)];
                $data_pelimpahan['bulan_11'] =  $data_pelimpahan['bulan_11'] + $data['pelimpahan_bulan_11_' . ($i + 1)];
                $data_pelimpahan['bulan_12'] =  $data_pelimpahan['bulan_12'] + $data['pelimpahan_bulan_12_' . ($i + 1)];


                $data_detail_pelimpahan['id'] =  $data['id_detail_pelimpahan_' . ($i + 1)];
                $data_detail_pelimpahan['bulan_1'] =  $data['pelimpahan_bulan_1_' . ($i + 1)];
                $data_detail_pelimpahan['bulan_2'] =  $data['pelimpahan_bulan_2_' . ($i + 1)];
                $data_detail_pelimpahan['bulan_3'] =  $data['pelimpahan_bulan_3_' . ($i + 1)];
                $data_detail_pelimpahan['bulan_4'] =  $data['pelimpahan_bulan_4_' . ($i + 1)];
                $data_detail_pelimpahan['bulan_5'] =  $data['pelimpahan_bulan_5_' . ($i + 1)];
                $data_detail_pelimpahan['bulan_6'] =  $data['pelimpahan_bulan_6_' . ($i + 1)];
                $data_detail_pelimpahan['bulan_7'] =  $data['pelimpahan_bulan_7_' . ($i + 1)];
                $data_detail_pelimpahan['bulan_8'] =  $data['pelimpahan_bulan_8_' . ($i + 1)];
                $data_detail_pelimpahan['bulan_9'] =  $data['pelimpahan_bulan_9_' . ($i + 1)];
                $data_detail_pelimpahan['bulan_10'] =  $data['pelimpahan_bulan_10_' . ($i + 1)];
                $data_detail_pelimpahan['bulan_11'] =  $data['pelimpahan_bulan_11_' . ($i + 1)];
                $data_detail_pelimpahan['bulan_12'] =  $data['pelimpahan_bulan_12_' . ($i + 1)];
                $simpan = $this->mDetailPelimpahan->save($data_detail_pelimpahan);
                $hasil = $hasil & $simpan;
            }


            $jumlah_pelimpahan = $data_pelimpahan['bulan_1'] + $data_pelimpahan['bulan_2'] + $data_pelimpahan['bulan_3']
                + $data_pelimpahan['bulan_4'] + $data_pelimpahan['bulan_5'] + $data_pelimpahan['bulan_6']
                + $data_pelimpahan['bulan_7'] + $data_pelimpahan['bulan_8'] + $data_pelimpahan['bulan_9'] + $data_pelimpahan['bulan_10']
                + $data_pelimpahan['bulan_11'] + $data_pelimpahan['bulan_12'];




            $data_anggaran['id'] = $data['id_anggaran'];
            $data_anggaran['pelimpahan'] = $jumlah_pelimpahan;

            $simpan = $this->mAnggaran->save($data_anggaran);
            //dd($data_anggaran);
            $hasil = $hasil & $simpan;
            $data_pelimpahan['id'] = $data['id_pelimpahan'];

            $simpan = $this->mPelimpahan->save($data_pelimpahan);
            $hasil = $hasil & $simpan;
            if (!$hasil) {
                $pesan = 'data yang gagal di insert';
                $status_pesan = 'danger';
            } else {
                $pesan = 'Berhasil insert data';
                $status_pesan = 'success';
            }
            session()->setFlashdata('result', ['status' => $status_pesan, 'message' => $pesan]);
            return redirect()->to(base_url('/anggaran'));
        }
    }

    //--------------------------------------------------------------------

}
