<?php

namespace App\Controllers;

use App\Models\mPegawai;

class Login extends BaseController
{
    protected $mPegawai;
    public function __construct()
    {
        $this->validation =  \Config\Services::validation();
        $this->mPegawai = new mPegawai();
    }
    public function index()
    {
        if (isset($_SESSION['data'])) {
            return redirect()->to(base_url('dashboard'));
        }

        $data = [
            'title' => 'Login - Diskominfo'
        ];
        return view('vLogin', $data);
    }

    public function checkLogin()
    {
        $data_login = $this->request->getVar();
        $data = $this->mPegawai->where('nip_pegawai', $data_login['nip_pegawai'])->where('password_pegawai', $data_login['password_pegawai'])->where('status', 1)->findAll();
        //dd($data);

        //
        if (count($data) > 0) {
            $_SESSION['data']['id_pegawai'] = $data[0]['id'];
            $_SESSION['data']['nip_pegawai'] = $data[0]['nip_pegawai'];
            $_SESSION['data']['unique_id'] = $data[0]['unique_id'];
            $_SESSION['data']['nama_pegawai'] = $data[0]['nama_pegawai'];
            $_SESSION['data']['id_bidang'] = $data[0]['id_bidang'];
            $_SESSION['data']['id_jabatan'] = $data[0]['id_jabatan'];

            return redirect()->to(base_url('/users/print_gaji'));
        } else {
            session()->setFlashdata('result', ['status' => 'danger', 'message' => 'nip atau password mu salah']);
            return redirect()->to(base_url());
        }
    }

    public function logout()
    {
        session_destroy();
        return redirect()->to(base_url());
    }

    //--------------------------------------------------------------------

}
