<?php

namespace App\Models;

use CodeIgniter\Model;

class mTpp extends Model
{
    protected $table = 'salary_tpp';
    protected $useTimestamps = true;
    protected $allowedFields = [
        'kodin',
        'rek_internal',
        'rek_eksternal',
        'nama',
        'tpp',
        'zakat',
        'simpanan_kkps',
        'potongan_kkps',
        'ptg_cab_utama',
        'ptg_gd_sate',
        'ptg_otista',
        'pot_dinas',
        'tpp_bersih'
    ];
}
