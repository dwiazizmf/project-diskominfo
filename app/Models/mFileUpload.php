<?php

namespace App\Models;

use CodeIgniter\Model;

class mFileUpload extends Model
{
    protected $table = 'file_upload';
    protected $useTimestamps = true;
    protected $allowedFields = [
        'nama_file',
        'keterangan',
        'file_name',
        'user_uploaded'
    ];
}
