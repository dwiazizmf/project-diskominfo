<?php

namespace App\Models;

use CodeIgniter\Model;

class mAnggaran extends Model
{
    protected $table = 'anggaran';
    protected $useTimestamps = true;
    protected $allowedFields = [
        'unique_id',
        'id_bidang',
        'id_pegawai',
        'id_pegawai_2',
        'tahun_anggaran',
        'jumlah_dpa',
        'anggaran_kas',
        'sp2d',
        'spj',
        'pelimpahan',
        'confirm_1',
        'confirm_2',
        'user_confirm_1',
        'user_confirm_2',
        'user_inserted'
    ];
}
