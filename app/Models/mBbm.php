<?php

namespace App\Models;

use CodeIgniter\Model;

class mBbm extends Model
{
    protected $table = 'salary_bbm';
    protected $useTimestamps = true;
    protected $allowedFields = [
        'kodin',
        'rek_internal',
        'rek_eksternal',
        'nama',
        'standar_biasa',
        'pph_21',
        'pot_dinas',
        'jumlah_bersih'
    ];
}
