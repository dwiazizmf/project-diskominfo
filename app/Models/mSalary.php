<?php

namespace App\Models;

use CodeIgniter\Model;

class mSalary extends Model
{
    protected $table = 'salary';
    protected $useTimestamps = true;
    protected $allowedFields = [
        'kodin',
        'rek_internal',
        'rek_eksternal',
        'nama',
        'gaji_kotor',
        'angs_cab_utama',
        'angs_kab_bdg',
        'angs_suci',
        'angs_bubat',
        'angs_gd_sate',
        'angs_otista',
        'angs_asia_afrika',
        'pot_dinas',
        'gaji_bersih',
    ];
}
