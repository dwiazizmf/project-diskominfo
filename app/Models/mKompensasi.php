<?php

namespace App\Models;

use CodeIgniter\Model;

class mKompensasi extends Model
{
    protected $table = 'salary_kompensasi';
    protected $useTimestamps = true;
    protected $allowedFields = [
        'kodin',
        'rek_internal',
        'rek_eksternal',
        'nama',
        'tpp',
        'pph_21',
        'simpanan_kas',
        'potongan_kkps',
        'pot_dinas'
    ];
}
