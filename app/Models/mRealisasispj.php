<?php

namespace App\Models;

use CodeIgniter\Model;

class mRealisasispj extends Model
{
    protected $table = 'realisasi_spj';
    protected $useTimestamps = true;
    protected $allowedFields = [
        'id_anggaran',
        'bulan_1',
        'bulan_2',
        'bulan_3',
        'bulan_4',
        'bulan_5',
        'bulan_6',
        'bulan_7',
        'bulan_8',
        'bulan_9',
        'bulan_10',
        'bulan_11',
        'bulan_12',
        'total_realisasi',
    ];
}
