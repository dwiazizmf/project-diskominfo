<?php

namespace App\Models;

use CodeIgniter\Model;

class mGaji extends Model
{
    protected $table = 'gaji';
    protected $useTimestamps = true;
    protected $allowedFields = [
        'periode',
        'tahun',
        'bulan',
        'nama',
        'nip',
        'pangkat',
        'golongan_pangkat',
        'nama_jabatan',
        'es',
        'status',
        'jiwa',
        'anak',
        'bulan_gaji',
        'gaji_pokok',
        'tunjangan_issu',
        'tunjangan_anak',
        'total_gaji_bersih',
        'tunjangan_struktural',
        'tunjangan_beras',
        'tunjangan_pajak_penghasilan',
        'pembulatan',
        'penghasilan_kotor',
        'iwp',
        'askes',
        'pph21',
        'bapetarum',
        'jumlah_potongan',
        'penghasilan_bersih',
        'sim_kompak',
        'pot_kompak',
        'pot_bjb',
        'pot_dw',
        'pot_kopri',
        'total_pot',
        'gaji_set_pot',
        'bbm',
        'bbm_pph21',
        'bbm_bersih',
        'kompensasi',
        'kompensasi_pph21',
        'pot_kop_kompen',
        'jumlah_pot_komponen',
        'kompensasi_bersih',
        'tpp',
        'persen',
        'bruto',
        'pph21_tpp',
        'tpp_net',
        'zakat',
        'kpps',
        'a_kpps',
        'a_bjb',
        'a_kompak',
        'a_syariah',
        'a_total_pot',
        'tpp_bersih',
        'total_penerimaan_gaji'
    ];
}
