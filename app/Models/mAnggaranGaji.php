<?php

namespace App\Models;

use CodeIgniter\Model;

class mAnggaranGaji extends Model
{
    protected $table = 'anggaran_gaji';
    protected $useTimestamps = true;
    protected $allowedFields = [
        'unique_id',
        'user_inserted',
        'tahun_anggaran',
        'jumlah_dpa_gaji',
        'anggaran_kas_gaji',
        'sp2d_gaji',
        'spj_gaji',
        'gaji',
        'bbm',
        'tpp',
        'kompensasi'
    ];
}
