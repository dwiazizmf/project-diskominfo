<?php

namespace App\Models;

use CodeIgniter\Model;

class mRealisasiGajisp2d extends Model
{
    protected $table = 'realisasi_gaji_sp2d';
    protected $useTimestamps = true;
    protected $allowedFields = [
        'id_anggaran_gaji',
        'bulan_1',
        'bulan_2',
        'bulan_3',
        'bulan_4',
        'bulan_5',
        'bulan_6',
        'bulan_7',
        'bulan_8',
        'bulan_9',
        'bulan_10',
        'bulan_11',
        'bulan_12',
        'total_realisasi',
    ];
}
