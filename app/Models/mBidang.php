<?php

namespace App\Models;

use CodeIgniter\Model;

class mBidang extends Model
{
    protected $table = 'bidang';
    protected $useTimestamps = true;
    protected $allowedFields = [
        'kode_bidang',
        'nama_bidang',
        'keterangan'
    ];
}
