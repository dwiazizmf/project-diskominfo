<?php

namespace App\Models;

use CodeIgniter\Model;

class mPegawai extends Model
{
    protected $table = 'pegawai';
    protected $useTimestamps = true;
    protected $allowedFields = [
        'unique_id',
        'nip_pegawai',
        'password_pegawai',
        'nama_pegawai',
        'pangkat',
        'gol_pangkat',
        'nama_jabatan',
        'id_bidang',
        'id_jabatan',
        'status'
    ];
}
