<?php

namespace App\Models;

use CodeIgniter\Model;

class mJabatan extends Model
{
    protected $table = 'jabatan';
    protected $useTimestamps = true;
    protected $allowedFields = [
        'kode_jabatan',
        'nama_jabatan',
        'keterangan'
    ];
}
