<?php

namespace Config;

class Validation
{
	//--------------------------------------------------------------------
	// Setup
	//--------------------------------------------------------------------

	/**
	 * Stores the classes that contain the
	 * rules that are available.
	 *
	 * @var array
	 */
	public $ruleSets = [
		\CodeIgniter\Validation\Rules::class,
		\CodeIgniter\Validation\FormatRules::class,
		\CodeIgniter\Validation\FileRules::class,
		\CodeIgniter\Validation\CreditCardRules::class,
	];

	/**
	 * Specifies the views that are used to display the
	 * errors.
	 *
	 * @var array
	 */
	public $templates = [
		'list'   => 'CodeIgniter\Validation\Views\list',
		'single' => 'CodeIgniter\Validation\Views\single',
	];

	//--------------------------------------------------------------------
	// Rules

	/** Rules Uplaod data */
	public $uploadFile = [
		'file-upload'     => 'uploaded[file-upload]|ext_in[file-upload,xls,xlsx]|max_size[file-upload,1000]',
		'periode'		  => 'required'
	];

	public $uploadFile_errors = [
		'file-upload' => [
			'ext_in'    => 'File Excel hanya boleh diisi dengan xls atau xlsx.',
			'max_size'  => 'File Excel product maksimal 1mb',
			'uploaded'  => 'File Excel product wajib diisi'
		],
		'periode' => [
			'required'  => 'Kolom {field} wajib diisi'
		]
	];

	public $uploadFile_file = [
		'file-upload'     => 'uploaded[file-upload]|max_size[file-upload,4000]',
		'nama_file'		  => 'required',
		'keterangan'	  => 'required'
	];

	public $uploadFile_file_errors = [
		'file-upload' => [
			'max_size'  => 'File maksimal 4mb',
			'uploaded'  => 'File wajib diisi'
		],
		'nama_file' => [
			'required'  => 'Kolom nama file wajib diisi'
		],
		'keterangan' => [
			'required'  => 'Kolom keterangan file wajib diisi'
		]
	];

	/** Rules Edit Salary */
	public $editSalary = [
		'gaji_kotor' => 'required|integer',
		'angs_cab_utama'  => 'required|integer',
		'angs_kab_bdg' => 'required|integer',
		'angs_suci' => 'required|integer',
		'angs_bubat' => 'required|integer',
		'angs_gd_sate' => 'required|integer',
		'angs_otista' => 'required|integer',
		'angs_asia_afrika' => 'required|integer',
		'pot_dinas' => 'required|integer',
		'gaji_bersih' => 'required|integer'
	];

	public $editSalary_errors = [
		'gaji_kotor' => [
			'required' => 'Kolom gaji kotor harus diisi',
			'integer' => 'Kolom gaji kotor harus berupa angka'
		],
		'angs_cab_utama'  => [
			'required' => 'Kolom angsuran cb utama harus diisi',
			'integer' => 'Kolom angsuran cb utama harus berupa angka'
		],
		'angs_kab_bdg' => [
			'required' => 'Kolom angsuran kab bandung harus diisi',
			'integer' => 'Kolom angsuran kab bandung harus berupa angka'
		],
		'angs_suci' => [
			'required' => 'Kolom angsuran suci harus diisi',
			'integer' => 'Kolom angsuran suci harus berupa angka'
		],
		'angs_bubat' => [
			'required' => 'Kolom angsuran buah batu harus diisi',
			'integer' => 'Kolom agsuran buah batu harus berupa angka'
		],
		'angs_gd_sate' => [
			'required' => 'Kolom angsuran gd. date harus diisi',
			'integer' => 'Kolom angsuran gd. date harus berupa angka'
		],
		'angs_otista' => [
			'required' => 'Kolom angsuuran otista harus diisi',
			'integer' => 'Kolom angsuran otista harus berupa angka'
		],
		'angs_asia_afrika' => [
			'required' => 'Kolom angsuran asia afrika harus diisi',
			'integer' => 'Kolom angsuran asia afrika harus berupa angka'
		],
		'pot_dinas' => [
			'required' => 'Kolom pot dinas harus diisi',
			'integer' => 'Kolom pot dinas harus berupa angka'
		],
		'gaji_bersih' => [
			'required' => 'Kolom gaji bersih harus diisi',
			'integer' => 'Kolom gaji bersih harus berupa angka'
		]
	];

	/** Rules Edit BBM */
	public $editBbm = [
		'standar_biasa' => 'required|integer',
		'pph_21' => 'required|integer',
		'pot_dinas' => 'required|integer',
		'jumlah_bersih' => 'required|integer'
	];

	public $editBbm_errors = [
		'standar_biasa' => [
			'required' => 'Kolom standar biasa harus diisi',
			'integer' => 'Kolom standar biasa harus berupa angka'
		],
		'pph_21' => [
			'required' => 'Kolom pph 21 harus diisi',
			'integer' => 'Kolom pph 21 harus berupa angka'
		],
		'pot_dinas' => [
			'required' => 'Kolom pot dinas harus diisi',
			'integer' => 'Kolom pot dinas harus berupa angka'
		],
		'jumlah_bersih' => [
			'required' => 'Kolom jumlah bersih harus diisi',
			'integer' => 'Kolom jumlah bersih harus berupa angka'
		]
	];

	/** Rules Edit Kompensasi */
	public $editKompensasi = [
		'tpp' => 'required|integer',
		'pph_21' => 'required|integer',
		'simpanan_kas' => 'required|integer',
		'potongan_kkps' => 'required|integer',
		'pot_dinas' => 'required|integer'
	];

	public $editKompensasi_errors = [
		'tpp' => [
			'required' => 'Kolom TPP harus diisi',
			'integer' => 'Kolom TPP harus berupa angka'
		],
		'pph_21' => [
			'required' => 'Kolom PPH 21 harus diisi',
			'integer' => 'Kolom PPH 21 harus berupa angka'
		],
		'simpanan_kas' => [
			'required' => 'Kolom simpanan kas harus diisi',
			'integer' => 'Kolom simpanan kas harus berupa angka'
		],
		'potongan_kkps' => [
			'required' => 'Kolom potongan KKPS harus diisi',
			'integer' => 'Kolom potongan KKPS harus berupa angka'
		],
		'pot_dinas' => [
			'required' => 'Kolom pot dinas harus diisi',
			'integer' => 'Kolom pot dinas harus berupa angka'
		]
	];

	/** Rules Edit TPP */
	public $editTpp = [
		'tpp' => 'required|integer',
		'zakat' => 'required|integer',
		'simpanan_kkps' => 'required|integer',
		'potongan_kkps' => 'required|integer',
		'ptg_cab_utama' => 'required|integer',
		'ptg_gd_sate' => 'required|integer',
		'ptg_otista' => 'required|integer',
		'pot_dinas' => 'required|integer',
		'tpp_bersih' => 'required|integer'
	];

	public $editTpp_errors = [
		'tpp' => [
			'required' => 'Kolom TPP harus diisi',
			'integer' => 'Kolom TPP harus berupa angka'
		],
		'zakat' => [
			'required' => 'Kolom zakat harus diisi',
			'integer' => 'Kolom zakat harus berupa angka'
		],
		'simpanan_kkps' => [
			'required' => 'Kolom simpanan kkps harus diisi',
			'integer' => 'Kolom simpanan kkps harus berupa angka'
		],
		'potongan_kkps' => [
			'required' => 'Kolom potongan kkps harus diisi',
			'integer' => 'Kolom potongan kkps harus berupa angka'
		],
		'ptg_cab_utama' => [
			'required' => 'Kolom potongan cab utama harus diisi',
			'integer' => 'Kolom potongan cab utama harus berupa angka'
		],
		'ptg_gd_sate' => [
			'required' => 'Kolom potongan gd sate harus diisi',
			'integer' => 'Kolom potongan gd sate harus berupa angka'
		],
		'ptg_otista' => [
			'required' => 'Kolom potongan otista harus diisi',
			'integer' => 'Kolom potongan otista harus berupa angka'
		],
		'pot_dinas' => [
			'required' => 'Kolom potongan dinas harus diisi',
			'integer' => 'Kolom potongan dinas harus berupa angka'
		],
		'tpp_bersih' => [
			'required' => 'Kolom TPP bersih harus diisi',
			'integer' => 'Kolom TPP bersih harus berupa angka'
		]
	];


	/** Rules Edit Kompensasi */
	public $insertUser = [
		'nip_pegawai' => 'required|integer|is_unique[pegawai.nip_pegawai,id,{id}]',
		'nama_pegawai' => 'required',
		'pangkat' => 'required',
		'gol_pangkat' => 'required',
		'nama_jabatan' => 'required',
		'id_jabatan' => 'required'
	];

	public $insertUser_errors = [
		'nip_pegawai' => [
			'required' => 'Kolom NIP harus diisi',
			'integer' => 'Harus berupa angka',
			'is_unique' => 'NIP ini sudah digunakan'
		],
		'password_pegawai' => [
			'required' => 'Kolom password harus diisi',
			'min_length' => 'Minimal harus 5 karakter'
		],
		're_password_pegawai' => [
			'required' => 'Kolom re password harus diisi',
			'matches' => 'password tidak sama'
		],
		'nama_pegawai' => [
			'required' => 'Kolom nama harus diisi'
		],
		'pangkat' => [
			'required' => 'Kolom pangkat harus diisi'
		],
		'gol_pangkat' => [
			'required' => 'Kolom golongan pangkat'
		],
		'nama_jabatan' => [
			'required' => 'Kolom nama jabatan harus diisi'
		],
		'id_jabatan' => [
			'required' => 'Jabatan harus diisi'
		]
	];

	/** Rules Edit Kompensasi */
	public $editPassword = [
		'password_pegawai' => 'required|min_length[5]',
		're_password_pegawai' => 'required|matches[password_pegawai]'
	];

	public $editPassword_errors = [
		'password_pegawai' => [
			'required' => 'Kolom password harus diisi',
			'min_length' => 'Minimal harus 5 karakter'
		],
		're_password_pegawai' => [
			'required' => 'Kolom re password harus diisi',
			'matches' => 'password tidak sama'
		],
	];

	/** Rules Anggaran Gaji*/
	public $insertAnggaran = [
		'tahun_anggaran' => 'required|is_unique[anggaran_gaji.tahun_anggaran]',
		'jumlah_dpa_gaji' => 'required|integer'
	];

	public $insertAnggaran_errors = [
		'tahun_anggaran' => [
			'required' => 'Kolom tahun anggaran harus diisi',
			'is_unique' => 'Tahun anggaran ini sudah ada di database'
		],
		'jumlah_dpa_gaji' => [
			'required' => 'Kolom jumlah DPA gaji harus diisi',
			'integer' => 'Harus berupa angka'
		],
	];


	/** Rules Anggaran Gaji*/
	public $insertAnggaran1 = [
		'tahun_anggaran' => 'required|is_unique[anggaran_gaji.tahun_anggaran]',
		'jumlah_dpa' => 'required|integer',
		'id_pegawai_2' => 'differs[id_pegawai]'
	];

	public $insertAnggaran1_errors = [
		'tahun_anggaran' => [
			'required' => 'Kolom tahun anggaran harus diisi',
			'is_unique' => 'Tahun anggaran ini sudah ada di database'
		],
		'jumlah_dpa' => [
			'required' => 'Kolom jumlah DPA harus diisi',
			'integer' => 'Harus berupa angka'
		],
		'id_pegawai_2' => [
			'differs' => 'BPP 2 harus berbeda dengan BPP 1'
		],
	];

	/** Rules isi realisai*/
	public $isiRealisasi = [
		'anggaran_kas_bulan_1' => 'required|integer',
		'anggaran_kas_bulan_2' => 'required|integer',
		'anggaran_kas_bulan_3' => 'required|integer',
		'anggaran_kas_bulan_4' => 'required|integer',
		'anggaran_kas_bulan_5' => 'required|integer',
		'anggaran_kas_bulan_6' => 'required|integer',
		'anggaran_kas_bulan_7' => 'required|integer',
		'anggaran_kas_bulan_8' => 'required|integer',
		'anggaran_kas_bulan_9' => 'required|integer',
		'anggaran_kas_bulan_10' => 'required|integer',
		'anggaran_kas_bulan_11' => 'required|integer',
		'anggaran_kas_bulan_12' => 'required|integer',
		'realisasi_sp2d_bulan_1' => 'required|integer',
		'realisasi_sp2d_bulan_2' => 'required|integer',
		'realisasi_sp2d_bulan_3' => 'required|integer',
		'realisasi_sp2d_bulan_4' => 'required|integer',
		'realisasi_sp2d_bulan_5' => 'required|integer',
		'realisasi_sp2d_bulan_6' => 'required|integer',
		'realisasi_sp2d_bulan_7' => 'required|integer',
		'realisasi_sp2d_bulan_8' => 'required|integer',
		'realisasi_sp2d_bulan_9' => 'required|integer',
		'realisasi_sp2d_bulan_10' => 'required|integer',
		'realisasi_sp2d_bulan_11' => 'required|integer',
		'realisasi_sp2d_bulan_12' => 'required|integer',
		'realisasi_spj_bulan_1' => 'required|integer',
		'realisasi_spj_bulan_2' => 'required|integer',
		'realisasi_spj_bulan_3' => 'required|integer',
		'realisasi_spj_bulan_4' => 'required|integer',
		'realisasi_spj_bulan_5' => 'required|integer',
		'realisasi_spj_bulan_6' => 'required|integer',
		'realisasi_spj_bulan_7' => 'required|integer',
		'realisasi_spj_bulan_8' => 'required|integer',
		'realisasi_spj_bulan_9' => 'required|integer',
		'realisasi_spj_bulan_10' => 'required|integer',
		'realisasi_spj_bulan_11' => 'required|integer',
		'realisasi_spj_bulan_12' => 'required|integer',
		/*'pelimpahan_bulan_1' => 'required|integer',
		'pelimpahan_bulan_2' => 'required|integer',
		'pelimpahan_bulan_3' => 'required|integer',
		'pelimpahan_bulan_4' => 'required|integer',
		'pelimpahan_bulan_5' => 'required|integer',
		'pelimpahan_bulan_6' => 'required|integer',
		'pelimpahan_bulan_7' => 'required|integer',
		'pelimpahan_bulan_8' => 'required|integer',
		'pelimpahan_bulan_9' => 'required|integer',
		'pelimpahan_bulan_10' => 'required|integer',
		'pelimpahan_bulan_11' => 'required|integer',
		'pelimpahan_bulan_12' => 'required|integer',*/
	];

	public $isiRealisasi_errors = [
		'anggaran_kas_bulan_1' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'anggaran_kas_bulan_2' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'anggaran_kas_bulan_3' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'anggaran_kas_bulan_4' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'anggaran_kas_bulan_5' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'anggaran_kas_bulan_6' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'anggaran_kas_bulan_7' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'anggaran_kas_bulan_8' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'anggaran_kas_bulan_9' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'anggaran_kas_bulan_10' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'anggaran_kas_bulan_11' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'anggaran_kas_bulan_12' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_sp2d_bulan_1' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_sp2d_bulan_2' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_sp2d_bulan_3' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_sp2d_bulan_4' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_sp2d_bulan_5' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_sp2d_bulan_6' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_sp2d_bulan_7' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_sp2d_bulan_8' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_sp2d_bulan_9' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_sp2d_bulan_10' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_sp2d_bulan_11' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_sp2d_bulan_12' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_spj_bulan_1' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_spj_bulan_2' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_spj_bulan_3' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_spj_bulan_4' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_spj_bulan_5' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_spj_bulan_6' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_spj_bulan_7' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_spj_bulan_8' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_spj_bulan_9' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_spj_bulan_10' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_spj_bulan_11' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_spj_bulan_12' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		/*'pelimpahan_bulan_1' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_2' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_3' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_4' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_5' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_6' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_7' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_8' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_9' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_10' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_11' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_12' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],*/
	];

	public $isiRealisasiPe = [
		'pelimpahan_bulan_1_1' => 'required|integer',
		'pelimpahan_bulan_2_1' => 'required|integer',
		'pelimpahan_bulan_3_1' => 'required|integer',
		'pelimpahan_bulan_4_1' => 'required|integer',
		'pelimpahan_bulan_5_1' => 'required|integer',
		'pelimpahan_bulan_6_1' => 'required|integer',
		'pelimpahan_bulan_7_1' => 'required|integer',
		'pelimpahan_bulan_8_1' => 'required|integer',
		'pelimpahan_bulan_9_1' => 'required|integer',
		'pelimpahan_bulan_10_1' => 'required|integer',
		'pelimpahan_bulan_11_1' => 'required|integer',
		'pelimpahan_bulan_12_1' => 'required|integer',
		'pelimpahan_bulan_1_2' => 'required|integer',
		'pelimpahan_bulan_2_2' => 'required|integer',
		'pelimpahan_bulan_3_2' => 'required|integer',
		'pelimpahan_bulan_4_2' => 'required|integer',
		'pelimpahan_bulan_5_2' => 'required|integer',
		'pelimpahan_bulan_6_2' => 'required|integer',
		'pelimpahan_bulan_7_2' => 'required|integer',
		'pelimpahan_bulan_8_2' => 'required|integer',
		'pelimpahan_bulan_9_2' => 'required|integer',
		'pelimpahan_bulan_10_2' => 'required|integer',
		'pelimpahan_bulan_11_2' => 'required|integer',
		'pelimpahan_bulan_12_2' => 'required|integer'
	];

	public $isiRealisasiPe_errors = [
		'pelimpahan_bulan_1_1' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_2_1' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_3_1' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_4_1' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_5_1' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_6_1' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_7_1' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_8_1' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_9_1' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_10_1' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_11_1' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_12_1' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_1_2' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_2_2' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_3_2' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_4_2' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_5_2' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_6_2' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_7_2' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_8_2' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_9_2' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_10_2' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_11_2' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'pelimpahan_bulan_12_2' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
	];

	public $isiRealisasiGaji = [
		'anggaran_kas_bulan_1' => 'required|integer',
		'anggaran_kas_bulan_2' => 'required|integer',
		'anggaran_kas_bulan_3' => 'required|integer',
		'anggaran_kas_bulan_4' => 'required|integer',
		'anggaran_kas_bulan_5' => 'required|integer',
		'anggaran_kas_bulan_6' => 'required|integer',
		'anggaran_kas_bulan_7' => 'required|integer',
		'anggaran_kas_bulan_8' => 'required|integer',
		'anggaran_kas_bulan_9' => 'required|integer',
		'anggaran_kas_bulan_10' => 'required|integer',
		'anggaran_kas_bulan_11' => 'required|integer',
		'anggaran_kas_bulan_12' => 'required|integer',
		'realisasi_sp2d_bulan_1' => 'required|integer',
		'realisasi_sp2d_bulan_2' => 'required|integer',
		'realisasi_sp2d_bulan_3' => 'required|integer',
		'realisasi_sp2d_bulan_4' => 'required|integer',
		'realisasi_sp2d_bulan_5' => 'required|integer',
		'realisasi_sp2d_bulan_6' => 'required|integer',
		'realisasi_sp2d_bulan_7' => 'required|integer',
		'realisasi_sp2d_bulan_8' => 'required|integer',
		'realisasi_sp2d_bulan_9' => 'required|integer',
		'realisasi_sp2d_bulan_10' => 'required|integer',
		'realisasi_sp2d_bulan_11' => 'required|integer',
		'realisasi_sp2d_bulan_12' => 'required|integer',
		'realisasi_spj_bulan_1' => 'required|integer',
		'realisasi_spj_bulan_2' => 'required|integer',
		'realisasi_spj_bulan_3' => 'required|integer',
		'realisasi_spj_bulan_4' => 'required|integer',
		'realisasi_spj_bulan_5' => 'required|integer',
		'realisasi_spj_bulan_6' => 'required|integer',
		'realisasi_spj_bulan_7' => 'required|integer',
		'realisasi_spj_bulan_8' => 'required|integer',
		'realisasi_spj_bulan_9' => 'required|integer',
		'realisasi_spj_bulan_10' => 'required|integer',
		'realisasi_spj_bulan_11' => 'required|integer',
		'realisasi_spj_bulan_12' => 'required|integer',
	];

	public $isiRealisasiGaji_errors = [
		'anggaran_kas_bulan_1' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'anggaran_kas_bulan_2' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'anggaran_kas_bulan_3' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'anggaran_kas_bulan_4' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'anggaran_kas_bulan_5' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'anggaran_kas_bulan_6' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'anggaran_kas_bulan_7' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'anggaran_kas_bulan_8' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'anggaran_kas_bulan_9' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'anggaran_kas_bulan_10' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'anggaran_kas_bulan_11' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'anggaran_kas_bulan_12' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_sp2d_bulan_1' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_sp2d_bulan_2' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_sp2d_bulan_3' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_sp2d_bulan_4' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_sp2d_bulan_5' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_sp2d_bulan_6' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_sp2d_bulan_7' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_sp2d_bulan_8' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_sp2d_bulan_9' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_sp2d_bulan_10' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_sp2d_bulan_11' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_sp2d_bulan_12' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_spj_bulan_1' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_spj_bulan_2' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_spj_bulan_3' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_spj_bulan_4' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_spj_bulan_5' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_spj_bulan_6' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_spj_bulan_7' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_spj_bulan_8' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_spj_bulan_9' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_spj_bulan_10' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_spj_bulan_11' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
		'realisasi_spj_bulan_12' => [
			'required' => 'Kolom harus diisi',
			'integer' => 'Kolom harus berupa angka'
		],
	];
	//--------------------------------------------------------------------


}
