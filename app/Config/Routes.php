<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Login::index');

/* 
* From this routes use form admin
*/
$routes->get('/dashboard', 'Admin\Home::index');


$routes->get('/users/print_gaji', 'Users::print_salary');
$routes->get('/users/gaji/print', 'Users::print_pdf');

/**  Users management */
$routes->get('/users', 'Admin\Users::index');
$routes->get('/roles', 'Admin\Users::roles');
$routes->get('/tree', 'Admin\Users::tree');
$routes->get('/users/insert', 'Admin\Users::insert');
$routes->post('/users/store', 'Admin\Users::store');
$routes->delete('/users/(:num)', 'Admin\Users::delete/$1');
$routes->put('/users/(:alphanum)', 'Admin\Users::change_status/$1');
$routes->get('/users/edit/(:alphanum)', 'Admin\Users::edit/$1');
$routes->post('/users/update/(:num)', 'Admin\Users::update/$1');

$routes->get('/users/edit_password/(:alphanum)', 'Users::edit_password/$1');
$routes->post('/users/update_password/(:num)', 'Users::update_password/$1');

/**  Salary management */
$routes->get('/salary', 'Admin\Salary::index_');
$routes->get('/salary/import', 'Admin\Salary::import');
$routes->post('/salary/store', 'Admin\Salary::store_');
$routes->get('/salary/edit/(:num)', 'Admin\Salary::edit/$1');
$routes->post('/salary/update/(:num)', 'Admin\Salary::update/$1');
$routes->delete('/salary/(:num)', 'Admin\Salary::delete/$1');

/**  Gaji management */
$routes->get('/gaji', 'Admin\Gaji::index');
$routes->get('/gaji/import', 'Admin\Gaji::import');
$routes->post('/gaji/store', 'Admin\Gaji::store');
$routes->get('/gaji/edit/(:num)', 'Admin\Gaji::edit/$1');
$routes->post('/gaji/update/(:num)', 'Admin\Gaji::update/$1');
$routes->delete('/gaji/(:num)', 'Admin\Gaji::delete/$1');

/** BBM */
$routes->get('/bbm', 'Admin\Bbm::index_');
$routes->get('/bbm/import', 'Admin\Bbm::import');
$routes->post('/bbm/store', 'Admin\Bbm::store_');
$routes->get('/bbm/edit/(:num)', 'Admin\Bbm::edit/$1');
$routes->post('/bbm/update/(:num)', 'Admin\Bbm::update/$1');
$routes->delete('/bbm/(:num)', 'Admin\Bbm::delete/$1');

/** Kompensasi */
$routes->get('/kompensasi', 'Admin\Kompensasi::index_');
$routes->get('/kompensasi/import', 'Admin\Kompensasi::import');
$routes->post('/kompensasi/store', 'Admin\Kompensasi::store_');
$routes->get('/kompensasi/edit/(:num)', 'Admin\Kompensasi::edit/$1');
$routes->post('/kompensasi/update/(:num)', 'Admin\Kompensasi::update/$1');
$routes->delete('/kompensasi/(:num)', 'Admin\Kompensasi::delete/$1');

/** TPP5 */
$routes->get('/tpp', 'Admin\Tpp::index_');
$routes->get('/tpp/import', 'Admin\Tpp::import');
$routes->post('/tpp/store', 'Admin\Tpp::store_');
$routes->get('/tpp/edit/(:num)', 'Admin\Tpp::edit/$1');
$routes->post('/tpp/update/(:num)', 'Admin\Tpp::update/$1');
$routes->delete('/tpp/(:num)', 'Admin\Tpp::delete/$1');

/** Anggaran */
$routes->get('/anggaran', 'Admin\Anggaran::index');
$routes->get('/anggaran/insert', 'Admin\Anggaran::insert');
$routes->post('/anggaran/store', 'Admin\Anggaran::store');
$routes->delete('/anggaran/(:num)', 'Admin\Anggaran::delete/$1');
$routes->put('/anggaran/(:num)', 'Admin\Anggaran::confirm/$1');
$routes->post('/anggaran/edit_jumlah_anggaran', 'Admin\Anggaran::edit_jumlah_anggaran');


/** Realisasi */
$routes->get('/realisasi/edit/(:alphanum)', 'Admin\Realisasi::index/$1');
$routes->get('/realisasi/edit_pelimpahan/(:alphanum)', 'Admin\Realisasi::index_pelimpahan/$1');
$routes->get('/realisasi/edit_bpp/(:num)', 'Admin\Realisasi::index_bpp/$1');
$routes->post('/realisasi/store', 'Admin\Realisasi::store');
$routes->post('/realisasi/store_pelimpahan', 'Admin\Realisasi::store_pelimpahan');

/** Report */
$routes->get('/report/bulan', 'Admin\Report::index');
$routes->get('/report/tahun', 'Admin\Report::tahun');
$routes->get('/report/bulan_gaji', 'Admin\Report::bulanGaji');
$routes->get('/report/tahun_gaji', 'Admin\Report::tahunGaji');


/** Pelimpahan */
$routes->get('/pelimpahan', 'Admin\Pelimpahan::index');

/**  Files management */
$routes->get('/files', 'Admin\FileUpload::index');
$routes->get('/files/import', 'Admin\FileUpload::import');
$routes->post('/files/store', 'Admin\FileUpload::store');
$routes->put('/files/(:num)', 'Admin\FileUpload::download/$1');
$routes->delete('/files/(:num)', 'Admin\FileUpload::delete/$1');

/** Anggaran Gaji */
$routes->get('/anggaran_gaji', 'Admin\AnggaranGaji::index');
$routes->get('/anggaran_gaji/insert', 'Admin\AnggaranGaji::insert');
$routes->post('/anggaran_gaji/store', 'Admin\AnggaranGaji::store');
$routes->delete('/anggaran_gaji/(:num)', 'Admin\AnggaranGaji::delete/$1');
$routes->put('/anggaran_gaji/(:num)', 'Admin\AnggaranGaji::confirm/$1');
$routes->get('/anggaran_gaji/edit/(:alphanum)', 'Admin\AnggaranGaji::isiAnggaranGaji/$1');
$routes->post('/anggaran_gaji/store_gaji', 'Admin\AnggaranGaji::store_isiAnggaran');

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
