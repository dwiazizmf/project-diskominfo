// Call the dataTables jQuery plugin
$(document).ready(function () {
  var table = $("#dataTable").DataTable({
    dom: "Bfrtip",
    buttons: ["copyHtml5", "excelHtml5", "print"],
  });
});
