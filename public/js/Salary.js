// Call the dataTables jQuery plugin
$(document).ready(function () {
  $("#periode").datepicker({
    format: "yyyy-mm",
    viewMode: "months",
    minViewMode: "months",
  });

  $("#tahun_anggaran").datepicker({
    format: "yyyy",
    viewMode: "years",
    minViewMode: "years",
  });

  var table = $("#dataTable").DataTable({
    dom: "Bfrtip",
    buttons: ["copyHtml5", "excelHtml5", {extend : "print", autoPrint: false}],
  });
});

function labeling() {
  const file = document.querySelector("#file-upload");
  const label = document.querySelector("#label-upload");

  label.textContent = file.files[0].name;
}
