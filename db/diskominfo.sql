-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 26, 2020 at 03:14 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `diskominfo`
--

-- --------------------------------------------------------

--
-- Table structure for table `anggaran`
--

CREATE TABLE `anggaran` (
  `id` int(13) UNSIGNED NOT NULL,
  `unique_id` text NOT NULL,
  `id_bidang` int(13) NOT NULL,
  `id_pegawai` int(13) NOT NULL,
  `tahun_anggaran` int(11) NOT NULL,
  `jumlah_dpa` decimal(30,0) DEFAULT NULL,
  `anggaran_kas` decimal(20,0) NOT NULL,
  `sp2d` decimal(20,0) NOT NULL,
  `spj` decimal(20,0) NOT NULL,
  `pelimpahan` decimal(20,0) NOT NULL,
  `total_all` decimal(20,0) NOT NULL,
  `confirm_1` tinyint(4) NOT NULL DEFAULT 0,
  `user_confirm_1` int(13) NOT NULL,
  `confirm_2` tinyint(4) NOT NULL DEFAULT 0,
  `user_confirm_2` int(11) NOT NULL,
  `user_inserted` int(13) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `anggaran`
--

INSERT INTO `anggaran` (`id`, `unique_id`, `id_bidang`, `id_pegawai`, `tahun_anggaran`, `jumlah_dpa`, `anggaran_kas`, `sp2d`, `spj`, `pelimpahan`, `total_all`, `confirm_1`, `user_confirm_1`, `confirm_2`, `user_confirm_2`, `user_inserted`, `created_at`, `updated_at`) VALUES
(15, 'c7bbd02685dba746fd449fcc5d930813', 1, 11, 2020, '10822532104', '75384299853', '25436551400', '23779118774', '2910270032', '0', 0, 0, 0, 0, 0, '2020-09-21 12:57:02', '2020-09-26 00:31:22'),
(16, '6f3038efff625b6cfbe331897670b13e', 3, 8, 2020, '6539427653', '44131167432', '11860647170', '9061472662', '0', '0', 0, 0, 0, 0, 0, '2020-09-21 12:58:59', '2020-09-22 11:30:30'),
(17, '99eaa168fe03f950b6034de2e59c5528', 4, 1, 2020, '1134791893', '7825797560', '3869928328', '2561671174', '0', '0', 0, 0, 0, 0, 0, '2020-09-21 13:00:44', '2020-09-25 05:19:49'),
(18, '79facc6f1b2a78482a8adb04cf2240f0', 5, 8, 2020, '22974475999', '128634597484', '25613554561', '23897964311', '0', '0', 0, 0, 0, 0, 0, '2020-09-21 13:01:31', '2020-09-25 20:46:24'),
(19, '43e92d2a4f0eca93847b8b6afc24fcb5', 6, 7, 2020, '11824693259', '87595900787', '21029321395', '17949648116', '0', '0', 0, 0, 0, 0, 0, '2020-09-21 13:02:28', '2020-09-25 21:00:50'),
(20, 'f43083701abfd2cc30daed724e339422', 7, 7, 2020, '1716205023', '10317212585', '3877778573', '2321502143', '0', '0', 0, 0, 0, 0, 0, '2020-09-21 13:03:36', '2020-09-25 21:15:45'),
(21, 'da7fe78aca00f427c3935e67d93a0336', 8, 1, 2020, '4648981884', '38002071093', '4923147471', '3153913632', '398267800', '0', 0, 0, 0, 0, 0, '2020-09-21 13:04:20', '2020-09-26 00:37:33');

-- --------------------------------------------------------

--
-- Table structure for table `anggaran_gaji`
--

CREATE TABLE `anggaran_gaji` (
  `id` int(13) UNSIGNED NOT NULL,
  `unique_id` text NOT NULL,
  `user_inserted` int(13) NOT NULL,
  `tahun_anggaran` int(11) NOT NULL,
  `jumlah_dpa_gaji` decimal(30,0) DEFAULT NULL,
  `anggaran_kas_gaji` decimal(20,0) NOT NULL,
  `gaji` decimal(20,0) NOT NULL,
  `bbm` decimal(20,0) NOT NULL,
  `tpp` decimal(20,0) NOT NULL,
  `kompensasi` decimal(20,0) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `anggaran_gaji`
--

INSERT INTO `anggaran_gaji` (`id`, `unique_id`, `user_inserted`, `tahun_anggaran`, `jumlah_dpa_gaji`, `anggaran_kas_gaji`, `gaji`, `bbm`, `tpp`, `kompensasi`, `created_at`, `updated_at`) VALUES
(2, '030cd329416666da61c38445c0a339c7', 3, 2020, '2000000000', '240000', '326097900', '32459727', '781711467', '38284890', '2020-09-23 11:50:18', '2020-09-23 14:03:13');

-- --------------------------------------------------------

--
-- Table structure for table `anggaran_kas`
--

CREATE TABLE `anggaran_kas` (
  `id` int(13) UNSIGNED NOT NULL,
  `id_anggaran` int(13) NOT NULL,
  `bulan_1` decimal(20,0) DEFAULT NULL,
  `bulan_2` decimal(20,0) DEFAULT NULL,
  `bulan_3` decimal(20,0) DEFAULT NULL,
  `bulan_4` decimal(20,0) DEFAULT NULL,
  `bulan_5` decimal(20,0) DEFAULT NULL,
  `bulan_6` decimal(20,0) DEFAULT NULL,
  `bulan_7` decimal(20,0) DEFAULT NULL,
  `bulan_8` decimal(20,0) DEFAULT NULL,
  `bulan_9` decimal(20,0) DEFAULT NULL,
  `bulan_10` decimal(20,0) DEFAULT NULL,
  `bulan_11` decimal(20,0) DEFAULT NULL,
  `bulan_12` decimal(20,0) DEFAULT NULL,
  `total` decimal(30,0) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `anggaran_kas`
--

INSERT INTO `anggaran_kas` (`id`, `id_anggaran`, `bulan_1`, `bulan_2`, `bulan_3`, `bulan_4`, `bulan_5`, `bulan_6`, `bulan_7`, `bulan_8`, `bulan_9`, `bulan_10`, `bulan_11`, `bulan_12`, `total`, `created_at`, `updated_at`) VALUES
(10, 15, '497257535', '1146367305', '2910247193', '4588070966', '5382060556', '6227943402', '7438854956', '8103303531', '8779004846', '9431140489', '10057516970', '10822532104', '0', '2020-09-21 12:57:02', '2020-09-26 00:31:22'),
(11, 16, '304696853', '918351903', '1565971308', '2230120258', '2814800244', '3270554736', '3933028536', '4706337986', '5431521153', '6100927603', '6315429199', '6539427653', '0', '2020-09-21 12:58:59', '2020-09-22 11:30:30'),
(12, 17, '1448000', '227584160', '413918036', '460940036', '515457036', '577880536', '638999305', '701668805', '952922305', '1075843555', '1124343893', '1134791893', '0', '2020-09-21 13:00:44', '2020-09-25 05:19:49'),
(13, 18, '39352000', '1708920873', '2907450573', '3196951868', '3263231918', '3304527918', '16937552259', '17024544559', '17131656759', '17204948759', '22940983999', '22974475999', '0', '2020-09-21 13:01:31', '2020-09-25 20:46:24'),
(14, 19, '904000000', '2882745945', '4200406945', '5653971349', '6373221349', '7242450645', '8263138259', '8983943259', '9703193259', '10422443259', '11141693259', '11824693259', '0', '2020-09-21 13:02:28', '2020-09-25 21:00:50'),
(15, 20, '58840800', '197093743', '388284688', '522149448', '606098248', '708624798', '851876742', '999684642', '1142483090', '1452235240', '1673636123', '1716205023', '0', '2020-09-21 13:03:36', '2020-09-25 21:15:45'),
(16, 21, '0', '526395377', '828588652', '1180237919', '4224191019', '4252151886', '4295420753', '4371698020', '4479957283', '4559219679', '4635228621', '4648981884', '0', '2020-09-21 13:04:20', '2020-09-26 00:37:33');

-- --------------------------------------------------------

--
-- Table structure for table `anggaran_kas_gaji`
--

CREATE TABLE `anggaran_kas_gaji` (
  `id` int(13) UNSIGNED NOT NULL,
  `id_anggaran_gaji` int(13) NOT NULL,
  `bulan_1` decimal(20,0) DEFAULT NULL,
  `bulan_2` decimal(20,0) DEFAULT NULL,
  `bulan_3` decimal(20,0) DEFAULT NULL,
  `bulan_4` decimal(20,0) DEFAULT NULL,
  `bulan_5` decimal(20,0) DEFAULT NULL,
  `bulan_6` decimal(20,0) DEFAULT NULL,
  `bulan_7` decimal(20,0) DEFAULT NULL,
  `bulan_8` decimal(20,0) DEFAULT NULL,
  `bulan_9` decimal(20,0) DEFAULT NULL,
  `bulan_10` decimal(20,0) DEFAULT NULL,
  `bulan_11` decimal(20,0) DEFAULT NULL,
  `bulan_12` decimal(20,0) DEFAULT NULL,
  `total` decimal(30,0) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `anggaran_kas_gaji`
--

INSERT INTO `anggaran_kas_gaji` (`id`, `id_anggaran_gaji`, `bulan_1`, `bulan_2`, `bulan_3`, `bulan_4`, `bulan_5`, `bulan_6`, `bulan_7`, `bulan_8`, `bulan_9`, `bulan_10`, `bulan_11`, `bulan_12`, `total`, `created_at`, `updated_at`) VALUES
(5, 2, '240000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2020-09-23 12:58:38', '2020-09-23 14:03:13');

-- --------------------------------------------------------

--
-- Table structure for table `bidang`
--

CREATE TABLE `bidang` (
  `id` int(13) UNSIGNED NOT NULL,
  `kode_bidang` varchar(30) NOT NULL,
  `nama_bidang` varchar(30) NOT NULL,
  `keterangan` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bidang`
--

INSERT INTO `bidang` (`id`, `kode_bidang`, `nama_bidang`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 'SEKRE', 'SEKRETARIAT', 'SEKRETARIAT', '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
(3, 'IKP', 'INFORMASI KOMUNIKASI PUBLIK', 'INFORMASI KOMUNIKASI PUBLIK', '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
(4, 'APTIKA', 'APTIKA', 'APTIKA', '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
(5, 'EGOV', 'E - GOVERNMENT', 'E - GOVERNMENT', '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
(6, 'UPTD', 'UPTD PLD', 'UPTD PLD', '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
(7, 'STATISTIK', 'STATISTIK', 'STATISTIK', '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
(8, 'PERSANDIAN', 'PERSANDIAN', 'PERSANDIAN', '2020-09-10 00:00:00', '2020-09-10 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `detail_pelimpahan`
--

CREATE TABLE `detail_pelimpahan` (
  `id` int(13) UNSIGNED NOT NULL,
  `id_anggaran` int(13) NOT NULL,
  `id_pelimpahan` int(13) NOT NULL,
  `id_pegawai` int(13) NOT NULL,
  `bulan_1` decimal(20,0) DEFAULT NULL,
  `bulan_2` decimal(20,0) DEFAULT NULL,
  `bulan_3` decimal(20,0) DEFAULT NULL,
  `bulan_4` decimal(20,0) DEFAULT NULL,
  `bulan_5` decimal(20,0) DEFAULT NULL,
  `bulan_6` decimal(20,0) DEFAULT NULL,
  `bulan_7` decimal(20,0) DEFAULT NULL,
  `bulan_8` decimal(20,0) DEFAULT NULL,
  `bulan_9` decimal(20,0) DEFAULT NULL,
  `bulan_10` decimal(20,0) DEFAULT NULL,
  `bulan_11` decimal(20,0) DEFAULT NULL,
  `bulan_12` decimal(20,0) DEFAULT NULL,
  `total` decimal(30,0) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `file_upload`
--

CREATE TABLE `file_upload` (
  `id` int(13) UNSIGNED NOT NULL,
  `nama_file` text NOT NULL,
  `keterangan` text DEFAULT NULL,
  `file_name` text NOT NULL,
  `user_uploaded` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `file_upload`
--

INSERT INTO `file_upload` (`id`, `nama_file`, `keterangan`, `file_name`, `user_uploaded`, `created_at`, `updated_at`) VALUES
(7, 'dokumen', 'dokumen penting', '1600645899_7e4dc4c014fcf1f59f87.xlsx', 2, '2020-09-20 18:51:39', '2020-09-20 18:51:39');

-- --------------------------------------------------------

--
-- Table structure for table `gaji`
--

CREATE TABLE `gaji` (
  `id` int(13) UNSIGNED NOT NULL,
  `periode` varchar(12) NOT NULL,
  `tahun` int(11) NOT NULL,
  `bulan` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `nip` varchar(30) NOT NULL,
  `pangkat` varchar(30) NOT NULL,
  `golongan_pangkat` varchar(30) NOT NULL,
  `nama_jabatan` text NOT NULL,
  `es` int(30) NOT NULL,
  `status` int(30) NOT NULL,
  `jiwa` int(30) NOT NULL,
  `anak` int(30) NOT NULL,
  `bulan_gaji` varchar(30) NOT NULL,
  `gaji_pokok` decimal(20,0) NOT NULL,
  `tunjangan_issu` decimal(20,0) DEFAULT NULL,
  `tunjangan_anak` decimal(20,0) DEFAULT NULL,
  `total_gaji_bersih` decimal(20,0) DEFAULT NULL,
  `tunjangan_struktural` decimal(20,0) DEFAULT NULL,
  `tunjangan_beras` decimal(20,0) DEFAULT NULL,
  `tunjangan_pajak_penghasilan` decimal(20,0) DEFAULT NULL,
  `pembulatan` decimal(20,0) DEFAULT NULL,
  `penghasilan_kotor` decimal(20,0) DEFAULT NULL,
  `iwp` decimal(20,0) DEFAULT NULL,
  `askes` decimal(20,0) DEFAULT NULL,
  `pph21` decimal(20,0) DEFAULT NULL,
  `bapetarum` decimal(20,0) DEFAULT NULL,
  `jumlah_potongan` decimal(20,0) DEFAULT NULL,
  `penghasilan_bersih` decimal(20,0) DEFAULT NULL,
  `sim_kompak` decimal(20,0) DEFAULT NULL,
  `pot_kompak` decimal(20,0) DEFAULT NULL,
  `pot_bjb` decimal(20,0) DEFAULT NULL,
  `pot_dw` decimal(20,0) DEFAULT NULL,
  `pot_kopri` decimal(20,0) DEFAULT NULL,
  `total_pot` decimal(20,0) DEFAULT NULL,
  `gaji_set_pot` decimal(20,0) DEFAULT NULL,
  `bbm` decimal(20,0) DEFAULT NULL,
  `bbm_pph21` decimal(20,0) DEFAULT NULL,
  `bbm_bersih` decimal(20,0) DEFAULT NULL,
  `kompensasi` decimal(20,0) DEFAULT NULL,
  `kompensasi_pph21` decimal(20,0) DEFAULT NULL,
  `pot_kop_kompen` decimal(20,0) DEFAULT NULL,
  `jumlah_pot_komponen` decimal(20,0) NOT NULL,
  `kompensasi_bersih` decimal(20,0) DEFAULT NULL,
  `tpp` decimal(20,0) DEFAULT NULL,
  `persen` varchar(20) DEFAULT NULL,
  `bruto` decimal(20,0) DEFAULT NULL,
  `pph21_tpp` decimal(20,0) DEFAULT NULL,
  `tpp_net` decimal(20,0) DEFAULT NULL,
  `zakat` decimal(20,0) DEFAULT NULL,
  `kpps` decimal(20,0) DEFAULT NULL,
  `a_kpps` decimal(20,0) DEFAULT NULL,
  `a_bjb` decimal(20,0) DEFAULT NULL,
  `a_kompak` decimal(20,0) DEFAULT NULL,
  `a_syariah` decimal(20,0) DEFAULT NULL,
  `a_total_pot` decimal(20,0) DEFAULT NULL,
  `tpp_bersih` decimal(20,0) DEFAULT NULL,
  `total_penerimaan_gaji` decimal(20,0) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `gaji`
--

INSERT INTO `gaji` (`id`, `periode`, `tahun`, `bulan`, `nama`, `nip`, `pangkat`, `golongan_pangkat`, `nama_jabatan`, `es`, `status`, `jiwa`, `anak`, `bulan_gaji`, `gaji_pokok`, `tunjangan_issu`, `tunjangan_anak`, `total_gaji_bersih`, `tunjangan_struktural`, `tunjangan_beras`, `tunjangan_pajak_penghasilan`, `pembulatan`, `penghasilan_kotor`, `iwp`, `askes`, `pph21`, `bapetarum`, `jumlah_potongan`, `penghasilan_bersih`, `sim_kompak`, `pot_kompak`, `pot_bjb`, `pot_dw`, `pot_kopri`, `total_pot`, `gaji_set_pot`, `bbm`, `bbm_pph21`, `bbm_bersih`, `kompensasi`, `kompensasi_pph21`, `pot_kop_kompen`, `jumlah_pot_komponen`, `kompensasi_bersih`, `tpp`, `persen`, `bruto`, `pph21_tpp`, `tpp_net`, `zakat`, `kpps`, `a_kpps`, `a_bjb`, `a_kompak`, `a_syariah`, `a_total_pot`, `tpp_bersih`, `total_penerimaan_gaji`, `created_at`, `updated_at`) VALUES
(13, '2020-09', 2020, 9, 'SETIAJI,S.T., M.Si.', '197406081998031003', 'Pembina Utama Muda', 'IV/c', 'KEPALA DINAS KOMUNIKASI DAN INFORMATIKA', 2, 1, 4, 2, 'August 2020', '4651800', '465180', '186072', '5303052', '3250000', '289680', '0', '43', '8842775', '424244', '85531', '126187', '10000', '645962', '8196813', '0', '0', '0', '0', '0', '0', '8196813', '1250000', '76033', '1173967', '600000', '28500', '0', '0', '571500', '37125000', '1', '35328150', '6763634', '28564516', '714113', '750000', '0', '0', '0', '0', '1464113', '27100403', '37042683', '2020-09-22 12:11:19', '2020-09-22 12:26:23'),
(14, '2020-09', 2020, 9, 'Hj. ASTRIA PRIANTIE,S.E., M.M.', '197111272007012005', 'Penata', 'III/c', 'KEPALA SUBBAGIAN KEUANGAN DAN ASET', 4, 0, 3, 1, 'August 2020', '3704300', '0', '74086', '3778386', '540000', '217260', NULL, '91', '4535737', '302271', '43184', NULL, '10000', '355455', '4180282', NULL, NULL, NULL, NULL, NULL, '0', '4180282', '1100000', '23316', '1076684', '500000', '0', NULL, '0', '500000', '15730000', '1', '15517645', '1929179', '13588466', '339712', '250000', '0', '0', '0', '0', '589712', '12998754', '18755721', '2020-09-22 12:11:19', '2020-09-22 12:26:23'),
(15, '2020-09', 2020, 9, 'IMAS ROSIDAH,S.P.', '197109192007012004', 'Penata Muda', 'III/a', 'PENGELOLA KEUANGAN', 5, 1, 2, 0, 'August 2020', '3627900', '362790', '0', '3990690', '0', '144840', NULL, '82', '4135612', '319255', '39907', NULL, '10000', '369162', '3766450', NULL, NULL, NULL, NULL, NULL, '0', '3766450', NULL, NULL, '0', '500000', '0', '53000', '0', '447000', '7590000', '1', '7378998', '578595', '6800403', '170010', '100000', '0', '0', '600000', '0', '870010', '5930393', '10143843', '2020-09-22 12:11:19', '2020-09-22 12:26:23'),
(16, '2020-09', 2020, 9, 'JEFRI PAISHA,A.Md.', '197609042009011001', 'Penata Muda', 'III/a', 'BENDAHARA', 5, 0, 1, 0, 'August 2020', '3409800', '0', '0', '3409800', '0', '72420', NULL, '12', '3482232', '272784', '34098', NULL, '10000', '316882', '3165350', NULL, NULL, NULL, NULL, NULL, '0', '3165350', NULL, NULL, '0', '500000', '0', NULL, '0', '500000', '9790000', '1', '9605948', '875020', '8730928', '218273', '100000', '0', '0', '0', '0', '318273', '8412655', '12078005', '2020-09-22 12:11:19', '2020-09-22 12:26:23'),
(17, '2020-09', 2020, 9, 'ADI SETIADI RAMDHANI,A.Md.', '198405102010011002', 'Penata Muda', 'III/a', 'PENGELOLA KEUANGAN', 5, 1, 4, 2, 'August 2020', '3204700', '320470', '128188', '3653358', '0', '289680', NULL, '15', '3943053', '292269', '36534', NULL, '10000', '338802', '3604251', NULL, NULL, NULL, NULL, NULL, '0', '3604251', NULL, NULL, '0', '500000', '0', NULL, '0', '500000', '7590000', '1', '7388865', '441095', '6947770', '173694', '100000', NULL, NULL, NULL, NULL, '273694', '6674076', '10778327', '2020-09-22 12:11:19', '2020-09-22 12:26:23'),
(18, '2020-09', 2020, 9, 'RANI FARDIANI', '197608182008012010', 'Pengatur Tk.I', 'II/d', 'PENGELOLA KEUANGAN', 5, 0, 1, 0, 'August 2020', '3171500', '0', '0', '3171500', '0', '72420', NULL, '15', '3243935', '253720', '31715', NULL, '10000', '295435', '2948500', NULL, NULL, NULL, NULL, NULL, '0', '2948500', NULL, NULL, '0', '400000', '0', NULL, '0', '400000', '7590000', '1', '7265907', '474208', '6791699', '169792', '100000', '0', '0', '0', '0', '269792', '6521907', '9870407', '2020-09-22 12:11:19', '2020-09-22 12:26:23');

-- --------------------------------------------------------

--
-- Table structure for table `gaji_bbm`
--

CREATE TABLE `gaji_bbm` (
  `id` int(13) UNSIGNED NOT NULL,
  `id_anggaran_gaji` int(13) NOT NULL,
  `bulan_1` decimal(20,0) DEFAULT NULL,
  `bulan_2` decimal(20,0) DEFAULT NULL,
  `bulan_3` decimal(20,0) DEFAULT NULL,
  `bulan_4` decimal(20,0) DEFAULT NULL,
  `bulan_5` decimal(20,0) DEFAULT NULL,
  `bulan_6` decimal(20,0) DEFAULT NULL,
  `bulan_7` decimal(20,0) DEFAULT NULL,
  `bulan_8` decimal(20,0) DEFAULT NULL,
  `bulan_9` decimal(20,0) DEFAULT NULL,
  `bulan_10` decimal(20,0) DEFAULT NULL,
  `bulan_11` decimal(20,0) DEFAULT NULL,
  `bulan_12` decimal(20,0) DEFAULT NULL,
  `total` decimal(30,0) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `gaji_bbm`
--

INSERT INTO `gaji_bbm` (`id`, `id_anggaran_gaji`, `bulan_1`, `bulan_2`, `bulan_3`, `bulan_4`, `bulan_5`, `bulan_6`, `bulan_7`, `bulan_8`, `bulan_9`, `bulan_10`, `bulan_11`, `bulan_12`, `total`, `created_at`, `updated_at`) VALUES
(5, 2, '0', '0', '0', '0', '0', '0', '0', '0', '32459727', '0', '0', '0', '0', '2020-09-23 12:58:38', '2020-09-23 13:39:40');

-- --------------------------------------------------------

--
-- Table structure for table `gaji_gaji`
--

CREATE TABLE `gaji_gaji` (
  `id` int(13) UNSIGNED NOT NULL,
  `id_anggaran_gaji` int(13) NOT NULL,
  `bulan_1` decimal(20,0) DEFAULT NULL,
  `bulan_2` decimal(20,0) DEFAULT NULL,
  `bulan_3` decimal(20,0) DEFAULT NULL,
  `bulan_4` decimal(20,0) DEFAULT NULL,
  `bulan_5` decimal(20,0) DEFAULT NULL,
  `bulan_6` decimal(20,0) DEFAULT NULL,
  `bulan_7` decimal(20,0) DEFAULT NULL,
  `bulan_8` decimal(20,0) DEFAULT NULL,
  `bulan_9` decimal(20,0) DEFAULT NULL,
  `bulan_10` decimal(20,0) DEFAULT NULL,
  `bulan_11` decimal(20,0) DEFAULT NULL,
  `bulan_12` decimal(20,0) DEFAULT NULL,
  `total` decimal(30,0) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `gaji_gaji`
--

INSERT INTO `gaji_gaji` (`id`, `id_anggaran_gaji`, `bulan_1`, `bulan_2`, `bulan_3`, `bulan_4`, `bulan_5`, `bulan_6`, `bulan_7`, `bulan_8`, `bulan_9`, `bulan_10`, `bulan_11`, `bulan_12`, `total`, `created_at`, `updated_at`) VALUES
(5, 2, '0', '0', '0', '0', '0', '0', '0', '0', '326097900', '0', '0', '0', '0', '2020-09-23 12:56:00', '2020-09-23 14:02:50');

-- --------------------------------------------------------

--
-- Table structure for table `gaji_kompensasi`
--

CREATE TABLE `gaji_kompensasi` (
  `id` int(13) UNSIGNED NOT NULL,
  `id_anggaran_gaji` int(13) NOT NULL,
  `bulan_1` decimal(20,0) DEFAULT NULL,
  `bulan_2` decimal(20,0) DEFAULT NULL,
  `bulan_3` decimal(20,0) DEFAULT NULL,
  `bulan_4` decimal(20,0) DEFAULT NULL,
  `bulan_5` decimal(20,0) DEFAULT NULL,
  `bulan_6` decimal(20,0) DEFAULT NULL,
  `bulan_7` decimal(20,0) DEFAULT NULL,
  `bulan_8` decimal(20,0) DEFAULT NULL,
  `bulan_9` decimal(20,0) DEFAULT NULL,
  `bulan_10` decimal(20,0) DEFAULT NULL,
  `bulan_11` decimal(20,0) DEFAULT NULL,
  `bulan_12` decimal(20,0) DEFAULT NULL,
  `total` decimal(30,0) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `gaji_kompensasi`
--

INSERT INTO `gaji_kompensasi` (`id`, `id_anggaran_gaji`, `bulan_1`, `bulan_2`, `bulan_3`, `bulan_4`, `bulan_5`, `bulan_6`, `bulan_7`, `bulan_8`, `bulan_9`, `bulan_10`, `bulan_11`, `bulan_12`, `total`, `created_at`, `updated_at`) VALUES
(5, 2, '0', '0', '0', '0', '0', '0', '0', '0', '38284890', '0', '0', '0', '0', '2020-09-23 12:58:38', '2020-09-23 13:48:49');

-- --------------------------------------------------------

--
-- Table structure for table `gaji_tpp`
--

CREATE TABLE `gaji_tpp` (
  `id` int(13) UNSIGNED NOT NULL,
  `id_anggaran_gaji` int(13) NOT NULL,
  `bulan_1` decimal(20,0) DEFAULT NULL,
  `bulan_2` decimal(20,0) DEFAULT NULL,
  `bulan_3` decimal(20,0) DEFAULT NULL,
  `bulan_4` decimal(20,0) DEFAULT NULL,
  `bulan_5` decimal(20,0) DEFAULT NULL,
  `bulan_6` decimal(20,0) DEFAULT NULL,
  `bulan_7` decimal(20,0) DEFAULT NULL,
  `bulan_8` decimal(20,0) DEFAULT NULL,
  `bulan_9` decimal(20,0) DEFAULT NULL,
  `bulan_10` decimal(20,0) DEFAULT NULL,
  `bulan_11` decimal(20,0) DEFAULT NULL,
  `bulan_12` decimal(20,0) DEFAULT NULL,
  `total` decimal(30,0) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `gaji_tpp`
--

INSERT INTO `gaji_tpp` (`id`, `id_anggaran_gaji`, `bulan_1`, `bulan_2`, `bulan_3`, `bulan_4`, `bulan_5`, `bulan_6`, `bulan_7`, `bulan_8`, `bulan_9`, `bulan_10`, `bulan_11`, `bulan_12`, `total`, `created_at`, `updated_at`) VALUES
(5, 2, '0', '0', '0', '0', '0', '0', '0', '0', '781711467', '0', '0', '0', '0', '2020-09-23 12:58:38', '2020-09-23 14:01:48');

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `id` int(13) UNSIGNED NOT NULL,
  `kode_jabatan` varchar(30) NOT NULL,
  `nama_jabatan` varchar(30) NOT NULL,
  `keterangan` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`id`, `kode_jabatan`, `nama_jabatan`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 'BPP', 'Bendahara Pengeluaran P', NULL, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
(2, 'BP', 'Bendahara Pengeluaran', NULL, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
(3, 'KU', 'Kepala Keuangan', NULL, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
(4, 'PEGAWAI', 'Pegawai', NULL, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
(5, 'BG', 'Bendahara Gaji', NULL, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
(6, 'SEKDIS', 'Sekretaris Dinas', NULL, '2020-09-10 00:00:00', '2020-09-10 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id` int(13) UNSIGNED NOT NULL,
  `unique_id` text NOT NULL,
  `nip_pegawai` varchar(30) NOT NULL,
  `password_pegawai` varchar(30) DEFAULT NULL,
  `nama_pegawai` varchar(30) NOT NULL,
  `pangkat` varchar(30) DEFAULT NULL,
  `gol_pangkat` varchar(25) DEFAULT NULL,
  `nama_jabatan` text DEFAULT NULL,
  `id_bidang` int(13) DEFAULT NULL,
  `id_jabatan` int(13) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id`, `unique_id`, `nip_pegawai`, `password_pegawai`, `nama_pegawai`, `pangkat`, `gol_pangkat`, `nama_jabatan`, `id_bidang`, `id_jabatan`, `status`, `created_at`, `updated_at`) VALUES
(1, '8ed1754fc366a3ff931892d2310e8ed4', '196804191990032003', '12345', 'Maemunah', 'Penata Muda Tk.I', 'III/b', 'Pengelola Keuangan', 1, 1, 1, '2020-09-10 00:00:00', '2020-09-20 21:45:20'),
(2, '8ed1754fc366a3ff931892d2310e8ed1', '197609042009011001', '12345', 'Jefri Paisha,A.Md', 'Penata Muda', 'III/a', 'Bendahara', 1, 2, 1, '2020-09-10 00:00:00', '2020-09-20 21:31:15'),
(3, '8ed1754fc366a3ff931892d2310e8ed9', '197111272007012005', '12345', 'Astria Priantie,S.E., M.M', 'Penata', 'III/c', 'Kepala Sub Bagian Keuangan dan Aset', 1, 3, 1, '2020-09-10 00:00:00', '2020-09-20 21:32:22'),
(4, '8ed1754fc366a3ff931892d2310e8ed10', '198706232011011003', '12345', 'Eri Garna Santika, A.Md', 'Pengatur Tk.I', 'II/d', 'Pranata Komputer Pelaksana', 1, 4, 1, '2020-09-10 00:00:00', '2020-09-20 21:30:27'),
(5, '8ed1754fc366a3ff931892d2310e8ed71', '196601071988011003', '12345', 'T Sukmana', 'Pengatur', 'II/c', 'Bendahara Gaji', 1, 5, 1, '2020-09-10 00:00:00', '2020-09-20 21:28:12'),
(7, '8ed1754fc366a3ff931892d2310e8ed74', '198405102010011002', '12345', 'Adi Setiadi Ramdhani,A.Md', 'Penata Muda', 'III/a', 'Pengelola Keuangan', NULL, 1, 1, '2020-09-19 10:47:06', '2020-09-20 21:42:07'),
(8, '8ed1754fc366a3ff931892d2310e8ed7', '197109192007012004', '1234567', 'Imas Rosidah,S.P', 'Penata Muda', 'II/a', 'Pengelola Keuangan', NULL, 1, 1, '2020-09-20 12:04:07', '2020-09-20 21:35:26'),
(9, 'fccefecfbcfbdca3c616a48454a8aaac', '197703092009021001', '12345', 'Irwan Hadi,S.T', 'Penata', 'III/c', 'Penyuluh Teknik Informatika', NULL, 4, 1, '2020-09-20 14:14:03', '2020-09-20 21:44:52'),
(10, '9aa39021933c4d44f596d6eef39af9b4', '197803211997111001', '12345', 'Gilang Sailendra,S.STP., M.Si', 'Pembina', 'IV/a', 'Sekretaris Dinas', NULL, 6, 1, '2020-09-20 21:10:40', '2020-09-20 21:10:40'),
(11, 'c36a00c0ce65414b5975e749bfa93d19', '197608182008012010', '12345', 'Rani Fardiani', 'Pengatur Tk.I', 'II/d', 'Pengelola Keuangan', NULL, 1, 1, '2020-09-21 12:54:49', '2020-09-21 12:54:49'),
(12, 'd1e0922d893a449d5f88bd10e32362ba', '197009151996032001', '12345', 'TIOMAIDA SEVIANA HASMIDAWATI H', 'Pembina Tk.I', 'IV/b', 'KEPALA BIDANG PERSANDIAN DAN KEAMANAN INFORMASI', NULL, 4, 1, '2020-09-25 21:26:50', '2020-09-25 21:26:50'),
(13, 'fe62c5efb3158bd8a9b9d699d454f712', '197203121998032004', '12345', 'IDA NINGRUM,S.SI., M.Pd', 'Pembina Tk.I', 'IV/b', 'KEPALA BIDANG STATISTIK', NULL, 4, 1, '2020-09-25 21:29:23', '2020-09-25 21:29:23'),
(14, '8c6dc30cbab68811c6bd0947e4851dcc', '196902111998032002', '12345', 'Dra. LOVITA ADRIANA ROSA,M.Si.', 'Pembina Tk.I', 'IV/b', 'KEPALA BIDANG INFORMASI KOMUNIKASI PUBLIK', NULL, 4, 1, '2020-09-25 21:31:22', '2020-09-25 21:31:22'),
(15, '919dcbe3e653b37c3b5279a123d0f7c5', '196210181993031006', '12345', 'A DEDI DHARMAWAN,S.H., M.M.', 'Pembina Tk.I', 'IV/b', 'MPP setara kelas 7', NULL, 4, 1, '2020-09-25 21:33:31', '2020-09-25 21:33:31');

-- --------------------------------------------------------

--
-- Table structure for table `realisasi_pelimpahan`
--

CREATE TABLE `realisasi_pelimpahan` (
  `id` int(13) UNSIGNED NOT NULL,
  `id_anggaran` int(13) NOT NULL,
  `bulan_1` int(11) NOT NULL,
  `bulan_2` int(11) NOT NULL,
  `bulan_3` int(11) NOT NULL,
  `bulan_4` int(11) NOT NULL,
  `bulan_5` int(11) NOT NULL,
  `bulan_6` int(11) NOT NULL,
  `bulan_7` int(11) NOT NULL,
  `bulan_8` int(11) NOT NULL,
  `bulan_9` int(11) NOT NULL,
  `bulan_10` int(11) NOT NULL,
  `bulan_11` int(11) NOT NULL,
  `bulan_12` int(11) NOT NULL,
  `total_realisasi` int(30) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `realisasi_pelimpahan`
--

INSERT INTO `realisasi_pelimpahan` (`id`, `id_anggaran`, `bulan_1`, `bulan_2`, `bulan_3`, `bulan_4`, `bulan_5`, `bulan_6`, `bulan_7`, `bulan_8`, `bulan_9`, `bulan_10`, `bulan_11`, `bulan_12`, `total_realisasi`, `created_at`, `updated_at`) VALUES
(8, 13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, '2020-09-20 13:40:13', '2020-09-20 19:14:46'),
(9, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, '2020-09-20 18:43:31', '2020-09-20 18:43:31'),
(10, 15, 624850760, 568512972, 629473600, 0, 161630000, 187720000, 186075000, 552007700, 0, 0, 0, 0, NULL, '2020-09-21 12:57:02', '2020-09-26 00:31:22'),
(11, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, '2020-09-21 12:58:59', '2020-09-22 11:30:30'),
(12, 17, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, '2020-09-21 13:00:45', '2020-09-25 05:19:49'),
(13, 18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, '2020-09-21 13:01:31', '2020-09-25 20:46:25'),
(14, 19, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, '2020-09-21 13:02:28', '2020-09-25 21:00:50'),
(15, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, '2020-09-21 13:03:36', '2020-09-25 21:15:46'),
(16, 21, 125716600, 67051200, 140000000, 0, 0, 0, 65500000, 0, 0, 0, 0, 0, NULL, '2020-09-21 13:04:20', '2020-09-26 00:37:33');

-- --------------------------------------------------------

--
-- Table structure for table `realisasi_sp2d`
--

CREATE TABLE `realisasi_sp2d` (
  `id` int(13) UNSIGNED NOT NULL,
  `id_anggaran` int(13) NOT NULL,
  `bulan_1` decimal(20,0) DEFAULT NULL,
  `bulan_2` decimal(20,0) DEFAULT NULL,
  `bulan_3` decimal(20,0) DEFAULT NULL,
  `bulan_4` decimal(20,0) DEFAULT NULL,
  `bulan_5` decimal(20,0) DEFAULT NULL,
  `bulan_6` decimal(20,0) DEFAULT NULL,
  `bulan_7` decimal(20,0) DEFAULT NULL,
  `bulan_8` decimal(20,0) DEFAULT NULL,
  `bulan_9` decimal(20,0) DEFAULT NULL,
  `bulan_10` decimal(20,0) DEFAULT NULL,
  `bulan_11` decimal(20,0) DEFAULT NULL,
  `bulan_12` decimal(20,0) DEFAULT NULL,
  `total` decimal(30,0) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `realisasi_sp2d`
--

INSERT INTO `realisasi_sp2d` (`id`, `id_anggaran`, `bulan_1`, `bulan_2`, `bulan_3`, `bulan_4`, `bulan_5`, `bulan_6`, `bulan_7`, `bulan_8`, `bulan_9`, `bulan_10`, `bulan_11`, `bulan_12`, `total`, `created_at`, `updated_at`) VALUES
(6, 11, '510000', '500000', '990000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2020-09-19 10:30:51', '2020-09-19 10:43:36'),
(7, 12, '2300000', '430000', '760000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2020-09-19 10:48:00', '2020-09-19 10:50:27'),
(8, 13, '200000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2020-09-20 13:40:13', '2020-09-20 19:14:46'),
(9, 14, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2020-09-20 18:43:31', '2020-09-20 18:43:31'),
(10, 15, '895455640', '895455640', '2162563184', '2659018084', '2959413245', '3872837785', '5995903911', '5995903911', '0', '0', '0', '0', '0', '2020-09-21 12:57:02', '2020-09-26 00:31:22'),
(11, 16, '857442270', '857442270', '1580277061', '1731128061', '1778028061', '1948191861', '3108137586', '0', '0', '0', '0', '0', '0', '2020-09-21 12:58:59', '2020-09-22 11:30:30'),
(12, 17, '277718289', '300218289', '529008914', '640108914', '640108914', '662908914', '819856094', '0', '0', '0', '0', '0', '0', '2020-09-21 13:00:44', '2020-09-25 05:19:49'),
(13, 18, '332778000', '725138375', '2851706997', '2906319997', '3718519997', '7470865467', '7608225728', '0', '0', '0', '0', '0', '0', '2020-09-21 13:01:31', '2020-09-25 20:46:24'),
(14, 19, '1074234000', '1717734000', '2330628800', '3172798800', '3172798800', '3957228800', '5603898195', '0', '0', '0', '0', '0', '0', '2020-09-21 13:02:28', '2020-09-25 21:00:50'),
(15, 20, '355250480', '355250480', '541888769', '541888769', '541888769', '647099169', '894512137', '0', '0', '0', '0', '0', '0', '2020-09-21 13:03:36', '2020-09-25 21:15:45'),
(16, 21, '239040584', '364840584', '609151184', '609151184', '609151184', '1162146384', '1329666367', '0', '0', '0', '0', '0', '0', '2020-09-21 13:04:20', '2020-09-26 00:37:33');

-- --------------------------------------------------------

--
-- Table structure for table `realisasi_spj`
--

CREATE TABLE `realisasi_spj` (
  `id` int(13) UNSIGNED NOT NULL,
  `id_anggaran` int(13) NOT NULL,
  `bulan_1` decimal(20,0) DEFAULT NULL,
  `bulan_2` decimal(20,0) DEFAULT NULL,
  `bulan_3` decimal(20,0) DEFAULT NULL,
  `bulan_4` decimal(20,0) DEFAULT NULL,
  `bulan_5` decimal(20,0) DEFAULT NULL,
  `bulan_6` decimal(20,0) DEFAULT NULL,
  `bulan_7` decimal(20,0) DEFAULT NULL,
  `bulan_8` decimal(20,0) DEFAULT NULL,
  `bulan_9` decimal(20,0) DEFAULT NULL,
  `bulan_10` decimal(20,0) DEFAULT NULL,
  `bulan_11` decimal(20,0) DEFAULT NULL,
  `bulan_12` decimal(20,0) DEFAULT NULL,
  `total` decimal(30,0) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `realisasi_spj`
--

INSERT INTO `realisasi_spj` (`id`, `id_anggaran`, `bulan_1`, `bulan_2`, `bulan_3`, `bulan_4`, `bulan_5`, `bulan_6`, `bulan_7`, `bulan_8`, `bulan_9`, `bulan_10`, `bulan_11`, `bulan_12`, `total`, `created_at`, `updated_at`) VALUES
(10, 15, '76085255', '792029444', '1624571350', '2646121803', '3170377973', '4301743924', '5354312627', '5813876398', '0', '0', '0', '0', '0', '2020-09-21 12:57:02', '2020-09-26 00:31:22'),
(11, 16, '81513069', '557170991', '1120760616', '1501408416', '1601021216', '1771185016', '2428413338', '0', '0', '0', '0', '0', '0', '2020-09-21 12:58:59', '2020-09-22 11:30:30'),
(12, 17, '0', '127202625', '326855962', '462708472', '504339805', '528587805', '611976505', '0', '0', '0', '0', '0', '0', '2020-09-21 13:00:44', '2020-09-25 05:19:49'),
(13, 18, '9790742', '488197117', '2592368530', '2683150258', '3498144258', '7250489728', '7375823678', '0', '0', '0', '0', '0', '0', '2020-09-21 13:01:31', '2020-09-25 20:46:24'),
(14, 19, '574000000', '1256394800', '1981889800', '2937089739', '2943089791', '3727519791', '4529664195', '0', '0', '0', '0', '0', '0', '2020-09-21 13:02:28', '2020-09-25 21:00:50'),
(15, 20, '27536450', '126307644', '325055189', '329204989', '404452057', '509662457', '599283357', '0', '0', '0', '0', '0', '0', '2020-09-21 13:03:36', '2020-09-25 21:15:46'),
(16, 21, '23373300', '271317400', '509241250', '528626983', '558626983', '1111622183', '151105533', '0', '0', '0', '0', '0', '0', '2020-09-21 13:04:20', '2020-09-26 00:37:33');

-- --------------------------------------------------------

--
-- Table structure for table `salary`
--

CREATE TABLE `salary` (
  `id` int(11) NOT NULL,
  `kodin` int(11) NOT NULL,
  `rek_internal` varchar(200) DEFAULT NULL,
  `rek_eksternal` varchar(200) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `gaji_kotor` int(100) NOT NULL DEFAULT 0,
  `angs_cab_utama` int(100) NOT NULL DEFAULT 0,
  `angs_kab_bdg` int(100) NOT NULL DEFAULT 0,
  `angs_suci` int(100) NOT NULL DEFAULT 0,
  `angs_bubat` int(100) NOT NULL DEFAULT 0,
  `angs_gd_sate` int(100) NOT NULL DEFAULT 0,
  `angs_otista` int(100) NOT NULL DEFAULT 0,
  `angs_asia_afrika` int(100) NOT NULL DEFAULT 0,
  `pot_dinas` int(100) NOT NULL DEFAULT 0,
  `gaji_bersih` int(100) NOT NULL DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `salary`
--

INSERT INTO `salary` (`id`, `kodin`, `rek_internal`, `rek_eksternal`, `nama`, `gaji_kotor`, `angs_cab_utama`, `angs_kab_bdg`, `angs_suci`, `angs_bubat`, `angs_gd_sate`, `angs_otista`, `angs_asia_afrika`, `pot_dinas`, `gaji_bersih`, `created_at`, `updated_at`) VALUES
(2, 1035, '', '0009304398100', 'ADI SETIADI RAMDHANI', 3, 0, 1000, 0, 0, 3, 0, 0, 0, 75, '2020-09-03 12:43:10', '2020-09-09 11:27:16'),
(3, 1035, NULL, '0009304398100', '', 3, 0, 0, 0, 0, 3, 0, 0, 0, 75, '2020-09-03 14:05:22', '2020-09-03 14:05:22'),
(4, 1035, NULL, '0009304398100', 'ADI SETIADI RAMDHANI', 3, 0, 0, 0, 0, 3, 0, 0, 0, 75, '2020-09-09 11:26:19', '2020-09-09 11:26:19'),
(5, 1035, NULL, '0009304398100', 'ADI SETIADI RAMDHANI', 3, 0, 0, 0, 0, 3, 0, 0, 0, 75, '2020-09-09 19:28:47', '2020-09-09 19:28:47'),
(6, 0, NULL, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2020-09-23 12:40:45', '2020-09-23 12:40:45');

-- --------------------------------------------------------

--
-- Table structure for table `salary_bbm`
--

CREATE TABLE `salary_bbm` (
  `id` int(11) NOT NULL,
  `kodin` int(11) NOT NULL,
  `rek_internal` varchar(200) DEFAULT NULL,
  `rek_eksternal` varchar(200) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `standar_biasa` int(100) NOT NULL DEFAULT 0,
  `pph_21` int(100) NOT NULL DEFAULT 0,
  `pot_dinas` int(100) NOT NULL DEFAULT 0,
  `jumlah_bersih` int(100) NOT NULL DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `salary_bbm`
--

INSERT INTO `salary_bbm` (`id`, `kodin`, `rek_internal`, `rek_eksternal`, `nama`, `standar_biasa`, `pph_21`, `pot_dinas`, `jumlah_bersih`, `created_at`, `updated_at`) VALUES
(1, 1035, '0001-205344-100', '0013210446977', 'AKSAN KHOLIQ, IR ', 200000, 52251, 2, 1047750, '2020-09-03 14:01:13', '2020-09-04 14:09:56');

-- --------------------------------------------------------

--
-- Table structure for table `salary_kompensasi`
--

CREATE TABLE `salary_kompensasi` (
  `id` int(11) NOT NULL,
  `kodin` int(11) NOT NULL,
  `rek_internal` varchar(200) DEFAULT NULL,
  `rek_eksternal` varchar(200) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `tpp` int(100) NOT NULL DEFAULT 0,
  `pph_21` int(100) NOT NULL DEFAULT 0,
  `simpanan_kas` int(100) NOT NULL DEFAULT 0,
  `potongan_kkps` int(100) NOT NULL DEFAULT 0,
  `pot_dinas` int(100) NOT NULL DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `salary_kompensasi`
--

INSERT INTO `salary_kompensasi` (`id`, `kodin`, `rek_internal`, `rek_eksternal`, `nama`, `tpp`, `pph_21`, `simpanan_kas`, `potongan_kkps`, `pot_dinas`, `created_at`, `updated_at`) VALUES
(1, 1035, '0009304398100', '0009304398100', 'ADI SETIADI RAMDHANI', 1999, 22, 0, 1000, 0, '2020-09-03 14:32:53', '2020-09-04 14:26:21');

-- --------------------------------------------------------

--
-- Table structure for table `salary_tpp`
--

CREATE TABLE `salary_tpp` (
  `id` int(11) NOT NULL,
  `kodin` int(11) NOT NULL,
  `rek_internal` varchar(200) DEFAULT NULL,
  `rek_eksternal` varchar(200) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `tpp` int(100) NOT NULL DEFAULT 0,
  `zakat` int(100) NOT NULL DEFAULT 0,
  `simpanan_kkps` int(100) NOT NULL DEFAULT 0,
  `potongan_kkps` int(100) NOT NULL DEFAULT 0,
  `ptg_cab_utama` int(100) NOT NULL DEFAULT 0,
  `ptg_gd_sate` int(100) NOT NULL DEFAULT 0,
  `ptg_otista` int(100) NOT NULL DEFAULT 0,
  `pot_dinas` int(100) NOT NULL DEFAULT 0,
  `tpp_bersih` int(100) NOT NULL DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `salary_tpp`
--

INSERT INTO `salary_tpp` (`id`, `kodin`, `rek_internal`, `rek_eksternal`, `nama`, `tpp`, `zakat`, `simpanan_kkps`, `potongan_kkps`, `ptg_cab_utama`, `ptg_gd_sate`, `ptg_otista`, `pot_dinas`, `tpp_bersih`, `created_at`, `updated_at`) VALUES
(1, 1035, '0009304398100', '0009304398100', 'ADI SETIADI RAMDHANI', 7118742, 177969, 1000, 0, 20000, 927807, 0, 1660000, 4252966, '2020-09-03 14:48:02', '2020-09-09 15:16:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anggaran`
--
ALTER TABLE `anggaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `anggaran_gaji`
--
ALTER TABLE `anggaran_gaji`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `anggaran_kas`
--
ALTER TABLE `anggaran_kas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `anggaran_kas_gaji`
--
ALTER TABLE `anggaran_kas_gaji`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bidang`
--
ALTER TABLE `bidang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_pelimpahan`
--
ALTER TABLE `detail_pelimpahan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `file_upload`
--
ALTER TABLE `file_upload`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gaji`
--
ALTER TABLE `gaji`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gaji_bbm`
--
ALTER TABLE `gaji_bbm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gaji_gaji`
--
ALTER TABLE `gaji_gaji`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gaji_kompensasi`
--
ALTER TABLE `gaji_kompensasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gaji_tpp`
--
ALTER TABLE `gaji_tpp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `realisasi_pelimpahan`
--
ALTER TABLE `realisasi_pelimpahan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `realisasi_sp2d`
--
ALTER TABLE `realisasi_sp2d`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `realisasi_spj`
--
ALTER TABLE `realisasi_spj`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salary`
--
ALTER TABLE `salary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salary_bbm`
--
ALTER TABLE `salary_bbm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salary_kompensasi`
--
ALTER TABLE `salary_kompensasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salary_tpp`
--
ALTER TABLE `salary_tpp`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `anggaran`
--
ALTER TABLE `anggaran`
  MODIFY `id` int(13) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `anggaran_gaji`
--
ALTER TABLE `anggaran_gaji`
  MODIFY `id` int(13) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `anggaran_kas`
--
ALTER TABLE `anggaran_kas`
  MODIFY `id` int(13) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `anggaran_kas_gaji`
--
ALTER TABLE `anggaran_kas_gaji`
  MODIFY `id` int(13) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `bidang`
--
ALTER TABLE `bidang`
  MODIFY `id` int(13) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `detail_pelimpahan`
--
ALTER TABLE `detail_pelimpahan`
  MODIFY `id` int(13) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `file_upload`
--
ALTER TABLE `file_upload`
  MODIFY `id` int(13) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `gaji`
--
ALTER TABLE `gaji`
  MODIFY `id` int(13) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `gaji_bbm`
--
ALTER TABLE `gaji_bbm`
  MODIFY `id` int(13) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `gaji_gaji`
--
ALTER TABLE `gaji_gaji`
  MODIFY `id` int(13) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `gaji_kompensasi`
--
ALTER TABLE `gaji_kompensasi`
  MODIFY `id` int(13) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `gaji_tpp`
--
ALTER TABLE `gaji_tpp`
  MODIFY `id` int(13) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `jabatan`
--
ALTER TABLE `jabatan`
  MODIFY `id` int(13) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id` int(13) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `realisasi_pelimpahan`
--
ALTER TABLE `realisasi_pelimpahan`
  MODIFY `id` int(13) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `realisasi_sp2d`
--
ALTER TABLE `realisasi_sp2d`
  MODIFY `id` int(13) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `realisasi_spj`
--
ALTER TABLE `realisasi_spj`
  MODIFY `id` int(13) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `salary`
--
ALTER TABLE `salary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `salary_bbm`
--
ALTER TABLE `salary_bbm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `salary_kompensasi`
--
ALTER TABLE `salary_kompensasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `salary_tpp`
--
ALTER TABLE `salary_tpp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
